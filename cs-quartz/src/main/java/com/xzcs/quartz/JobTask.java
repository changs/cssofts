package com.xzcs.quartz;

import com.xzcs.util.spring.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 任务运行接口 
 * @date 2014-01-02
 */
public interface JobTask {
	public final Logger logger = LoggerFactory.getLogger(JobTask.class);
	
    public void run(PropertyUtils propertyUtils) throws Exception;
    
}
