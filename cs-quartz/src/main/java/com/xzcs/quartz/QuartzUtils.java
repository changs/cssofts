package com.xzcs.quartz;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Quartz工具类
 * @date 2014-01-03
 */
@Component
public class QuartzUtils {
	private static Logger logger = LoggerFactory.getLogger(QuartzUtils.class);
	@Resource(name="quartzScheduler")
	private Scheduler scheduler;
    private String JOB_GROUP_NAME = "JOB_GROUP_DEFAULT";
    private String TRIGGER_GROUP_NAME = "TRIGGER_GROUP_DEFAULT";
   
    public void addJob(String jobName, String triggerName, 
    		Class jobClass, String cron, Map<String, Object> params) {  
        try {  
            // 任务名，任务组，任务执行类
            JobDetail jobDetail= JobBuilder.newJob(jobClass)
            		.withIdentity(jobName, JOB_GROUP_NAME).build();
            
            JobDataMap map = jobDetail.getJobDataMap();
            map.putAll(params);

            // 创建Trigger对象
            CronTrigger trigger = (CronTrigger) TriggerBuilder.newTrigger()
            		.withIdentity(triggerName, TRIGGER_GROUP_NAME)
            		.withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
            
            // 调度容器设置JobDetail和Trigger
            scheduler.scheduleJob(jobDetail, trigger);  

            // 启动  
            if (!scheduler.isShutdown()) {  
            	scheduler.start();  
            }  
        } catch (Exception e) {
        	logger.debug("创建job失败！", e);
        }  
    }
    
    public void removeJob(String jobName, String triggerName){
        try {
        	TriggerKey tk = TriggerKey.triggerKey(triggerName, TRIGGER_GROUP_NAME);
        	scheduler.pauseTrigger(tk);//停止触发器  
        	scheduler.unscheduleJob(tk);//移除触发器
            JobKey jobKey = JobKey.jobKey(jobName, JOB_GROUP_NAME);
            scheduler.deleteJob(jobKey);//删除作业
        } catch (SchedulerException e) {
        	logger.debug("删除job失败！", e);
        }
    }

    public void pauseJob(String name){
        try {
        	JobKey jobKey = JobKey.jobKey(name, JOB_GROUP_NAME);
        	scheduler.pauseJob(jobKey);
        } catch (SchedulerException e) {
            logger.debug("暂停job失败！", e);
        }
    }
     
    public void resumeJob(String name){
        try {
            JobKey jobKey = JobKey.jobKey(name, JOB_GROUP_NAME);         
            scheduler.resumeJob(jobKey);
        } catch (SchedulerException e) {
            logger.debug("恢复job失败！", e);
        }       
    }
     
    public void modifyTime(String name, String cronExpression){       
        try {
        	TriggerKey tk = TriggerKey.triggerKey(name, TRIGGER_GROUP_NAME);
            //构造任务触发器
            Trigger trg = TriggerBuilder.newTrigger()
                    .withIdentity(name, TRIGGER_GROUP_NAME)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                    .build();       
            scheduler.rescheduleJob(tk, trg);
        } catch (SchedulerException e) {
            logger.debug("修改触发器时间失败！", e);
        }
    }
    
    public void start() {
        try {
        	scheduler.start();
        } catch (SchedulerException e) {
            logger.debug("启动调度器失败！", e);
        }
    }
 
    public void shutdown() {
        try {
        	scheduler.shutdown();
        } catch (SchedulerException e) {
            logger.debug("停止调度器失败！", e);
        }
    }
}
