/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50716
Source Host           : 127.0.0.1:3306
Source Database       : cs_softs

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-05-14 14:23:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_dict_group
-- ----------------------------
DROP TABLE IF EXISTS `t_dict_group`;
CREATE TABLE `t_dict_group` (
  `group_id` varchar(64) NOT NULL,
  `group_name` varchar(200) DEFAULT NULL,
  `group_desc` varchar(400) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1：有效 0：无效',
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_user` varchar(32) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_dict_group
-- ----------------------------
INSERT INTO `t_dict_group` VALUES ('BLOG.CATEGORY.TYPE', '栏目类型', '博客栏目类型', '1', '1', '2018-03-19 14:18:34', '1', '2018-03-19 14:18:34');
INSERT INTO `t_dict_group` VALUES ('BLOG.CONTENT.STATUS', '文章状态', '博客文章状态', '1', '1', '2018-03-19 15:23:49', '1', '2018-03-19 15:23:49');
INSERT INTO `t_dict_group` VALUES ('WEIX.ACCOUNT.TYPE', '微信公众号类型', '微信公众号类型', '1', '1', '2017-10-28 14:43:13', '1', '2017-10-28 14:43:13');
INSERT INTO `t_dict_group` VALUES ('WEIX.HUODONG.TYPE', '微信活动类型', '微信活动类型', '1', '1', '2017-11-08 19:58:12', '1', '2017-11-08 19:58:12');
INSERT INTO `t_dict_group` VALUES ('WEIX.MENU.TYPE', '微信菜单类型', '微信菜单类型', '1', '1', '2017-11-01 14:29:33', '1', '2017-11-01 14:29:33');
INSERT INTO `t_dict_group` VALUES ('WEIX.MESSAGE.TYPE', '微信消息类型', '微信消息类型', '1', '1', '2016-08-26 16:55:33', '1', '2017-10-31 20:42:24');
INSERT INTO `t_dict_group` VALUES ('WEIX.VOTE.MOBAN', '微信投票模板', '微信投票模板', '1', '1', '2017-11-16 13:48:06', '1', '2017-11-16 13:48:06');

-- ----------------------------
-- Table structure for t_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `t_dict_item`;
CREATE TABLE `t_dict_item` (
  `item_id` varchar(64) DEFAULT NULL,
  `item_name` varchar(200) DEFAULT NULL,
  `group_id` varchar(64) DEFAULT NULL,
  `show_order` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_dict_item
-- ----------------------------
INSERT INTO `t_dict_item` VALUES ('1', '订阅号', 'WEIX.ACCOUNT.TYPE', '0');
INSERT INTO `t_dict_item` VALUES ('2', '服务号', 'WEIX.ACCOUNT.TYPE', '1');
INSERT INTO `t_dict_item` VALUES ('text', '文本', 'WEIX.MESSAGE.TYPE', '0');
INSERT INTO `t_dict_item` VALUES ('news', '图文', 'WEIX.MESSAGE.TYPE', '1');
INSERT INTO `t_dict_item` VALUES ('expand', '扩展', 'WEIX.MESSAGE.TYPE', '2');
INSERT INTO `t_dict_item` VALUES ('click', '消息触发类', 'WEIX.MENU.TYPE', '0');
INSERT INTO `t_dict_item` VALUES ('view', '网页链接类', 'WEIX.MENU.TYPE', '1');
INSERT INTO `t_dict_item` VALUES ('1', '大转盘', 'WEIX.HUODONG.TYPE', '0');
INSERT INTO `t_dict_item` VALUES ('1', '模版一[粉色系]', 'WEIX.VOTE.MOBAN', '0');
INSERT INTO `t_dict_item` VALUES ('3', '模版三[草绿系]', 'WEIX.VOTE.MOBAN', '1');
INSERT INTO `t_dict_item` VALUES ('4', '模版四[淡蓝系]', 'WEIX.VOTE.MOBAN', '2');
INSERT INTO `t_dict_item` VALUES ('5', '模版五[土豪金]', 'WEIX.VOTE.MOBAN', '3');
INSERT INTO `t_dict_item` VALUES ('6', '模版六[黄色系]', 'WEIX.VOTE.MOBAN', '4');
INSERT INTO `t_dict_item` VALUES ('7', '模版七[深粉系]', 'WEIX.VOTE.MOBAN', '5');
INSERT INTO `t_dict_item` VALUES ('8', '模版八[清新系]', 'WEIX.VOTE.MOBAN', '6');
INSERT INTO `t_dict_item` VALUES ('news', '图文', 'BLOG.CATEGORY.TYPE', '0');
INSERT INTO `t_dict_item` VALUES ('img', '图片', 'BLOG.CATEGORY.TYPE', '1');
INSERT INTO `t_dict_item` VALUES ('video', '视频', 'BLOG.CATEGORY.TYPE', '2');
INSERT INTO `t_dict_item` VALUES ('0', '未发布', 'BLOG.CONTENT.STATUS', '0');
INSERT INTO `t_dict_item` VALUES ('1', '已发布', 'BLOG.CONTENT.STATUS', '1');

-- ----------------------------
-- Table structure for t_module
-- ----------------------------
DROP TABLE IF EXISTS `t_module`;
CREATE TABLE `t_module` (
  `module_id` int(12) NOT NULL AUTO_INCREMENT,
  `parent_id` int(12) DEFAULT NULL,
  `module_name` varchar(100) DEFAULT NULL,
  `module_type` int(1) DEFAULT NULL COMMENT '1：目录 2：菜单',
  `module_url` varchar(100) DEFAULT NULL,
  `module_level` int(4) DEFAULT '1',
  `module_order` int(11) DEFAULT NULL,
  `module_icon` varchar(64) DEFAULT NULL,
  `module_code` varchar(64) DEFAULT NULL,
  `module_path` varchar(200) DEFAULT NULL,
  `module_remark` varchar(400) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `operating_code` varchar(32) DEFAULT NULL,
  `operating_time` datetime DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_module
-- ----------------------------
INSERT INTO `t_module` VALUES ('1', '-1', '基础管理', '1', '', '1', '1', 'table', '', '基础管理', '', '1', '1', '2018-01-14 15:07:05');
INSERT INTO `t_module` VALUES ('2', '1', '系统管理', '1', '', '2', '1', 'cogs', '', '基础管理 - 系统管理', '', '1', '1', '2018-01-14 15:07:13');
INSERT INTO `t_module` VALUES ('3', '2', '模块管理', '2', '/sysmgr/module/frame.do', '3', '1', 'plane', 'module_frame', '基础管理 - 系统管理 - 模块管理', '模块管理', '1', '1', '2018-01-14 15:07:55');
INSERT INTO `t_module` VALUES ('7', '1', '公共资源', '1', '', '2', '0', 'database', '', '基础管理 - 公共资源', '', '1', '1', '2018-01-30 09:49:20');
INSERT INTO `t_module` VALUES ('8', '2', '操作日志', '2', '/sysmgr/log/frame.do', '3', '99', 'file-word-o', 'operating_frame', '基础管理 - 系统管理 - 操作日志', '', '1', '1', '2018-01-14 15:32:24');
INSERT INTO `t_module` VALUES ('12', '2', '用户管理', '2', '/sysmgr/user/frame.do', '3', '3', 'user', 'user_frame', '基础管理 - 系统管理 - 用户管理', '', '1', '1', '2018-01-14 15:33:52');
INSERT INTO `t_module` VALUES ('13', '2', '角色管理', '2', '/sysmgr/role/frame.do', '3', '4', 'group', 'role_frame', '基础管理 - 系统管理 - 角色管理', '', '1', '1', '2018-01-14 15:34:32');
INSERT INTO `t_module` VALUES ('14', '2', '文件管理', '2', '/sysmgr/file/frame.do', '3', '6', 'file-zip-o', 'file_frame', '基础管理 - 系统管理 - 文件管理', '', '1', '1', '2018-01-14 15:35:51');
INSERT INTO `t_module` VALUES ('28', '2', '网站信息', '2', '/sysmgr/site/frame.do', '3', '7', 'table', 'site_frame', '基础管理 - 系统管理 - 网站信息', '', '1', '1', '2018-01-14 15:36:02');
INSERT INTO `t_module` VALUES ('29', '2', '字典管理', '2', '/sysmgr/dict/frame.do', '3', '5', 'table', 'dict_frame', '基础管理 - 系统管理 - 字典管理', '', '1', '1', '2018-01-14 15:35:42');
INSERT INTO `t_module` VALUES ('30', '-1', '微信管家', '1', '', '1', '2', 'table', '', '微信管家', '', '1', '1', '2018-03-16 09:53:40');
INSERT INTO `t_module` VALUES ('31', '30', '微信管理', '1', '', '2', '1', 'cogs', '', '微信管理 - 微信管理', '', '1', '1', '2018-01-14 15:07:36');
INSERT INTO `t_module` VALUES ('34', '80', '任务管理', '2', '/quartz/task/frame.do', '3', '2', 'database', 'task_frame', '基础管理 - 系统工具 - 任务管理', '', '1', '1', '2018-03-15 10:09:52');
INSERT INTO `t_module` VALUES ('72', '31', '公众号管理', '2', '/weixin/account/frame.do', '3', '1', 'table', 'wx_acct_frame', '微信管理 - 微信管理 - 微信公众号管理', '', '1', '1', '2018-01-14 15:36:28');
INSERT INTO `t_module` VALUES ('80', '1', '系统工具', '1', '', '2', '2', 'cogs', '', '基础管理 - 系统工具', '', '1', '1', '2018-03-15 10:05:12');
INSERT INTO `t_module` VALUES ('82', '80', '系统监控', '2', '/druid/index.html', '3', '9', 'cogs', 'druid_index', '基础管理 - 系统工具 - 系统监控', '', '1', '1', '2018-03-15 10:08:19');
INSERT INTO `t_module` VALUES ('84', '80', '代码生成', '2', '/systools/generator/frame.do', '3', '1', 'cogs', 'generator_frame', '基础管理 - 系统工具 - 代码生成', '', '1', '1', '2018-03-15 10:08:35');
INSERT INTO `t_module` VALUES ('85', '-1', '博客系统', '1', '', '1', '3', 'cogs', '', '博客系统', '', '1', '1', '2018-03-16 09:53:30');
INSERT INTO `t_module` VALUES ('86', '-1', '企业门户', '1', '', '1', '4', 'cogs', '', '企业门户', '', '1', '1', '2018-03-16 09:54:13');
INSERT INTO `t_module` VALUES ('87', '85', '博客管理', '1', '', '2', '1', 'cogs', '', '博客系统 - 博客管理', '', '1', '1', '2018-03-16 16:22:58');
INSERT INTO `t_module` VALUES ('88', '87', '栏目管理', '2', '/blog/category/frame.do', '3', '1', 'cogs', 'category_frame', '博客系统 - 博客管理 - 栏目管理', '', '1', '1', '2018-03-16 16:23:55');
INSERT INTO `t_module` VALUES ('89', '87', '文章管理', '2', '/blog/content/frame.do', '3', '2', 'cogs', 'content_frame', '博客系统 - 博客管理 - 文章管理', '', '1', '1', '2018-03-16 16:24:46');
INSERT INTO `t_module` VALUES ('90', '87', '友情链接', '2', '/blog/friendlink/frame.do', '3', '9', 'cogs', 'blglink_frame', '博客系统 - 博客管理 - 友情链接', '', '1', '1', '2018-03-30 16:11:10');

-- ----------------------------
-- Table structure for t_permit
-- ----------------------------
DROP TABLE IF EXISTS `t_permit`;
CREATE TABLE `t_permit` (
  `permit_id` int(12) NOT NULL AUTO_INCREMENT,
  `permit_name` varchar(100) DEFAULT NULL,
  `module_id` int(12) DEFAULT NULL,
  `permit_url` varchar(100) DEFAULT NULL,
  `permit_code` varchar(64) DEFAULT NULL,
  `operating_code` varchar(32) DEFAULT NULL,
  `operating_time` datetime DEFAULT NULL,
  PRIMARY KEY (`permit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_permit
-- ----------------------------
INSERT INTO `t_permit` VALUES ('4', '工作台', '7', '/home.do', 'admin_home', '1', '2016-07-18 20:43:45');
INSERT INTO `t_permit` VALUES ('5', '退出', '7', '/loginOut.do', 'login_out', '1', '2016-07-18 20:43:45');
INSERT INTO `t_permit` VALUES ('6', '管理首页', '7', '/index.do', 'admin_index', '1', '2018-01-30 14:33:46');
INSERT INTO `t_permit` VALUES ('9', '模块编辑', '3', '/sysmgr/module/edit.do', 'module_edit', '1', '2017-09-02 17:47:10');
INSERT INTO `t_permit` VALUES ('10', '模块保存', '3', '/sysmgr/module/save.do', 'module_save', '1', '2017-09-02 17:51:44');
INSERT INTO `t_permit` VALUES ('11', '模块删除', '3', '/sysmgr/module/delete.do', 'module_delete', '1', '2017-09-02 17:52:03');
INSERT INTO `t_permit` VALUES ('15', '密码修改保存', '12', '/sysmgr/user/saveModifyPwd.do', 'user_pwd_save', '1', '2016-07-19 16:14:51');
INSERT INTO `t_permit` VALUES ('16', '文件上传界面', '14', '/admin/file/edit.do', 'filemgr_edit', '1', '2017-10-24 11:24:09');
INSERT INTO `t_permit` VALUES ('17', '文件上传', '14', '/admin/file/upload.do', 'filemgr_upload', '1', '2017-10-24 11:24:17');
INSERT INTO `t_permit` VALUES ('18', '角色编辑', '13', '/sysmgr/role/edit.do', 'role_edit', '1', '2016-07-20 23:29:36');
INSERT INTO `t_permit` VALUES ('21', '用户编辑', '12', '/sysmgr/user/edit.do', 'user_edit', '1', '2016-07-19 16:09:06');
INSERT INTO `t_permit` VALUES ('22', '用户保存', '12', '/sysmgr/user/save.do', 'user_save', '1', '2017-10-17 20:42:01');
INSERT INTO `t_permit` VALUES ('23', '用户删除', '12', '/sysmgr/user/delete.do', 'user_delete', '1', '2017-10-17 20:41:46');
INSERT INTO `t_permit` VALUES ('24', '用户角色', '12', '/sysmgr/user/userRole.do', 'user_role', '1', '2016-07-19 16:12:15');
INSERT INTO `t_permit` VALUES ('25', '用户角色保存', '12', '/sysmgr/user/saveUserRole.do', 'user_role_save', '1', '2016-07-19 16:12:54');
INSERT INTO `t_permit` VALUES ('27', '密码修改', '12', '/sysmgr/user/modifyPwd.do', 'user_modify_pwd', '1', '2016-07-19 16:14:20');
INSERT INTO `t_permit` VALUES ('35', '角色保存', '13', '/sysmgr/role/save.do', 'role_save', '1', '2017-10-17 20:46:44');
INSERT INTO `t_permit` VALUES ('36', '角色删除', '13', '/sysmgr/role/delete.do', 'role_delete', '1', '2017-10-17 20:47:04');
INSERT INTO `t_permit` VALUES ('38', '角色资源树', '13', '/sysmgr/role/permitTree.do', 'permit_tree', '1', '2017-10-17 20:48:20');
INSERT INTO `t_permit` VALUES ('41', '角色资源保存', '13', '/sysmgr/role/saveRolePermit.do', 'role_save_rolepermit', '1', '2016-07-20 23:33:22');
INSERT INTO `t_permit` VALUES ('43', '网站信息保存', '28', '/sysmgr/site/save.do', 'site_save_info', '1', '2017-10-24 10:23:50');
INSERT INTO `t_permit` VALUES ('45', '字典编辑', '29', '/sysmgr/dict/edit.do', 'dict_edit', '1', '2016-08-23 18:45:32');
INSERT INTO `t_permit` VALUES ('46', '字典保存', '29', '/sysmgr/dict/saveInfo.do', 'dict_save', '1', '2016-08-23 18:46:17');
INSERT INTO `t_permit` VALUES ('52', '模块列表', '3', '/sysmgr/module/list.do', 'module_list', '1', '2017-09-02 17:29:31');
INSERT INTO `t_permit` VALUES ('53', '资源代码校验', '3', '/sysmgr/module/checkCode.do', 'permit_chk_code', '1', '2017-09-02 17:53:12');
INSERT INTO `t_permit` VALUES ('54', '资源管理', '3', '/sysmgr/permit/frame.do', 'permit_frame', '1', '2017-09-02 17:53:57');
INSERT INTO `t_permit` VALUES ('55', '资源列表', '3', '/sysmgr/permit/list.do', 'permit_list', '1', '2017-09-02 17:54:25');
INSERT INTO `t_permit` VALUES ('56', '资源编辑', '3', '/sysmgr/permit/edit.do', 'permit_edit', '1', '2017-09-02 17:55:02');
INSERT INTO `t_permit` VALUES ('57', '资源保存', '3', '/sysmgr/permit/save.do', 'permit_save', '1', '2017-09-02 17:55:28');
INSERT INTO `t_permit` VALUES ('58', '资源删除', '3', '/sysmgr/permit/delete.do', 'permit_delete', '1', '2017-09-02 17:55:58');
INSERT INTO `t_permit` VALUES ('60', '操作日志列表', '8', '/sysmgr/log/list.do', 'operating_list', '1', '2017-10-17 20:40:14');
INSERT INTO `t_permit` VALUES ('61', '用户列表', '12', '/sysmgr/user/list.do', 'user_list', '1', '2017-10-17 20:42:38');
INSERT INTO `t_permit` VALUES ('62', '角色列表', '13', '/sysmgr/role/list.do', 'role_list', '1', '2017-10-17 20:46:23');
INSERT INTO `t_permit` VALUES ('63', '角色名称校验', '13', '/sysmgr/role/checkRoleName.do', 'chk_role_name', '1', '2017-10-17 20:49:11');
INSERT INTO `t_permit` VALUES ('64', '用户名校验', '12', '/sysmgr/user/checkUserName.do', 'chk_user_name', '1', '2017-10-17 20:50:10');
INSERT INTO `t_permit` VALUES ('65', '模块角色', '3', '/sysmgr/module/role.do', 'module_role', '1', '2017-10-17 20:52:30');
INSERT INTO `t_permit` VALUES ('66', '资源角色', '3', '/sysmgr/permit/role.do', 'permit_role', '1', '2017-10-17 20:52:56');
INSERT INTO `t_permit` VALUES ('67', '保存模块角色', '3', '/sysmgr/module/saveModuleRole.do', 'save_module_role', '1', '2017-10-17 20:53:32');
INSERT INTO `t_permit` VALUES ('68', '保存资源角色', '3', '/sysmgr/permit/savePermitRole.do', 'save_permit_role', '1', '2017-10-17 20:54:27');
INSERT INTO `t_permit` VALUES ('74', '任务列表', '34', '/quartz/task/list.do', 'task_list', '1', '2017-11-21 10:29:30');
INSERT INTO `t_permit` VALUES ('75', '任务编辑', '34', '/quartz/task/edit.do', 'task_edit', '1', '2017-11-21 10:30:02');
INSERT INTO `t_permit` VALUES ('76', '任务保存', '34', '/quartz/task/saveTask.do', 'task_save', '1', '2017-11-21 10:30:29');
INSERT INTO `t_permit` VALUES ('77', '任务删除', '34', '/quartz/task/deleteTask.do', 'task_delete', '1', '2017-11-21 10:30:57');
INSERT INTO `t_permit` VALUES ('78', '启动任务', '34', '/quartz/task/startTask.do', 'task_start', '1', '2017-11-21 10:31:22');
INSERT INTO `t_permit` VALUES ('79', '停止任务', '34', '/quartz/task/stopTask.do', 'task_stop', '1', '2017-11-21 10:31:45');
INSERT INTO `t_permit` VALUES ('83', '获取菜单', '7', '/module.do', 'get_module', '1', '2018-01-30 14:32:22');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `role_id` int(12) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `show_order` int(8) DEFAULT NULL,
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` varchar(32) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '超级管理员', '1', '1', null, '2017-09-02 18:49:05', null, '2017-09-02 18:49:05');
INSERT INTO `t_role` VALUES ('2', '系统管理员', '1', '2', null, '2017-09-02 18:49:05', null, '2017-09-02 18:49:05');
INSERT INTO `t_role` VALUES ('3', '普通用户', '1', '3', null, '2017-10-11 15:07:22', '1', '2017-10-11 15:07:22');
INSERT INTO `t_role` VALUES ('5', '匿名用户', '1', '4', null, '2017-09-02 18:49:05', null, '2017-09-02 18:49:05');

-- ----------------------------
-- Table structure for t_role_module
-- ----------------------------
DROP TABLE IF EXISTS `t_role_module`;
CREATE TABLE `t_role_module` (
  `role_id` int(12) DEFAULT NULL,
  `module_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_module
-- ----------------------------
INSERT INTO `t_role_module` VALUES ('2', '1');
INSERT INTO `t_role_module` VALUES ('2', '2');
INSERT INTO `t_role_module` VALUES ('2', '14');
INSERT INTO `t_role_module` VALUES ('2', '8');
INSERT INTO `t_role_module` VALUES ('2', '29');
INSERT INTO `t_role_module` VALUES ('2', '12');
INSERT INTO `t_role_module` VALUES ('2', '28');
INSERT INTO `t_role_module` VALUES ('2', '30');
INSERT INTO `t_role_module` VALUES ('2', '31');
INSERT INTO `t_role_module` VALUES ('3', '14');
INSERT INTO `t_role_module` VALUES ('2', '72');
INSERT INTO `t_role_module` VALUES ('3', '72');
INSERT INTO `t_role_module` VALUES ('5', '72');
INSERT INTO `t_role_module` VALUES ('1', '1');
INSERT INTO `t_role_module` VALUES ('1', '2');
INSERT INTO `t_role_module` VALUES ('1', '12');
INSERT INTO `t_role_module` VALUES ('1', '13');
INSERT INTO `t_role_module` VALUES ('1', '14');
INSERT INTO `t_role_module` VALUES ('1', '28');
INSERT INTO `t_role_module` VALUES ('1', '8');
INSERT INTO `t_role_module` VALUES ('1', '29');
INSERT INTO `t_role_module` VALUES ('1', '3');
INSERT INTO `t_role_module` VALUES ('1', '80');
INSERT INTO `t_role_module` VALUES ('1', '34');
INSERT INTO `t_role_module` VALUES ('1', '82');
INSERT INTO `t_role_module` VALUES ('1', '84');
INSERT INTO `t_role_module` VALUES ('1', '30');
INSERT INTO `t_role_module` VALUES ('1', '31');
INSERT INTO `t_role_module` VALUES ('1', '72');
INSERT INTO `t_role_module` VALUES ('1', '85');
INSERT INTO `t_role_module` VALUES ('1', '87');
INSERT INTO `t_role_module` VALUES ('1', '88');
INSERT INTO `t_role_module` VALUES ('1', '89');
INSERT INTO `t_role_module` VALUES ('1', '86');
INSERT INTO `t_role_module` VALUES ('1', '90');

-- ----------------------------
-- Table structure for t_role_permit
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permit`;
CREATE TABLE `t_role_permit` (
  `role_id` int(12) DEFAULT NULL,
  `permit_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_permit
-- ----------------------------
INSERT INTO `t_role_permit` VALUES ('2', '16');
INSERT INTO `t_role_permit` VALUES ('2', '17');
INSERT INTO `t_role_permit` VALUES ('2', '45');
INSERT INTO `t_role_permit` VALUES ('2', '46');
INSERT INTO `t_role_permit` VALUES ('2', '21');
INSERT INTO `t_role_permit` VALUES ('2', '27');
INSERT INTO `t_role_permit` VALUES ('2', '22');
INSERT INTO `t_role_permit` VALUES ('2', '24');
INSERT INTO `t_role_permit` VALUES ('2', '15');
INSERT INTO `t_role_permit` VALUES ('2', '23');
INSERT INTO `t_role_permit` VALUES ('2', '25');
INSERT INTO `t_role_permit` VALUES ('2', '43');
INSERT INTO `t_role_permit` VALUES ('2', '4');
INSERT INTO `t_role_permit` VALUES ('2', '5');
INSERT INTO `t_role_permit` VALUES ('2', '6');
INSERT INTO `t_role_permit` VALUES ('3', '27');
INSERT INTO `t_role_permit` VALUES ('3', '4');
INSERT INTO `t_role_permit` VALUES ('3', '5');
INSERT INTO `t_role_permit` VALUES ('3', '6');
INSERT INTO `t_role_permit` VALUES ('2', '83');
INSERT INTO `t_role_permit` VALUES ('3', '83');
INSERT INTO `t_role_permit` VALUES ('5', '83');
INSERT INTO `t_role_permit` VALUES ('1', '23');
INSERT INTO `t_role_permit` VALUES ('1', '21');
INSERT INTO `t_role_permit` VALUES ('1', '27');
INSERT INTO `t_role_permit` VALUES ('1', '22');
INSERT INTO `t_role_permit` VALUES ('1', '25');
INSERT INTO `t_role_permit` VALUES ('1', '24');
INSERT INTO `t_role_permit` VALUES ('1', '15');
INSERT INTO `t_role_permit` VALUES ('1', '61');
INSERT INTO `t_role_permit` VALUES ('1', '64');
INSERT INTO `t_role_permit` VALUES ('1', '36');
INSERT INTO `t_role_permit` VALUES ('1', '38');
INSERT INTO `t_role_permit` VALUES ('1', '35');
INSERT INTO `t_role_permit` VALUES ('1', '41');
INSERT INTO `t_role_permit` VALUES ('1', '18');
INSERT INTO `t_role_permit` VALUES ('1', '62');
INSERT INTO `t_role_permit` VALUES ('1', '63');
INSERT INTO `t_role_permit` VALUES ('1', '16');
INSERT INTO `t_role_permit` VALUES ('1', '17');
INSERT INTO `t_role_permit` VALUES ('1', '43');
INSERT INTO `t_role_permit` VALUES ('1', '60');
INSERT INTO `t_role_permit` VALUES ('1', '45');
INSERT INTO `t_role_permit` VALUES ('1', '46');
INSERT INTO `t_role_permit` VALUES ('1', '11');
INSERT INTO `t_role_permit` VALUES ('1', '10');
INSERT INTO `t_role_permit` VALUES ('1', '52');
INSERT INTO `t_role_permit` VALUES ('1', '53');
INSERT INTO `t_role_permit` VALUES ('1', '55');
INSERT INTO `t_role_permit` VALUES ('1', '56');
INSERT INTO `t_role_permit` VALUES ('1', '57');
INSERT INTO `t_role_permit` VALUES ('1', '58');
INSERT INTO `t_role_permit` VALUES ('1', '54');
INSERT INTO `t_role_permit` VALUES ('1', '9');
INSERT INTO `t_role_permit` VALUES ('1', '65');
INSERT INTO `t_role_permit` VALUES ('1', '66');
INSERT INTO `t_role_permit` VALUES ('1', '67');
INSERT INTO `t_role_permit` VALUES ('1', '68');
INSERT INTO `t_role_permit` VALUES ('1', '74');
INSERT INTO `t_role_permit` VALUES ('1', '75');
INSERT INTO `t_role_permit` VALUES ('1', '76');
INSERT INTO `t_role_permit` VALUES ('1', '77');
INSERT INTO `t_role_permit` VALUES ('1', '78');
INSERT INTO `t_role_permit` VALUES ('1', '79');
INSERT INTO `t_role_permit` VALUES ('1', '4');
INSERT INTO `t_role_permit` VALUES ('1', '5');
INSERT INTO `t_role_permit` VALUES ('1', '6');
INSERT INTO `t_role_permit` VALUES ('1', '83');

-- ----------------------------
-- Table structure for t_sequence
-- ----------------------------
DROP TABLE IF EXISTS `t_sequence`;
CREATE TABLE `t_sequence` (
  `seq_name` varchar(64) NOT NULL,
  `seq_value` int(16) DEFAULT NULL,
  `seq_step` int(8) DEFAULT NULL,
  PRIMARY KEY (`seq_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sequence
-- ----------------------------
INSERT INTO `t_sequence` VALUES ('seq_common_id', '100777', '1');
INSERT INTO `t_sequence` VALUES ('seq_log_id', '1010752', '1');
INSERT INTO `t_sequence` VALUES ('seq_module_id', '90', '1');
INSERT INTO `t_sequence` VALUES ('seq_role_id', '16', '1');
INSERT INTO `t_sequence` VALUES ('seq_user_id', '1002', '1');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `user_id` varchar(32) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_pwd` varchar(64) NOT NULL,
  `nickname` varchar(64) DEFAULT NULL COMMENT '昵称',
  `real_name` varchar(64) DEFAULT NULL,
  `user_mobile` varchar(32) NOT NULL,
  `user_email` varchar(64) NOT NULL,
  `sex` int(1) DEFAULT '1' COMMENT '1：男  2：女',
  `idcard` varchar(32) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1：有效 0：无效 -1：删除',
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_user` varchar(32) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', '4297f44b13955235245b2497399d7a93', '超级管理员', '常帅', '15961879770', 'admin@xzsofts.com', '1', null, '1', '1', '2016-07-07 11:53:30', '1', '2016-07-19 15:42:01');
INSERT INTO `t_user` VALUES ('2', 'changs', '325a2cc052914ceeb8c19016c091d2ac', '管理员', '管理员', '15961879770', 'admin@xzsofts.com', '1', null, '1', '1', '2016-07-19 15:41:51', '1', '2016-07-19 15:41:51');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `user_id` varchar(32) DEFAULT NULL,
  `role_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('2', '2');
INSERT INTO `t_user_role` VALUES ('2', '3');
INSERT INTO `t_user_role` VALUES ('5', '3');
INSERT INTO `t_user_role` VALUES ('1', '1');
INSERT INTO `t_user_role` VALUES ('1', '2');
INSERT INTO `t_user_role` VALUES ('1', '3');

-- ----------------------------
-- Table structure for blog_category
-- ----------------------------
DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE `blog_category` (
  `category_id` int(12) NOT NULL,
  `category_name` varchar(64) DEFAULT NULL,
  `parent_id` int(12) DEFAULT NULL,
  `category_type` varchar(32) DEFAULT NULL,
  `show_order` int(4) DEFAULT NULL,
  `category_path` varchar(128) DEFAULT NULL,
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_user` varchar(32) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog_category
-- ----------------------------
INSERT INTO `blog_category` VALUES ('206', '开发', '-1', 'news', '1', '开发', '1', '2016-08-25 15:00:28', '1', '2018-03-30 12:21:33');
INSERT INTO `blog_category` VALUES ('207', '数据库', '-1', 'news', '2', '数据库', '1', '2016-08-25 15:03:46', '1', '2018-03-30 12:21:37');
INSERT INTO `blog_category` VALUES ('208', 'Oracle', '207', 'news', '1', '数据库 - Oracle', '1', '2016-08-25 15:04:22', '1', '2018-03-30 12:21:19');
INSERT INTO `blog_category` VALUES ('209', 'Mysql', '207', 'news', '2', '数据库 - Mysql', '1', '2016-08-25 15:05:06', '1', '2018-03-30 12:22:08');
INSERT INTO `blog_category` VALUES ('210', '美图', '-1', 'img', '4', '美图', '1', '2016-08-25 15:05:06', '1', '2018-03-30 12:21:44');
INSERT INTO `blog_category` VALUES ('211', '服务器', '-1', 'news', '3', '服务器', '1', '2017-03-02 23:52:58', '1', '2018-03-30 12:21:41');
INSERT INTO `blog_category` VALUES ('212', '视频', '-1', 'video', '5', '视频', '1', '2017-03-02 23:54:04', '1', '2018-03-30 12:22:00');
INSERT INTO `blog_category` VALUES ('213', '小说', '-1', 'news', '6', '小说', '1', '2017-03-02 23:54:18', '1', '2018-03-30 12:22:04');

-- ----------------------------
-- Table structure for blog_content
-- ----------------------------
DROP TABLE IF EXISTS `blog_content`;
CREATE TABLE `blog_content` (
  `content_id` int(12) NOT NULL,
  `category_id` int(12) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `title_img` varchar(128) DEFAULT NULL,
  `key_words` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `content` longtext COMMENT '内容',
  `status` int(1) DEFAULT '1',
  `views_cnt` int(12) DEFAULT '0',
  `comments_cnt` int(12) DEFAULT '0',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `origin` varchar(100) DEFAULT NULL COMMENT '来源',
  `origin_url` varchar(255) DEFAULT NULL COMMENT '来源url',
  `create_user` varchar(32) DEFAULT NULL COMMENT '发布人',
  `create_time` datetime DEFAULT NULL,
  `modify_user` varchar(32) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for blog_content_file
-- ----------------------------
DROP TABLE IF EXISTS `blog_content_file`;
CREATE TABLE `blog_content_file` (
  `id` int(12) NOT NULL,
  `content_id` int(12) NOT NULL,
  `file_id` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `show_order` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK38D59D6D2FCE4CAE` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for blog_content_tag
-- ----------------------------
DROP TABLE IF EXISTS `blog_content_tag`;
CREATE TABLE `blog_content_tag` (
  `content_id` int(12) NOT NULL,
  `tag` varchar(64) NOT NULL,
  `show_order` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for blog_friendlink
-- ----------------------------
DROP TABLE IF EXISTS `blog_friendlink`;
CREATE TABLE `blog_friendlink` (
  `id` int(12) NOT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `show_order` int(4) DEFAULT NULL,
  `create_id` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modify_id` varchar(32) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_attachment
-- ----------------------------
DROP TABLE IF EXISTS `t_attachment`;
CREATE TABLE `t_attachment` (
  `attachment_id` varchar(64) NOT NULL,
  `attachment_path` varchar(256) NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `file_root` varchar(256) DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  `upload_user` varchar(32) DEFAULT NULL,
  `upload_ip` varchar(32) DEFAULT NULL,
  `download_type` int(2) DEFAULT '1' COMMENT '下载方式【1：下载引擎 2：直接访问 】',
  `attachment_suffix` varchar(32) DEFAULT NULL,
  `attachment_size` int(11) DEFAULT NULL,
  `module_name` varchar(256) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_operating_log
-- ----------------------------
DROP TABLE IF EXISTS `t_operating_log`;
CREATE TABLE `t_operating_log` (
  `seq_id` varchar(32) NOT NULL,
  `req_time` datetime DEFAULT NULL,
  `req_url` varchar(255) DEFAULT NULL,
  `req_referer` varchar(255) DEFAULT NULL,
  `req_method` varchar(32) DEFAULT NULL,
  `req_params` varchar(4000) DEFAULT NULL,
  `user_agent` varchar(400) DEFAULT NULL,
  `client_ip` varchar(64) DEFAULT NULL,
  `client_mac` varchar(64) DEFAULT NULL,
  `client_port` varchar(32) DEFAULT NULL,
  `server_ip` varchar(64) DEFAULT NULL,
  `server_mac` varchar(64) DEFAULT NULL,
  `server_port` varchar(32) DEFAULT NULL,
  `session_id` varchar(64) DEFAULT NULL,
  `req_user` varchar(32) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`seq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_quartz_task
-- ----------------------------
DROP TABLE IF EXISTS `t_quartz_task`;
CREATE TABLE `t_quartz_task` (
  `task_id` int(16) NOT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `cron_expression` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `create_id` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `task_desc` varchar(500) DEFAULT NULL,
  `status` int(2) DEFAULT '1',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_quartz_task_log
-- ----------------------------
DROP TABLE IF EXISTS `t_quartz_task_log`;
CREATE TABLE `t_quartz_task_log` (
  `log_id` varchar(32) NOT NULL,
  `task_id` int(16) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL COMMENT '状态【1：运行中 2：运行成功 3：运行失败 4：上次尚未完结,本次取消】',
  `run_msg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_account
-- ----------------------------
DROP TABLE IF EXISTS `weix_account`;
CREATE TABLE `weix_account` (
  `account_id` varchar(32) NOT NULL COMMENT '主键',
  `account_name` varchar(200) DEFAULT NULL COMMENT '公众帐号名称',
  `account_email` varchar(200) DEFAULT NULL COMMENT '电子邮箱',
  `account_desc` varchar(400) DEFAULT NULL COMMENT '公众帐号描述',
  `weix_id` varchar(32) DEFAULT NULL COMMENT '原始ID',
  `weix_number` varchar(64) DEFAULT NULL COMMENT '公众微信号',
  `weix_type` varchar(8) DEFAULT NULL COMMENT '公众号类型',
  `appid` varchar(32) DEFAULT NULL COMMENT '公众帐号APPID',
  `appsecret` varchar(64) DEFAULT NULL COMMENT '公众帐号APPSECRET',
  `token` varchar(32) DEFAULT NULL,
  `aeskey` varchar(43) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `access_token` varchar(256) DEFAULT NULL,
  `expires_time` varchar(32) DEFAULT '',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_autoresponse
-- ----------------------------
DROP TABLE IF EXISTS `weix_autoresponse`;
CREATE TABLE `weix_autoresponse` (
  `id` varchar(32) NOT NULL,
  `template_id` varchar(32) DEFAULT NULL,
  `req_key` varchar(255) DEFAULT NULL,
  `msg_type` varchar(32) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` int(12) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_expand
-- ----------------------------
DROP TABLE IF EXISTS `weix_expand`;
CREATE TABLE `weix_expand` (
  `id` varchar(32) NOT NULL,
  `class_name` varchar(100) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `template_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_hdrecord
-- ----------------------------
DROP TABLE IF EXISTS `weix_hdrecord`;
CREATE TABLE `weix_hdrecord` (
  `id` varchar(32) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `hd_id` varchar(32) DEFAULT NULL,
  `nick_name` varchar(200) DEFAULT NULL,
  `prize` varchar(8) DEFAULT NULL,
  `open_id` varchar(100) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_huodong
-- ----------------------------
DROP TABLE IF EXISTS `weix_huodong`;
CREATE TABLE `weix_huodong` (
  `id` varchar(32) NOT NULL COMMENT '键主',
  `title` varchar(400) DEFAULT NULL COMMENT '活动名称',
  `description` text COMMENT '活动描述',
  `one_price` varchar(400) DEFAULT NULL COMMENT '一等奖奖品',
  `one_total` int(4) DEFAULT NULL COMMENT '一等奖数量',
  `one_gl` varchar(8) DEFAULT NULL,
  `two_price` varchar(400) DEFAULT NULL COMMENT '二等奖奖品',
  `two_total` int(4) DEFAULT NULL COMMENT '二等奖数量',
  `two_gl` varchar(8) DEFAULT NULL,
  `three_price` varchar(400) DEFAULT NULL COMMENT '三等奖奖品',
  `three_total` int(4) DEFAULT NULL COMMENT '三等奖数量',
  `three_gl` varchar(8) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '结束时间',
  `hd_type` varchar(4) DEFAULT NULL COMMENT '活动类型',
  `count` varchar(4) DEFAULT NULL COMMENT '抽奖次数',
  `account_id` varchar(32) DEFAULT NULL,
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_menu
-- ----------------------------
DROP TABLE IF EXISTS `weix_menu`;
CREATE TABLE `weix_menu` (
  `menu_id` varchar(32) NOT NULL,
  `menu_key` varchar(255) DEFAULT NULL,
  `menu_type` varchar(255) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `parent_id` varchar(32) DEFAULT NULL,
  `menu_url` varchar(255) DEFAULT NULL,
  `msg_type` varchar(32) DEFAULT NULL,
  `show_order` int(4) DEFAULT NULL,
  `template_id` varchar(32) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_id`),
  KEY `FK_astulwpsla864at9igbas3eic` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_newsitem
-- ----------------------------
DROP TABLE IF EXISTS `weix_newsitem`;
CREATE TABLE `weix_newsitem` (
  `id` varchar(32) NOT NULL,
  `template_id` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL COMMENT '外部URL',
  `show_order` int(8) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m8qs8ufeqkk5cx17budto66r0` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_newstemplate
-- ----------------------------
DROP TABLE IF EXISTS `weix_newstemplate`;
CREATE TABLE `weix_newstemplate` (
  `id` varchar(32) NOT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_prizerecord
-- ----------------------------
DROP TABLE IF EXISTS `weix_prizerecord`;
CREATE TABLE `weix_prizerecord` (
  `id` varchar(32) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `hd_id` varchar(32) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `open_id` varchar(100) DEFAULT NULL,
  `nick_name` varchar(200) DEFAULT NULL,
  `prize` varchar(8) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_receive_msg
-- ----------------------------
DROP TABLE IF EXISTS `weix_receive_msg`;
CREATE TABLE `weix_receive_msg` (
  `id` varchar(32) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `from_user` varchar(100) DEFAULT NULL,
  `to_user` varchar(100) DEFAULT NULL,
  `in_message` varchar(2000) DEFAULT NULL,
  `out_message` varchar(2000) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_subscribemsg
-- ----------------------------
DROP TABLE IF EXISTS `weix_subscribemsg`;
CREATE TABLE `weix_subscribemsg` (
  `id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `account_id` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `msg_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `template_id` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for weix_texttemplate
-- ----------------------------
DROP TABLE IF EXISTS `weix_texttemplate`;
CREATE TABLE `weix_texttemplate` (
  `id` varchar(32) NOT NULL,
  `template_name` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_user
-- ----------------------------
DROP TABLE IF EXISTS `weix_user`;
CREATE TABLE `weix_user` (
  `id` varchar(32) NOT NULL,
  `open_id` varchar(100) DEFAULT NULL,
  `subscribe` varchar(4) DEFAULT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `headimgurl` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tagid_list` varchar(255) DEFAULT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_vote
-- ----------------------------
DROP TABLE IF EXISTS `weix_vote`;
CREATE TABLE `weix_vote` (
  `id` varchar(32) NOT NULL,
  `moban` int(2) NOT NULL DEFAULT '1' COMMENT '模板',
  `title` varchar(50) NOT NULL COMMENT '活动标题',
  `fxms` varchar(1000) NOT NULL COMMENT '微信分享描述',
  `wappicurl` varchar(500) NOT NULL COMMENT '微信分享外链图标',
  `tpnub` int(4) DEFAULT '12' COMMENT '每个微信用户可投票数',
  `ipnubs` int(4) DEFAULT '4' COMMENT '同一个IP下每天能投多少票',
  `keyword` varchar(60) NOT NULL COMMENT '活动关键词',
  `account_id` varchar(32) NOT NULL,
  `check1` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `bms` int(10) DEFAULT '0' COMMENT '报名数',
  `tps` int(10) DEFAULT '0' COMMENT '投票数',
  `statdate` datetime DEFAULT NULL COMMENT '报名时间',
  `enddate` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL COMMENT '投票时间',
  `over_time` datetime DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `shuma` text COMMENT '活动说明一内容',
  `shumat` varchar(2000) DEFAULT NULL COMMENT '活动说明标题一',
  `shumb` text COMMENT '活动说明二内容',
  `shumbt` varchar(2000) DEFAULT NULL COMMENT '活动说明二标题',
  `shumc` text COMMENT '活动说明三内容',
  `shumct` varchar(2000) DEFAULT NULL COMMENT '活动说明三标题',
  `xntps` int(10) DEFAULT '0' COMMENT '虚拟投票数',
  `xncheck` int(10) DEFAULT '0' COMMENT '虚拟浏览量',
  `xnbms` int(8) DEFAULT '0' COMMENT '虚拟报名数',
  `wfbmbz` varchar(2000) DEFAULT NULL COMMENT '无法报名帮助',
  `music` varchar(500) DEFAULT NULL,
  `myzps` int(4) NOT NULL DEFAULT '16' COMMENT '每页显示作品数',
  `hftp` int(1) NOT NULL DEFAULT '1' COMMENT '公众号回复编号投票',
  `tpxzmos` int(1) NOT NULL DEFAULT '1' COMMENT '投票限制模式',
  `xz1p` int(1) NOT NULL DEFAULT '0' COMMENT '每个微信号同作品限1票',
  `picnum` int(2) NOT NULL DEFAULT '5' COMMENT '作品图片上传数限制',
  `xzlx` int(1) NOT NULL DEFAULT '0' COMMENT '限制区域投票',
  `area` varchar(2000) DEFAULT NULL COMMENT '限制区域',
  `ydgzbt` varchar(1000) NOT NULL DEFAULT '如何参与活动' COMMENT '引导用户关注的标题',
  `ydgzan` varchar(1000) NOT NULL DEFAULT '详细了解参与方法' COMMENT '引导用户关注按钮名称',
  `dbdhm` varchar(500) DEFAULT NULL COMMENT '页面右下角菜单名称',
  `dbdhurl` varchar(1000) DEFAULT NULL COMMENT '页面右下角菜单链接',
  `spxz` int(1) DEFAULT '0' COMMENT '开启刷票警告锁定',
  `jgfen` int(8) DEFAULT '60' COMMENT '警告分钟',
  `jgpiao` int(8) DEFAULT '300' COMMENT '警告票数',
  `jgtext` text COMMENT '警告内容',
  `sdfen` int(8) DEFAULT '60' COMMENT '锁定分钟',
  `sdpiao` int(8) DEFAULT '600' COMMENT '锁定票数',
  `sdtext` text COMMENT '锁定内容',
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`),
  FULLTEXT KEY `keyword` (`keyword`),
  FULLTEXT KEY `token` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_vote_item
-- ----------------------------
DROP TABLE IF EXISTS `weix_vote_item`;
CREATE TABLE `weix_vote_item` (
  `id` varchar(32) NOT NULL,
  `vid` varchar(32) NOT NULL COMMENT 'vote_id',
  `account_id` varchar(32) DEFAULT NULL,
  `item_name` varchar(64) NOT NULL COMMENT '姓名',
  `vcount` int(11) NOT NULL DEFAULT '0' COMMENT '票数',
  `picurl1` varchar(100) DEFAULT NULL,
  `picurl2` varchar(100) DEFAULT NULL,
  `picurl3` varchar(100) DEFAULT NULL,
  `picurl4` varchar(100) DEFAULT NULL,
  `picurl5` varchar(100) DEFAULT NULL,
  `mobile` varchar(16) NOT NULL COMMENT '电话',
  `item_no` int(4) unsigned NOT NULL COMMENT '选手编号',
  `intro` text NOT NULL COMMENT '参赛宣言',
  `wechat` varchar(200) NOT NULL COMMENT '微信号',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT 'å®¡æ ¸çŠ¶æ€ 0-æœªå®¡æ ¸ 1-å®¡æ ¸åŒæ„Ÿ 2 é”å®š',
  `open_id` varchar(100) DEFAULT NULL COMMENT 'å¾®ä¿¡id',
  `add_time` datetime DEFAULT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  `lock_info` varchar(256) DEFAULT NULL COMMENT 'é”å®šå›žå¤',
  `lock_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weix_vote_record
-- ----------------------------
DROP TABLE IF EXISTS `weix_vote_record`;
CREATE TABLE `weix_vote_record` (
  `id` varchar(32) NOT NULL,
  `item_id` varchar(32) NOT NULL COMMENT 'æŠ•ç¥¨é¡¹ 1,2,3,',
  `vid` varchar(32) NOT NULL,
  `account_id` varchar(32) DEFAULT NULL,
  `open_id` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL,
  `ip` varchar(300) DEFAULT NULL,
  `area` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Function structure for fn_currval
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_currval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_currval`(in_seq_name VARCHAR(50)) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
BEGIN  
  
DECLARE VALUE INTEGER;  
  
SET VALUE = 0;  
  
SELECT seq_value INTO VALUE FROM t_sequence WHERE seq_name = in_seq_name;  
  
RETURN VALUE;  
  
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getcategorypath
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getcategorypath`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getcategorypath`(`in_rootId` varchar(32)) RETURNS varchar(1000) CHARSET utf8
BEGIN
	declare v_p_list varchar(1000);
	declare v_p_temp varchar(1000);
	declare v_p_id varchar(1000);
  set v_p_id = in_rootId;
  while v_p_id is not null do
		if (v_p_list is not null) then
			set v_p_list = CONCAT(v_p_temp,' - ',v_p_list);
		else
			set v_p_list = CONCAT(v_p_temp);
		end if;
		select GROUP_CONCAT(category_name), GROUP_CONCAT(parent_id) into v_p_temp, v_p_id 
		  from blog_category where FIND_IN_SET(category_id, v_p_id)>0;
	end while;
	return v_p_list;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getitemname
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getitemname`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getitemname`(`in_group_id` varchar(64),`in_item_id` varchar(64)) RETURNS varchar(100) CHARSET utf8
BEGIN
	#Routine body goes here...
	DECLARE v_result varchar(100);
	set v_result = '';
	select a.item_name into v_result from t_dict_item a where a.group_id=in_group_id and a.item_id=in_item_id;
	
	RETURN v_result;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getmoduleids
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getmoduleids`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getmoduleids`(`in_rootId` varchar(32)) RETURNS varchar(1000) CHARSET utf8
BEGIN
	declare v_p_list varchar(4000);
	declare v_p_id varchar(1000);
  set v_p_id = in_rootId;
  while v_p_id is not null do
		if (v_p_list is not null) then
			set v_p_list = CONCAT(v_p_id,',',v_p_list);
		else
			set v_p_list = CONCAT(v_p_id);
		end if;
		select GROUP_CONCAT(module_id) into v_p_id 
		  from t_module where FIND_IN_SET(parent_id, v_p_id)>0;
	end while;
	return v_p_list;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getmodulename
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getmodulename`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getmodulename`(`in_module_id` int) RETURNS varchar(100) CHARSET utf8
BEGIN
	declare v_module_name varchar(100);
  set v_module_name = '';

	select module_name into v_module_name
    from t_module
   where module_id = in_module_id;

	return v_module_name;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_getmodulepath
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_getmodulepath`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_getmodulepath`(`in_rootId` varchar(32)) RETURNS varchar(1000) CHARSET utf8
BEGIN
	declare v_p_list varchar(1000);
	declare v_p_temp varchar(1000);
	declare v_p_id varchar(1000);
  set v_p_id = in_rootId;
  while v_p_id is not null do
		if (v_p_list is not null) then
			set v_p_list = CONCAT(v_p_temp,' - ',v_p_list);
		else
			set v_p_list = CONCAT(v_p_temp);
		end if;
		select GROUP_CONCAT(module_name), GROUP_CONCAT(parent_id) into v_p_temp, v_p_id 
		  from t_module where FIND_IN_SET(module_id, v_p_id)>0;
	end while;
	return v_p_list;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for fn_nextval
-- ----------------------------
DROP FUNCTION IF EXISTS `fn_nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_nextval`(in_seq_name VARCHAR(50)) RETURNS int(11)
    DETERMINISTIC
BEGIN  
  
UPDATE t_sequence SET seq_value = seq_value + seq_step WHERE seq_name = in_seq_name;  
  
RETURN fn_currval(in_seq_name);
  
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS=1;
