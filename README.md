### 项目介绍
本项目个人开源项目，基于maven构建，使用的技术有spring、springmvc、mybatis、shiro、quartz、redis、jquery、easyui、amazeui、layui等。支持集群部署，session使用redis管理。mybatis二级缓存使用redis。项目已经包括了一些基础的功能，如：用户管理、角色管理、模块管理、权限管理，其中权限支持按钮级别的权限控制。一些做管理项目开发的可以直接拿来用。

项目演示站：[http://www.fenghuox.com](http://www.fenghuox.com)

### 内置功能
1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 模块管理：配置系统菜单，操作权限，按钮权限标识等。
3. 角色管理：角色菜单权限分配、设置角色分配菜单按钮权限。
4. 字典管理：对系统中经常使用的一些较为固定的数据进行维护，如：是否、男女、类别、级别等。
5. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
6. 连接池监视：监视当期系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

### 项目优势
1. 使用 Apache License 2.0 协议，源代码完全开源，无商业限制。
2. 使用目前主流的Java EE开发框架，简单易学，学习成本低。
3. 数据库无限制，目前支持MySql，可扩充Oracle、SQL Server、PostgreSQL、H2等。
4. 模块化设计，层次结构清晰。内置一系列企业信息管理的基础功能。
5. 操作权限控制精密细致，对所有管理链接都进行权限验证，可控制到按钮。
6. 提供在线功能代码生成工具，提高开发效率及质量。
7. 提供常用工具类封装，日志、缓存、验证、字典、网络请求等，常用标签（taglib）。
8. 兼容目前最流行浏览器（IE8及以上、Chrome、Firefox）。

### 技术选型
1、后端
- 核心框架：Spring Framework
- 安全框架：Apache Shiro
- 视图框架：Spring MVC
- 模板框架：Freemarker
- 任务调度：Quartz
- 持久层框架：MyBatis
- 数据库连接池：Alibaba Druid
- 缓存框架：Redis
- 日志管理：SLF4J、Log4j2
- 工具类：Apache Commons、Jackson、POI、gson等

2、前端
- JS框架：jQuery、easyui。
- CSS框架：amazeui、easyui。
- 客户端验证：easyui
- 富文本在线编辑：uEditor
- 对话框：layer
- 树结构控件：jQuery zTree
- 日期控件： My97DatePicker

4、平台
- 服务器中间件：在Java EE 5规范（Servlet 2.5、JSP 2.1）下开发，支持应用服务器中间件 有Tomcat 6+、Jboss 7+、WebLogic 10+、WebSphere 8+。
- 数据库支持：目前仅提供MySql数据库的支持，但不限于数据库，你可以方便的更改为其它数据库，如：Oracle、SqlServer 2008、MySql 5.5、H2等
- 开发环境：Intellij Idea、Maven、Git、Jrebel

### 安全考虑
- 开发语言：系统采用Java 语言开发，具有卓越的通用性、高效性、平台移植性和安全性。
- 分层设计：数据库层，数据访问层，业务逻辑层，展示层 层次清楚，低耦合。
- 安全编码：用户表单提交所有数据，在服务器端都进行安全编码，防止用户提交非法脚本及SQL注入获取敏感数据等，确保数据安全。
- 密码加密：登录用户密码进行MD5加密，此加密方法是不可逆的。保证密文泄露后的安全问题。
- 强制访问：系统对所有管理端链接都进行用户身份权限验证，防止用户直接填写url进行访问。

### 快速体验
1. 具备运行环境：Intellij Idea或myeclipse、JDK1.7+、Maven3.0+、MySql5+。
2. 修改cs-main\src\main\webapp\WEB-INF\config\jdbc.properties文件中的数据库及redis设置参数。
3. 根据修改参数创建对应MySql数据库用户，导入sql脚本。
4. 启动Web服务器（第一次运行，需要下载依赖jar包，请耐心等待）。
5. 最高管理员账号，用户名：admin 密码：123123
