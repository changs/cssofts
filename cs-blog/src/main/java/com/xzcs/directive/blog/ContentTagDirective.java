package com.xzcs.directive.blog;

import com.xzcs.mapper.blog.content.ContentMapper;
import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 博客标签
 * @author: chang
 * @date: 2017/6/16
 */
@Component("fl_blog_tag_list")
public class ContentTagDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ContentMapper contentMapper;

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		String contentId = DirectiveUtils.getString("contentId", params);
		List<Map<String, Object>> list = contentMapper.findContentTag(contentId);
		env.setVariable(DirectiveUtils.OUT_LIST, DirectiveUtils.getDefaultObjectWrapper().wrap(list));

		body.render(env.getOut());
	}
}
