package com.xzcs.directive.blog;

import com.xzcs.mapper.blog.category.CategoryMapper;
import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 博客导航
 * @author: chang
 * @date: 2017/6/16
 */
@Component("fl_blog_category")
public class CategoryDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CategoryMapper categoryMapper;

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		String categoryId = DirectiveUtils.getString("categoryId", params);
		Map<String, Object> map = categoryMapper.findCategory(categoryId);
		env.setVariable(DirectiveUtils.OUT_BEAN, DirectiveUtils.getDefaultObjectWrapper().wrap(map));

		body.render(env.getOut());
	}
}
