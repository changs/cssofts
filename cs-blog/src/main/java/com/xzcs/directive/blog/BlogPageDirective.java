package com.xzcs.directive.blog;

import com.xzcs.util.common.URLUtils;
import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 分页
 * @author: chang
 * @date: 2017/6/16
 */
@Component("fl_blog_page")
public class BlogPageDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		String url = DirectiveUtils.getString("url", params);
		int pageSize = DirectiveUtils.getInt("pageSize", params);
		int pageNum = DirectiveUtils.getInt("pageNum", params);
		int total = DirectiveUtils.getInt("total", params);
		int pages = DirectiveUtils.getInt("pages", params);

		StringBuilder sb = new StringBuilder();
		if(total>pageSize){
			//页号越界处理
			if(pageNum > pages){
				pageNum = pages;
			}
			if(pageNum < 1){
				pageNum = 1;
			}
			com.xzcs.util.common.URLUtils.PageInfo pageInfo = URLUtils.getPageInfo(url, null);
			sb.append("<center><div class=\"pagination pagination-lg\"><ul>");
			//上一页处理
			if (pageNum != 1) {
				sb.append("<li><a href=\""+pageInfo.getUrl(1)+"\">首页</a></li>");
				sb.append("<li><a href=\""+pageInfo.getUrl(pageNum - 1)+"\">上一页</a></li>");
			}

			//如果前面页数过多,显示"..."
			int start = 1;
			if(pageNum > 3){
				start = pageNum - 1;
				sb.append("<li><a href=\""+pageInfo.getUrl(1)).append("\">1</a></li>");
				sb.append("<li><a href=\""+pageInfo.getUrl(2)).append("\">2</a></li>");
				sb.append("<li><a href=\""+pageInfo.getUrl(2)).append("\">3</a></li>");
				sb.append("<li><span>&hellip;</span></li>");
			}
			//显示当前页附近的页
			int end = pageNum + 2;
			if(end > pages){
				end = pages;
			}
			for(int i = start; i <= end; i++){
				if(pageNum == i){   //当前页号不需要超链接
					sb.append("<li class=\"active\">")
							.append("<span class=\"current\">"+i+"</span>")
							.append("</li>");
				}else{
					sb.append("<li><a href=\""+pageInfo.getUrl(i))
							.append("\">")
							.append(i)
							.append("</a></li>");
				}
			}
			//如果后面页数过多,显示"..."
			if(end < pages - 3){
				sb.append("<li><span>&hellip;</li></span>");
				for(int i = pages - 2; i <= pages; i++){
					if(pageNum == i){   //当前页号不需要超链接
						sb.append("<li class=\"active\">")
								.append("<span class=\"current\">"+i+"</span>")
								.append("</li>");
					}else{
						sb.append("<li><a href=\""+pageInfo.getUrl(i))
								.append("\">")
								.append(i)
								.append("</a></li>");
					}
				}
			}
			//下一页处理
			if (pageNum != pages) {
				sb.append("<li><a href=\""+pageInfo.getUrl(pageNum + 1)+"\">下一页</a></li>");
				sb.append("<li><a href=\""+pageInfo.getUrl(pages)+"\">尾页</a></li>");
			}

			sb.append("</ul></div></center>");
		}

		env.setVariable(DirectiveUtils.OUT_HTML, DirectiveUtils.getDefaultObjectWrapper().wrap(sb.toString()));

		body.render(env.getOut());
	}
}
