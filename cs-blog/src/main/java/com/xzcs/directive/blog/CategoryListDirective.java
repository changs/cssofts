package com.xzcs.directive.blog;

import com.xzcs.mapper.blog.category.CategoryMapper;
import com.xzcs.mapper.user.UserMapper;
import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 博客导航
 * @author: chang
 * @date: 2017/6/16
 */
@Component("fl_blog_category_list")
public class CategoryListDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CategoryMapper categoryMapper;

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		String parentId = DirectiveUtils.getString("parentId", params);
		String status = DirectiveUtils.getString("status", params);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		map.put("status", status);
		List<Map<String, Object>> list = categoryMapper.findCategorys(map);
		env.setVariable(DirectiveUtils.OUT_LIST, DirectiveUtils.getDefaultObjectWrapper().wrap(list));

		body.render(env.getOut());
	}
}
