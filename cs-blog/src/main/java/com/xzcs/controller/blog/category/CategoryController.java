package com.xzcs.controller.blog.category;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.blog.category.CategoryService;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.util.common.URLUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 栏目管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/blog/category/")
public class CategoryController extends BaseController {
	
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private DictMgrService dictMgrService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		return "html/blog/category/frame";
	}
	
	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = categoryService.findCategorys(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}
	
	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> task = categoryService.findCategory(request);
			request.setAttribute("info", task);
		}
		request.setAttribute("parents", categoryService.findAllCategory());
		request.setAttribute("types", dictMgrService.findDictItem("BLOG.CATEGORY.TYPE"));
		request.setAttribute("method", method);
		return "html/blog/category/edit";
	}
	
	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			categoryService.saveCategory(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			categoryService.deleteCategory(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "**.shtml")
	public String index(HttpServletRequest request) {
		String[] params = URLUtils.getParams(URLUtils.getURI(request));
		String categoryId = "";
		if (params.length > 0) {
			categoryId = params[params.length-1];
		}
		String pageNum = URLUtils.getPageNo(URLUtils.getURI(request))+"";
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("categoryId", categoryId);

		return "html/blog/category/news";
	}
}
