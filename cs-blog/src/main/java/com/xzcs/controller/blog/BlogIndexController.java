package com.xzcs.controller.blog;

import com.xzcs.controller.BaseController;
import com.xzcs.util.common.URLUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


/**
 * 首页
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/blog")
public class BlogIndexController extends BaseController {
	
	@RequestMapping(value = "/")
	public String home(HttpServletRequest request) {
		return "html/blog/index";
	}

	@RequestMapping(value = "/index*.shtml")
	public String index(HttpServletRequest request) {
		int pageNum = URLUtils.getPageNo(URLUtils.getURI(request));
		request.setAttribute("pageNum", pageNum);
		return "html/blog/index";
	}

}
