package com.xzcs.controller.blog.duty;

import com.xzcs.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/blog/duty/")
public class DutyController extends BaseController {

	@RequestMapping(value = "mianze.shtml")
	public String mianze(HttpServletRequest request) {
		return "html/blog/duty/mianze";
	}

	@RequestMapping(value = "yinsiquan.shtml")
	public String yinsiquan(HttpServletRequest request) {
		return "html/blog/duty/yinsiquan";
	}

}
