package com.xzcs.controller.blog.friendlink;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.blog.friendlink.FriendlinkService;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author changs
 * @createDate 2018-03-30 16:07:18
 */
@Controller
@RequestMapping("/blog/friendlink/")
public class FriendlinkController extends BaseController {
	@Autowired
	private FriendlinkService friendlinkService;
	
	@RequestMapping(value = "frame.do")
    public String frame(HttpServletRequest request) {
        return "html/blog/friendlink/frame";
    }

    @RequestMapping(value = "list.do")
    public void list(HttpServletRequest request, HttpServletResponse response) {
        PageInfo<Map<String, Object>> page = friendlinkService.findFriendlinks(request);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("total", page.getTotal());
        result.put("rows", page.getList());
        ResponseUtils.writeJson(result, response);
    }

    @RequestMapping(value = "edit.do")
    public String edit(HttpServletRequest request) {
        String method = RequestUtils.getValue(request, "method");
        if (method.equals("edit")) {
            Map<String, Object> task = friendlinkService.findFriendlink(request);
            request.setAttribute("info", task);
        }
        request.setAttribute("method", method);
        return "html/blog/friendlink/edit";
    }

    @RequestMapping(value = "save.do")
    public void save(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            friendlinkService.saveFriendlink(request);
            result.put("result", 1);
            result.put("msg", getMessage("operating.type.success"));
        } catch (Exception e) {
            result.put("result", 0);
            result.put("msg", getMessage("operating.type.error"));
        }
        ResponseUtils.writeJson(result, response);
    }

    @RequestMapping(value = "delete.do")
    public void delete(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            friendlinkService.deleteFriendlink(request);
            result.put("result", 1);
            result.put("msg", getMessage("operating.type.success"));
        } catch (Exception e) {
            result.put("result", 0);
            result.put("msg", getMessage("operating.type.error"));
        }
        ResponseUtils.writeJson(result, response);
    }
	
}
