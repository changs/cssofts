package com.xzcs.controller.blog.content;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.blog.category.CategoryService;
import com.xzcs.service.blog.content.ContentService;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.util.common.URLUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 文章管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/blog/content/")
public class ContentController extends BaseController {
	
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private DictMgrService dictMgrService;
	@Autowired
	private ContentService contentService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		request.setAttribute("status", dictMgrService.findDictItem("BLOG.CONTENT.STATUS"));
		request.setAttribute("categorys", categoryService.findAllCategory());
		return "html/blog/content/frame";
	}
	
	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = contentService.findContents(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}
	
	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> content = contentService.findContent(request);
			request.setAttribute("info", content);
			request.setAttribute("files", contentService.findContentFile(request));
		}
		request.setAttribute("status", dictMgrService.findDictItem("BLOG.CONTENT.STATUS"));
		request.setAttribute("categorys", categoryService.findAllCategory());
		return "html/blog/content/edit";
	}
	
	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			contentService.saveContent(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			contentService.deleteContent(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "**.shtml")
	public String index(HttpServletRequest request) {
		String[] params = URLUtils.getParams(URLUtils.getURI(request));
		String contentId = "";
		if (params.length > 0) {
			contentId = params[params.length-1];
		}
		request.setAttribute("contentId", contentId);
		contentService.updateViews(contentId);
		return "html/blog/content/news";
	}
}
