package com.xzcs.controller.blog.search;

import com.xzcs.controller.BaseController;
import com.xzcs.service.blog.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/blog/search/")
public class SearchController extends BaseController {

	@Autowired 
	public SearchService searchService;

	@RequestMapping(value = "find.shtml")
	public String find(HttpServletRequest request) {
		List<Map<String, Object>> list = searchService.searchContents(request);
		request.setAttribute("list", list);
		return "html/blog/find/find";
	}

}
