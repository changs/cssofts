package com.xzcs.mapper.blog.category;

import java.util.List;
import java.util.Map;

public interface CategoryMapper {
	public String findCategoryPath(String categoryId);
	public void insertCategory(Map<String, Object> params);
	public boolean updateCategory(Map<String, Object> params);
	public boolean deleteCategory(String categoryId);
	public Map<String, Object> findCategory(String categoryId);
	public List<Map<String, Object>> findCategorys(Map<String, Object> params);
}
