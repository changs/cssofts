package com.xzcs.mapper.blog.content;

import java.util.List;
import java.util.Map;

public interface ContentMapper {
	public void insertContent(Map<String, Object> params);
	public boolean updateContent(Map<String, Object> params);
	public void insertContentFile(Map<String, Object> params);
	public boolean deleteContent(String contentId);
	public boolean deleteContentFile(String contentId);
	public Map<String, Object> findContent(String contentId);
	public List<Map<String, Object>> findContentFile(String contentId);
	public List<Map<String, Object>> findContents(Map<String, Object> params);
	public boolean deleteContentTag(String contentId);
	public void insertContentTag(Map<String, Object> params);
	public List<Map<String, Object>> findContentTag(String contentId);
	public boolean updateViews(String contentId);
}
