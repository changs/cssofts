package com.xzcs.mapper.blog.friendlink;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author changs
 * @createDate 2018-03-30 16:07:18
 */
public interface FriendlinkMapper {
    public List<Map<String, Object>> findFriendlinks(Map<String, Object> params);
    public Map<String, Object> findFriendlink(String id);
    public boolean deleteFriendlink(String id);
    public void insertFriendlink(Map<String, Object> params);
    public boolean updateFriendlink(Map<String, Object> params);

}
