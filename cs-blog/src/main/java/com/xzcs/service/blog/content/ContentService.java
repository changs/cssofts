package com.xzcs.service.blog.content;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.blog.content.ContentMapper;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ContentService extends BaseService {
	
	@Autowired
	private ContentMapper contentMapper;
	@Autowired
	private SequenceMapper sequenceMapper;

	public PageInfo<Map<String, Object>> findContents(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = contentMapper.findContents(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveContent(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		String contentId = RequestUtils.getValue(request, "contentId");
		String keyWords = RequestUtils.getValue(request, "keyWords");
		String content = request.getParameter("content");
		String[] fileIds = RequestUtils.getValues(request, "fileId");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		params.put("content", content);
		if (method.equals("create")) {
			contentId = sequenceMapper.getSequenceValue("seq_common_id")+"";
			params.put("contentId", contentId);
			contentMapper.insertContent(params);
			for (int i=0; i<fileIds.length; i++){
				Map<String, Object> fileParams = new HashMap<String, Object>();
				fileParams.put("id", sequenceMapper.getSequenceValue("seq_common_id"));
				fileParams.put("contentId", contentId);
				fileParams.put("fileId", fileIds[i]);
				fileParams.put("description", "");
				fileParams.put("showOrder", i);
				contentMapper.insertContentFile(fileParams);
			}
			String[] tags = keyWords.split(" ");
			for (int i = 0; i < tags.length; i++) {
				Map<String, Object> tagParams = new HashMap<String, Object>();
				tagParams.put("contentId", contentId);
				tagParams.put("tag", tags[i]);
				tagParams.put("showOrder", i+1);
				contentMapper.insertContentTag(tagParams);
			}
		} else {
			params.put("contentId", contentId);
			contentMapper.updateContent(params);
			contentMapper.deleteContentFile(contentId);
			for (int i=0; i<fileIds.length; i++){
				Map<String, Object> fileParams = new HashMap<String, Object>();
				fileParams.put("id", sequenceMapper.getSequenceValue("seq_common_id"));
				fileParams.put("contentId", contentId);
				fileParams.put("fileId", fileIds[i]);
				fileParams.put("description", "");
				fileParams.put("showOrder", i);
				contentMapper.insertContentFile(fileParams);
			}
			contentMapper.deleteContentTag(contentId);
			String[] tags = keyWords.split(" ");
			for (int i = 0; i < tags.length; i++) {
				Map<String, Object> tagParams = new HashMap<String, Object>();
				tagParams.put("contentId", contentId);
				tagParams.put("tag", tags[i]);
				tagParams.put("showOrder", i+1);
				contentMapper.insertContentTag(tagParams);
			}
		}
	}

	public Map<String, Object> findContent(String contentId){
		return contentMapper.findContent(contentId);
	}
	public Map<String, Object> findContent(HttpServletRequest request){
		String contentId = RequestUtils.getValue(request, "contentId");
		return contentMapper.findContent(contentId);
	}

	public List<Map<String, Object>> findContentFile(String contentId){
		return contentMapper.findContentFile(contentId);
	}
	public List<Map<String, Object>> findContentFile(HttpServletRequest request){
		String contentId = RequestUtils.getValue(request, "contentId");
		return contentMapper.findContentFile(contentId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteContent(HttpServletRequest request){
		String contentId = RequestUtils.getValue(request, "contentId");
		contentMapper.deleteContent(contentId);
		contentMapper.deleteContentFile(contentId);
	}

	public void updateViews(String contentId){
		contentMapper.updateViews(contentId);
	}
}
