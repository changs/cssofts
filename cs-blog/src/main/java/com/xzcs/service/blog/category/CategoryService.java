package com.xzcs.service.blog.category;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.blog.category.CategoryMapper;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CategoryService extends BaseService {
	
	@Autowired
	private CategoryMapper categoryMapper;
	@Autowired
	private SequenceMapper sequenceMapper;

	public PageInfo<Map<String, Object>> findCategorys(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = categoryMapper.findCategorys(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public List<Map<String, Object>> findAllCategory(){
		Map<String, Object> params = new HashMap<String, Object>();
		List<Map<String, Object>> list = categoryMapper.findCategorys(params);
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveCategory(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		String categoryId = RequestUtils.getValue(request, "categoryId");
		String categoryName = RequestUtils.getValue(request, "categoryName");
		String parentId = RequestUtils.getValue(request, "parentId");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		String categoryPath = categoryMapper.findCategoryPath(parentId);
		if (null != categoryPath) {
			categoryPath = categoryPath + " - " + categoryName;
		} else {
			categoryPath = categoryName;
		}
		params.put("categoryPath", categoryPath);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("create")) {
			categoryId = sequenceMapper.getSequenceValue("seq_common_id")+"";
			params.put("categoryId", categoryId);
			categoryMapper.insertCategory(params);
		} else {
			params.put("categoryId", categoryId);
			categoryMapper.updateCategory(params);
		}
	}

	public Map<String, Object> findCategory(String categoryId){
		return categoryMapper.findCategory(categoryId);
	}

	public Map<String, Object> findCategory(HttpServletRequest request){
		String categoryId = RequestUtils.getValue(request, "categoryId");
		return categoryMapper.findCategory(categoryId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteCategory(HttpServletRequest request){
		String categoryId = RequestUtils.getValue(request, "categoryId");
		categoryMapper.deleteCategory(categoryId);
	}
}
