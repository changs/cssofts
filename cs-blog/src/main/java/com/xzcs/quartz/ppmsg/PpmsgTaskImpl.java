package com.xzcs.quartz.ppmsg;

import com.xzcs.mapper.attachment.AttachmentMapper;
import com.xzcs.mapper.blog.content.ContentMapper;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.quartz.JobTask;
import com.xzcs.util.Constants;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.net.HttpClientUtils;
import com.xzcs.util.spring.PropertyUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.*;

public class PpmsgTaskImpl implements JobTask {
	
	public String mtUrl = "http://www.ppmsg.org/meituimote/";
	public String categoryId = "210";
	
	@Override
	public void run(PropertyUtils propertyUtils) throws Exception {
		
		ContentMapper contentMapper = (ContentMapper)propertyUtils.getSpringBean("contentMapper");
		AttachmentMapper attachmentMapper = (AttachmentMapper)propertyUtils.getSpringBean("attachmentMapper");
		SequenceMapper sequenceMapper = (SequenceMapper)propertyUtils.getSpringBean("sequenceMapper");
		String fileRoot = propertyUtils.getPropertyValue(Constants.DEFAULT_FILE_UPLOAD_PATH);
		
		calcPpmsg(mtUrl, fileRoot, contentMapper, sequenceMapper, attachmentMapper);
	}

	public void calcPpmsg(String url, String imgDir, 
			ContentMapper contentMapper, SequenceMapper sequenceMapper,
			AttachmentMapper attachmentMapper){
		HttpClientUtils http = new HttpClientUtils("GBK");
		String result = http.doGetRequest(url, null, null, null, false);
		Document document = Jsoup.parse(result);
		Elements ulElements = document.getElementsByAttributeValue("class", "photo");
		for (Element ulElement : ulElements) {
			Elements liElements = ulElement.getElementsByTag("li");
			List<Map<String, Object>> cList = new ArrayList<Map<String, Object>>();
			for (Element liElement : liElements) {
				Elements aElements = liElement.getElementsByTag("a");
				Elements imgElements = liElement.getElementsByTag("img");
				
				Map<String, Object> content = new HashMap<String, Object>();
				
				String title = imgElements.get(0).attr("alt");
				String originUrl = mtUrl + aElements.get(0).attr("href");
				content.put("categoryId", categoryId);
				content.put("title", title);
				content.put("keyWords", "美图 美腿");
				content.put("description", title);
				content.put("content", title);
				content.put("status", "0");
				content.put("author", "漂漂美术馆");
				content.put("origin", "漂漂美术馆");
				content.put("originUrl", originUrl);
				content.put("userId", "1");
				content.put("userId", "1");
				/*if (checkContentExists(title, contentMapper)) {
					logger.debug(title+"已采集");
					break;
				}*/
				String contentId = sequenceMapper.getSequenceValue("seq_common_id")+"";
				content.put("contentId", contentId);
				
				String imgSrc = imgElements.get(0).attr("src").replaceAll("\n", "");
				String tmp1 = imgSrc.substring(7);
				String imgPath = "images/ppmsg/mtmt" + tmp1.substring(tmp1.indexOf("/"), tmp1.lastIndexOf("/"));
				String imgName = tmp1.substring(tmp1.lastIndexOf("/")+1);
				File fileDir = new File(imgDir + imgPath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
				
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put("Accept", "image/png, image/svg+xml, image/*;q=0.8, */*;q=0.5");
				headerMap.put("Referer", url);
				headerMap.put("Accept-Language", "zh-CN");
				headerMap.put("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
				headerMap.put("Accept-Encoding", "gzip, deflate");
				headerMap.put("Connection", "Keep-Alive");
				headerMap.put("Pragma", "no-cache");
				//headerMap.put("Host", "img.ppmsg.net");
				
				String attachmentSuffix = "";
		 		if(imgName.lastIndexOf(".") != -1) {
		 			attachmentSuffix = imgName.substring(imgName.lastIndexOf(".") + 1).toLowerCase();
		 		}
		 		String attachmentId = UUID.randomUUID().toString();
		 		String newFileName = attachmentId + "." + attachmentSuffix;//新文件名

				Map<String, Object> attachment = new HashMap<String, Object>();
				attachment.put("attachmentId", attachmentId);
				attachment.put("attachmentPath", imgPath + "/" + newFileName);
				attachment.put("attachmentSuffix", attachmentSuffix);
				attachment.put("fileName", imgName);
				attachment.put("fileRoot", imgDir);
				attachment.put("uploadIp", "127.0.0.1");
				attachment.put("uploadUser", "1");
				attachment.put("moduleName", "文章管理");
				attachment.put("remark", "文章缩略图");
				attachment.put("downloadType", "2");

				try {
					http.getImage(imgSrc, imgDir + imgPath + "/" + newFileName, null, headerMap, null);
				} catch (Exception e) {
					continue;
				}
				
				content.put("titleImg", "/" + imgPath + "/" + newFileName);
				File image = new File(imgDir + imgPath + "/" + newFileName);
				attachment.put("attachmentSize", image.length());
				content.put("attachment", attachment);
				
				try {
					calcPpmsgDetail(originUrl, url, content, imgDir, sequenceMapper);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
				cList.add(content);
			}
			for (Map<String, Object> map : cList) {
				Map<String, Object> attachment = (Map<String, Object>)map.get("attachment");
				String contentId = StringUtils.get(map, "contentId");
				attachmentMapper.insertAttachment(attachment);
				contentMapper.insertContent(map);
				
				List<Map<String, Object>> cp = (List<Map<String, Object>>)map.get("imageFiles");
				for (Map<String, Object> map2 : cp) {
					Map<String, Object> attachment2 = (Map<String, Object>)map2.get("attachment");
					attachmentMapper.insertAttachment(attachment2);
					contentMapper.insertContentFile(map2);
				}

				String[] tags = "美图 美腿".split(" ");
				for (int i = 0; i < tags.length; i++) {
					Map<String, Object> tagParams = new HashMap<String, Object>();
					tagParams.put("contentId", contentId);
					tagParams.put("tag", tags[i]);
					tagParams.put("showOrder", i+1);
					contentMapper.insertContentTag(tagParams);
				}
			}
		}
	}
	
	public void calcPpmsgDetail(String url, String referer, Map<String, Object> content, 
			String imgDir, SequenceMapper sequenceMapper) throws Exception{
		HttpClientUtils http = new HttpClientUtils("GBK");
		Map<String, String> reqHeader = new HashMap<String, String>();
		reqHeader.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		reqHeader.put("Referer", referer);
		reqHeader.put("Accept-Language", "zh-CN");
		reqHeader.put("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
		reqHeader.put("Accept-Encoding", "gzip, deflate");
		reqHeader.put("Connection", "Keep-Alive");
		reqHeader.put("Pragma", "no-cache");
		String result = http.doGetRequest(url, null, null, null, false);
		Document document = Jsoup.parse(result);
		//Element divElement = document.getElementById("imagelist");
		Elements ulElements = document.getElementsByAttributeValue("class", "file");
		Elements imgElements = ulElements.get(0).getElementsByTag("img");
		String contentId = StringUtils.get(content, "contentId");
		List<Map<String, Object>> cp = new ArrayList<Map<String, Object>>();
		int j = 1;
		for (Element imgElement : imgElements) {
			String imgSrc = imgElement.attr("src").replaceAll("\n", "");
			String tmp1 = imgSrc.substring(7);
			
			String imgPath = "images/ppmsg/mtmt" + tmp1.substring(tmp1.indexOf("/"), tmp1.lastIndexOf("/"));
			String imgName = tmp1.substring(tmp1.lastIndexOf("/")+1);
			
			File fileDir = new File(imgDir + imgPath);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("Accept", "image/png, image/svg+xml, image/*;q=0.8, */*;q=0.5");
			headerMap.put("Referer", url);
			headerMap.put("Accept-Language", "zh-CN");
			headerMap.put("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
			headerMap.put("Accept-Encoding", "gzip, deflate");
			headerMap.put("Connection", "Keep-Alive");
			headerMap.put("Pragma", "no-cache");
			//headerMap.put("Host", "img.ppmsg.net");
			
			String attachmentSuffix = "";
	 		if(imgName.lastIndexOf(".") != -1) {
	 			attachmentSuffix = imgName.substring(imgName.lastIndexOf(".") + 1).toLowerCase();
	 		}
	 		String attachmentId = UUID.randomUUID().toString();
	 		String newFileName = attachmentId + "." + attachmentSuffix;//新文件名

			Map<String, Object> attachment = new HashMap<String, Object>();
			attachment.put("attachmentId", attachmentId);
			attachment.put("attachmentPath", imgPath + "/" + newFileName);
			attachment.put("attachmentSuffix", attachmentSuffix);
			attachment.put("fileName", imgName);
			attachment.put("fileRoot", imgDir);
			attachment.put("uploadIp", "127.0.0.1");
			attachment.put("uploadUser", "1");
			attachment.put("moduleName", "文章管理");
			attachment.put("remark", "文章附件");
			attachment.put("downloadType", "2");

			http.getImage(imgSrc, imgDir + imgPath + "/" + newFileName, null, headerMap, null);
			
			Map<String, Object> pImg = new HashMap<String, Object>();
			pImg.put("id", sequenceMapper.getSequenceValue("seq_common_id"));
			pImg.put("contentId", contentId);
			pImg.put("fileId", "/" + imgPath + "/" + newFileName);
			pImg.put("description", imgElement.attr("alt"));
			pImg.put("showOrder", j);
			File image = new File(imgDir + imgPath + "/" + newFileName);
			attachment.put("attachmentSize", image.length());
			pImg.put("attachment", attachment);
			cp.add(pImg);
			j++;
		}
		String tmpUrl = url.substring(0, url.lastIndexOf("."));
		Elements elements = document.getElementsByAttributeValue("class", "image");
		String page = elements.get(0).child(0).html();
		int ipage = StringUtils.toInt(page);
		for (int i = 2; i <= ipage; i++) {
			String nUrl = tmpUrl + "_" + i + ".html";
			result = http.doGetRequest(nUrl, null, null, null, false);
			Document document1 = Jsoup.parse(result);
			Elements ulElements1 = document1.getElementsByAttributeValue("class", "file");
			Elements imgElements1 = ulElements1.get(0).getElementsByTag("img");
			for (Element imgElement : imgElements1) {
				String imgSrc = imgElement.attr("src").replaceAll("\n", "");
				String tmp1 = imgSrc.substring(7);
				
				String imgPath = "images/ppmsg/mtmt" + tmp1.substring(tmp1.indexOf("/"), tmp1.lastIndexOf("/"));
				String imgName = tmp1.substring(tmp1.lastIndexOf("/")+1);
				
				File fileDir = new File(imgDir + imgPath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
				
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put("Accept", "image/png, image/svg+xml, image/*;q=0.8, */*;q=0.5");
				headerMap.put("Referer", nUrl);
				headerMap.put("Accept-Language", "zh-CN");
				headerMap.put("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
				headerMap.put("Accept-Encoding", "gzip, deflate");
				headerMap.put("Connection", "Keep-Alive");
				headerMap.put("Pragma", "no-cache");
				//headerMap.put("Host", "img.ppmsg.net");
				
				String attachmentSuffix = "";
		 		if(imgName.lastIndexOf(".") != -1) {
		 			attachmentSuffix = imgName.substring(imgName.lastIndexOf(".") + 1).toLowerCase();
		 		}
		 		String attachmentId = UUID.randomUUID().toString();
		 		String newFileName = attachmentId + "." + attachmentSuffix;//新文件名

				Map<String, Object> attachment = new HashMap<String, Object>();
				attachment.put("attachmentId", attachmentId);
				attachment.put("attachmentPath", imgPath + "/" + newFileName);
				attachment.put("attachmentSuffix", attachmentSuffix);
				attachment.put("fileName", imgName);
				attachment.put("fileRoot", imgDir);
				attachment.put("uploadIp", "127.0.0.1");
				attachment.put("uploadUser", "1");
				attachment.put("moduleName", "文章管理");
				attachment.put("remark", "文章附件");
				attachment.put("downloadType", "2");

				http.getImage(imgSrc, imgDir + imgPath + "/" + newFileName, null, headerMap, null);
				
				Map<String, Object> pImg = new HashMap<String, Object>();
				pImg.put("id", sequenceMapper.getSequenceValue("seq_common_id"));
				pImg.put("contentId", contentId);
				pImg.put("fileId", "/" + imgPath + "/" + newFileName);
				pImg.put("description", imgElement.attr("alt"));
				pImg.put("showOrder", j);
				File image = new File(imgDir + imgPath + "/" + newFileName);
				attachment.put("attachmentSize", image.length());
				pImg.put("attachment", attachment);
				cp.add(pImg);
				j++;
			}
		}
		content.put("imageFiles", cp);
	}
	
	public boolean checkContentExists(String title, ContentMapper contentMapper) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("categoryId", categoryId);
		params.put("title", title);
		List<Map<String, Object>> list = contentMapper.findContents(params);
		if (list.size() > 0) {
			return true;
		}
		return false;
	}
}
