package com.xzcs.quartz.baidu;

import com.xzcs.mapper.blog.content.ContentMapper;
import com.xzcs.quartz.JobTask;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.net.HttpClientUtils;
import com.xzcs.util.spring.PropertyUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PushUrlTaskImpl implements JobTask {
	
	@Override
	public void run(PropertyUtils propertyUtils) throws Exception {
		ContentMapper contentMapper = (ContentMapper)propertyUtils.getSpringBean("contentMapper");
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("createTime", DateUtils.getToday("yyyy-MM-dd"));
		List<Map<String, Object>> list = contentMapper.findContents(params);
		StringBuffer urls = new StringBuffer();
		if (null != list && list.size() > 0) {
			for (Map<String, Object> map: list) {
				urls.append("http://www.fenghuox.com/cssofts/blog/content/"+
						StringUtils.get(map, "content_id")+".shtml\n");
			}
			HttpClientUtils httpClient = new HttpClientUtils();
			String result = httpClient.doPostRequest("http://data.zz.baidu.com/urls?site=www.fenghuox.com&token=VZp6nOTiB5WPql6g",
					urls.toString(), null, null, false);
			logger.debug("百度站长链接推送结果："+result);
		}
	}

	
}
