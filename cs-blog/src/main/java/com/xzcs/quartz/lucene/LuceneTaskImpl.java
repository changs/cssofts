package com.xzcs.quartz.lucene;

import com.xzcs.mapper.blog.content.ContentMapper;
import com.xzcs.quartz.JobTask;
import com.xzcs.util.Constants;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.lucene.LuceneUtils;
import com.xzcs.util.spring.PropertyUtils;
import org.apache.lucene.document.Document;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LuceneTaskImpl implements JobTask {
	
	@Override
	public void run(PropertyUtils propertyUtils) throws Exception {
		
		ContentMapper contentMapper = (ContentMapper)propertyUtils.getSpringBean("contentMapper");
		String fileRoot = propertyUtils.getPropertyValue(Constants.DEFAULT_FILE_UPLOAD_PATH);
		String indexDir = fileRoot + "lucene";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", "1");
		List<Map<String, Object>> list = contentMapper.findContents(params);
		LuceneUtils.deleteAllIndex(indexDir);
		for (Map<String, Object> map : list) {
			Map<String, String> docMap = new HashMap<String, String>();
			docMap.put("content_id", StringUtils.get(map, "content_id"));
			docMap.put("title", StringUtils.get(map, "title"));
			docMap.put("description", StringUtils.get(map, "description"));
			docMap.put("content", StringUtils.get(map, "content"));
			docMap.put("title_img", StringUtils.get(map, "title_img"));
			docMap.put("category_id", StringUtils.get(map, "category_id"));
			docMap.put("category_name", StringUtils.get(map, "category_name"));
			docMap.put("author", StringUtils.get(map, "author"));
			docMap.put("create_time", StringUtils.get(map, "create_time"));
			docMap.put("views_cnt", StringUtils.get(map, "views_cnt"));
			docMap.put("key_words", StringUtils.get(map, "key_words"));
			
			Document doc = LuceneUtils.createDocument(docMap);
			LuceneUtils.createIndex(doc, indexDir, true);
		}
	}

	
}
