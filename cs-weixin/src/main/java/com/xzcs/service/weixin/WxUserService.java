package com.xzcs.service.weixin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.weixin.WxUserMapper;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WxUserService extends WxBaseService {
	@Autowired
	public WxUserMapper wxUserMapper;

	public PageInfo<Map<String, Object>> findWeixUsers(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxUserMapper.findWxUsers(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findWeixUser(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		return wxUserMapper.findWxUser(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteWeixUser(HttpServletRequest request){
		String openId = RequestUtils.getValue(request, "openId");
		String accountId = RequestUtils.getValue(request, "accountId");
		wxUserMapper.deleteWxUser(openId, accountId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void getWxUserInfo(HttpServletRequest request) throws Exception {
		String accountId = RequestUtils.getValue(request, "accountId");
		String openId = RequestUtils.getValue(request, "openId");
		String id = RequestUtils.getValue(request, "id");

		WxMpService wxService = this.getWxMpService(accountId);
		try {
			WxMpUser wxUser = wxService.getUserService().userInfo(openId);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("subscribe", wxUser.getSubscribe());
			params.put("nickName", wxUser.getNickname());
			params.put("sex", wxUser.getSex());
			params.put("city", wxUser.getCity());
			params.put("country", wxUser.getCountry());
			params.put("province", wxUser.getProvince());
			params.put("language", wxUser.getLanguage());
			params.put("headimgurl", wxUser.getHeadImgUrl());
			params.put("remark", wxUser.getRemark());
			params.put("accountId", accountId);
			params.put("id", id);
			wxUserMapper.updateWxUser(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
