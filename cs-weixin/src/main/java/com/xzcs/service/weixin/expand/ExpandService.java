package com.xzcs.service.weixin.expand;

import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

import javax.servlet.http.HttpServletRequest;

/**
 * @desc: 微信扩展服务
 * @author: chang
 * @date: 2017/10/31
 */
public interface ExpandService {
    public String excute(HttpServletRequest request, WxMpXmlMessage inMessage);
}
