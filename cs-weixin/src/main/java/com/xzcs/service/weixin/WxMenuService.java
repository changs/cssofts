package com.xzcs.service.weixin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.weixin.WxMenuMapper;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class WxMenuService extends WxBaseService {

	@Autowired
	private WxMenuMapper wxMenuMapper;

	public PageInfo<Map<String, Object>> findMenus(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxMenuMapper.findMenus(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public List<Map<String, Object>> findMenuList(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		List<Map<String, Object>> list = wxMenuMapper.findMenus(params);
		return list;
	}

	public List<Map<String, Object>> findParentMenu(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("parentId", "-1");
		List<Map<String, Object>> list = wxMenuMapper.findMenus(params);
		return list;
	}

	public Map<String, Object> findMenu(HttpServletRequest request){
		String menuId = RequestUtils.getValue(request, "menuId");
		return wxMenuMapper.findMenu(menuId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveMenu(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		params.put("menuUrl", RequestUtils.getValue(request, "menuUrl").replaceAll("／", "/"));
		if (method.equals("create")) {
			params.put("menuId", StringUtils.getUUID());
			wxMenuMapper.insertMenu(params);
		} else if (method.equals("edit")){
			wxMenuMapper.updateMenu(params);
		}
	}

	/**
	 * 校验菜单数量
	 * @param request
	 * @return 0 校验通过；1 一级菜单最多3个；2 二级菜单最多5个
     */
	public int checkMenuCount(HttpServletRequest request){
		String accountId = RequestUtils.getValue(request, "accountId");
		String parentId = RequestUtils.getValue(request, "parentId");
		int cnt = wxMenuMapper.checkChildren(accountId, parentId);
		if (parentId.equals("-1")) {
			return cnt >= 3 ? 1 : 0;
		}
		return cnt >= 5 ? 2 : 0;
	}

	public int checkMenuKey(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		return wxMenuMapper.checkMenuKey(params);
	}

	public int checkChildren(HttpServletRequest request){
		String accountId = RequestUtils.getValue(request, "accountId");
		String menuId = RequestUtils.getValue(request, "menuId");
		return wxMenuMapper.checkChildren(accountId, menuId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteMenu(HttpServletRequest request){
		String menuId = RequestUtils.getValue(request, "menuId");
		wxMenuMapper.deleteMenu(menuId);
	}

	public String pushMenu(HttpServletRequest request) throws Exception {
		String accountId = RequestUtils.getValue(request, "accountId");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		List<Map<String, Object>> list = wxMenuMapper.findMenus(params);
		WxMpService wxService = getWxMpService(accountId);
		WxMenu menu = new WxMenu();
		List<WxMenuButton> btn = new ArrayList<WxMenuButton>();
		for (Map<String, Object> menuMap : list) {
			String menuId = StringUtils.get(menuMap, "menu_id");
			String parentId = StringUtils.get(menuMap, "parent_id");
			String menuName = StringUtils.get(menuMap, "menu_name");
			String menuType = StringUtils.get(menuMap, "menu_type");
			String menuKey = StringUtils.get(menuMap, "menu_key");
			String menuUrl = StringUtils.get(menuMap, "menu_url");
			if (parentId.equals("-1")) {
				WxMenuButton wxmb = new WxMenuButton();
				wxmb.setName(menuName);
				wxmb.setKey(menuKey);
				wxmb.setType(menuType);
				wxmb.setUrl(menuUrl);
				List<WxMenuButton> subBtn = new ArrayList<WxMenuButton>();
				for (Map<String, Object> menuMap2 : list) {
					String parentId2 = StringUtils.get(menuMap2, "parent_id");
					if (menuId.equals(parentId2)) {
						WxMenuButton wxmb2 = new WxMenuButton();
						wxmb2.setName(StringUtils.get(menuMap2, "menu_name"));
						wxmb2.setKey(StringUtils.get(menuMap2, "menu_key"));
						wxmb2.setType(StringUtils.get(menuMap2, "menu_type"));
						wxmb2.setUrl(StringUtils.get(menuMap2, "menu_url"));
						subBtn.add(wxmb2);
					}
				}
				wxmb.setSubButtons(subBtn);
				btn.add(wxmb);
			}
		}
		menu.setButtons(btn);
		String result = wxService.getMenuService().menuCreate(menu);
		return result;
	}
}
