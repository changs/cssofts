package com.xzcs.service.weixin;

import com.xzcs.mapper.weixin.WxSubscribeMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class WxSubsMsgService extends BaseService {

	@Autowired
	private WxSubscribeMapper subscribeMapper;

	public Map<String, Object> findSubsMsg(HttpServletRequest request){
		String accountId = RequestUtils.getValue(request, "accountId");
		return subscribeMapper.findSubsMsg(accountId);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveSubsMsg(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		String accountId = RequestUtils.getValue(request, "accountId");
		params.put("id", StringUtils.getUUID());
		params.put("userId", StringUtils.get(user, "user_id"));

		subscribeMapper.deleteSubsMsg(accountId);
		subscribeMapper.insertSubsMsg(params);
	}
}
