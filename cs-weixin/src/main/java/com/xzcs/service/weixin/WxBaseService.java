package com.xzcs.service.weixin;

import com.xzcs.mapper.weixin.WeiXinAccountMapper;
import com.xzcs.mapper.weixin.WxResponseMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class WxBaseService extends BaseService {
	@Autowired
	public WeiXinAccountMapper weiXinAccountMapper;
	@Autowired
	public WxResponseMapper wxResponseMapper;

	public WxMpService getWxMpService(String accountId){
		Map<String, Object> weixAccount = weiXinAccountMapper.findWeixAccount(accountId);
		WxMpInMemoryConfigStorage wxMpConfigStorage = new WxMpInMemoryConfigStorage();
		wxMpConfigStorage.setAppId(StringUtils.get(weixAccount, "appid")); // 设置微信公众号的appid
		wxMpConfigStorage.setSecret(StringUtils.get(weixAccount, "appsecret")); // 设置微信公众号的app corpSecret
		wxMpConfigStorage.setToken(StringUtils.get(weixAccount, "token")); // 设置微信公众号的token
		wxMpConfigStorage.setAesKey(StringUtils.get(weixAccount, "aeskey")); // 设置微信公众号的EncodingAESKey
		wxMpConfigStorage.setAccessToken(StringUtils.get(weixAccount, "access_token"));
		wxMpConfigStorage.setExpiresTime(StringUtils.toLong(StringUtils.get(weixAccount, "expires_time")));
		WxMpService wxService = new WxMpServiceImpl();
		wxService.setWxMpConfigStorage(wxMpConfigStorage);
		return wxService;
	}

	public boolean checkSignature(HttpServletRequest request) {
		String acctId = RequestUtils.getValue(request, "acctId");
		String signature = RequestUtils.getValue(request, "signature");
		//String echostr = RequestUtils.getValue(request, "echostr");
		String timestamp = RequestUtils.getValue(request, "timestamp");
		String nonce = RequestUtils.getValue(request, "nonce");
		//String encryptType = RequestUtils.getValue(request, "encrypt_type");
		//encryptType = encryptType.equals("")?"raw":encryptType;//消息加密类型，raw明文
		WxMpService wxService = this.getWxMpService(acctId);
		boolean checkResult = wxService.checkSignature(timestamp, nonce, signature);
		return checkResult;
	}

	public WxMpXmlOutMessage getOutTextMsg(String content, WxMpXmlMessage inMessage){
		WxMpXmlOutMessage outMessage = WxMpXmlOutMessage.TEXT()
				.content(content)
				.fromUser(inMessage.getToUser())
				.toUser(inMessage.getFromUser()).build();
		return outMessage;
	}

	public String getOauth2buildAuthorizationUrl(String accountId, String url){
		WxMpService wxService = this.getWxMpService(accountId);
		return wxService.oauth2buildAuthorizationUrl(url, WxConsts.OAUTH2_SCOPE_USER_INFO, "STATE");
	}

	public WxMpOAuth2AccessToken oauth2getAccessToken(String accountId, String code){
		WxMpService wxService = this.getWxMpService(accountId);
		WxMpOAuth2AccessToken wxMpOAuth2AccessToken = null;
		try {
			wxMpOAuth2AccessToken = wxService.oauth2getAccessToken(code);
		} catch (WxErrorException e) {
			e.printStackTrace();
		}
		return wxMpOAuth2AccessToken;
	}

	public WxMpUser oauth2getUserInfo(String accountId, WxMpOAuth2AccessToken wxMpOAuth2AccessToken){
		WxMpService wxService = this.getWxMpService(accountId);
		WxMpUser wxMpUser = null;
		try {
			wxMpUser = wxService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);
		} catch (WxErrorException e) {
			e.printStackTrace();
		}
		return wxMpUser;
	}

}
