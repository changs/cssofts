package com.xzcs.service.weixin.expand;

import com.xzcs.util.common.GsonUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.net.HttpClientUtils;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @desc: 天气扩展实现
 * @author: chang
 * @date: 2017/10/31
 */
public class TianQiServiceImpl implements ExpandService {
    public String MOBI_TQ_URL = "http://apicloud.mob.com/v1/weather/query?key=1ca5296eb3fec&city=%s&province=";
    @Override
    public String excute(HttpServletRequest request, WxMpXmlMessage inMessage) {
        String responseMsg = "";
        String content = inMessage.getContent();
        StringBuffer buffer = new StringBuffer();
        if (content.startsWith("天气")) {
            String city = content.replaceAll("天气", "").trim();
            HttpClientUtils http = new HttpClientUtils();
            String result = http.doGetRequest(String.format(MOBI_TQ_URL, city), null, null, null, false);
            buffer.append("今日"+city+"天气情况").append("\n");
            Map<String, Object> tianq = (Map<String, Object>) GsonUtils.stringToMap(result);
            String retCode = StringUtils.get(tianq, "retCode");
            if (retCode.equals("200")) {
                List<Map<String, Object>> list = (List<Map<String, Object>>)tianq.get("result");
                for (Map<String, Object> info:list){
                    String distrct = StringUtils.get(info, "distrct");
                    buffer.append("区县："+distrct).append("\n");
                    String week = StringUtils.get(info, "week");
                    buffer.append("星期："+week).append("\n");
                    String temperature = StringUtils.get(info, "temperature");
                    buffer.append("温度："+temperature).append("\n");
                    String weather = StringUtils.get(info, "weather");
                    buffer.append("天气："+weather).append("\n");
                    String wind = StringUtils.get(info, "wind");
                    buffer.append("风向："+wind).append("\n");
                    String airCondition = StringUtils.get(info, "airCondition");
                    buffer.append("空气质量："+airCondition).append("\n");
                    String coldIndex = StringUtils.get(info, "coldIndex");
                    buffer.append("感冒指数："+coldIndex).append("\n");
                    //String city = StringUtils.get(info, "city");
                    String dressingIndex = StringUtils.get(info, "dressingIndex");
                    buffer.append("穿衣指数："+dressingIndex).append("\n");
                    String exerciseIndex = StringUtils.get(info, "exerciseIndex");
                    buffer.append("运动指数："+exerciseIndex).append("\n");
                    String washIndex = StringUtils.get(info, "washIndex");
                    buffer.append("洗车指数："+washIndex).append("\n");
                    String humidity = StringUtils.get(info, "humidity");
                    buffer.append("湿度："+humidity).append("\n");
                    String pollutionIndex = StringUtils.get(info, "pollutionIndex");
                    buffer.append("空气质量指数："+pollutionIndex).append("\n");
                    String sunrise = StringUtils.get(info, "sunrise");
                    buffer.append("日出时间："+sunrise).append("\n");
                    String sunset = StringUtils.get(info, "sunset");
                    buffer.append("日落时间："+sunset).append("\n");
                    buffer.append("一周天气情况").append("\n");
                    List<Map<String, Object>> w = (List<Map<String, Object>>)info.get("future");
                    for (Map<String, Object> m : w){
                        String date = StringUtils.get(m, "date");
                        buffer.append("日期："+date).append("\n");
                        String week2 = StringUtils.get(m, "week");
                        buffer.append("星期："+week2).append("\n");
                        String dayTime = StringUtils.get(m, "dayTime");
                        buffer.append("白天天气："+dayTime).append("\n");
                        String night = StringUtils.get(m, "night");
                        buffer.append("晚上天气："+night).append("\n");
                        String temperature2 = StringUtils.get(m, "temperature");
                        buffer.append("温度："+temperature2).append("\n");
                        String wind2 = StringUtils.get(m, "wind");
                        buffer.append("风向："+wind2).append("\n");
                    }
                }
            } else {
                buffer.append("获取不到天气信息");
            }
        } else {
            buffer.append("发送天气+城市，例如'天气徐州'");
        }
        WxMpXmlOutMessage outMessage = WxMpXmlOutMessage.TEXT()
                .content(buffer.toString())
                .fromUser(inMessage.getToUser())
                .toUser(inMessage.getFromUser()).build();
        responseMsg = outMessage.toXml();
        return responseMsg;
    }
}
