package com.xzcs.service.weixin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.weixin.WeixinUtils;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WeiXinAccountService extends WxBaseService {

	public PageInfo<Map<String, Object>> findWeixAccounts(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = weiXinAccountMapper.findWeixAccounts(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findWeixAccount(HttpServletRequest request){
		String accountId = RequestUtils.getValue(request, "accountId");
		return weiXinAccountMapper.findWeixAccount(accountId);
	}

	public Map<String, Object> findWeixAccount(String accountId){
		return weiXinAccountMapper.findWeixAccount(accountId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveWeixAccount(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("edit")) {
			weiXinAccountMapper.updateWeixAccount(params);
		} else if(method.equals("create")){//新增
			params.put("accountId", StringUtils.getUUID());
			params.put("token", WeixinUtils.getToken(32, 0, 1));
			params.put("aeskey", WeixinUtils.getToken(43, 0, 1));
			weiXinAccountMapper.insertWeixAccount(params);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteWeixAccount(HttpServletRequest request){
		String accountId = RequestUtils.getValue(request, "accountId");
		weiXinAccountMapper.deleteWeixAccount(accountId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void resetAccessToken(HttpServletRequest request) throws Exception {
		Map<String, Object> user = this.getUser(request);
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		List<Map<String, Object>> list = weiXinAccountMapper.findWeixAccounts(params);
		for (Map<String, Object> acct:list) {
			String accountId = StringUtils.get(acct, "account_id");
			WxMpService wxService = this.getWxMpService(accountId);
			wxService.getAccessToken(false);
			String accessToken = wxService.getWxMpConfigStorage().getAccessToken();
			long expiresTime = wxService.getWxMpConfigStorage().getExpiresTime();
			Map<String, Object> params1 = new HashMap<String, Object>();
			params1.put("accessToken", accessToken);
			params1.put("expiresTime", expiresTime);
			params1.put("accountId", accountId);
			weiXinAccountMapper.updateAccessToken(params1);
		}
	}
}
