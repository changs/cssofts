package com.xzcs.service.weixin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.weixin.WxTextMsgMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Service
public class WxTextMsgService extends BaseService {

	@Autowired
	private WxTextMsgMapper textMsgMapper;

	public PageInfo<Map<String, Object>> findTextMsgs(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = textMsgMapper.findTextMsgs(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public List<Map<String, Object>> findTextMsgList(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		List<Map<String, Object>> list = textMsgMapper.findTextMsgs(params);
		return list;
	}

	public Map<String, Object> findTextMsg(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		return textMsgMapper.findTextMsg(id);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveTextMsg(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		params.put("content", RequestUtils.getValue(request, "content").replaceAll("／", "/"));
		if (method.equals("create")) {
			params.put("id", StringUtils.getUUID());
			textMsgMapper.insertTextMsg(params);
		} else if (method.equals("edit")){
			textMsgMapper.updateTextMsg(params);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteTextMsg(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		textMsgMapper.deleteTextMsg(id);
	}
}
