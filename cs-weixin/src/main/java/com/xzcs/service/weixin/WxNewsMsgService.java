package com.xzcs.service.weixin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.weixin.WxNewsMsgMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Service
public class WxNewsMsgService extends BaseService {

	@Autowired
	private WxNewsMsgMapper newsMsgMapper;

	public PageInfo<Map<String, Object>> findNewsMsgs(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = newsMsgMapper.findNewsMsgs(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public List<Map<String, Object>> findNewsMsgList(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		List<Map<String, Object>> list = newsMsgMapper.findNewsMsgs(params);
		return list;
	}

	public Map<String, Object> findNewsMsg(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		return newsMsgMapper.findNewsMsg(id);
	}

	public void saveNewsMsg(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));

		if (method.equals("create")) {
			params.put("id", StringUtils.getUUID());
			newsMsgMapper.insertNewsMsg(params);
		} else if (method.equals("edit")){
			newsMsgMapper.updateNewsMsg(params);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteNewsMsg(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		newsMsgMapper.deleteNewsMsg(id);
		newsMsgMapper.deleteNewsItems(id);
	}

	public List<Map<String, Object>> findNewsItem(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		return newsMsgMapper.findNewsItem(id);
	}

	public void deleteNewsItem(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		newsMsgMapper.deleteNewsItem(id);
	}

	public void saveNewsItem(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		params.put("id", StringUtils.getUUID());
		params.put("url", RequestUtils.getValue(request, "url").replaceAll("／", "/"));
		newsMsgMapper.insertNewsItem(params);
	}
}
