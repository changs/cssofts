package com.xzcs.service.weixin.huodong;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.mapper.weixin.huodong.WxHuodongMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WxHuodongService extends BaseService {

	@Autowired
	private WxHuodongMapper wxHuodongMapper;
	@Autowired
	private SequenceMapper sequenceMapper;

	public PageInfo<Map<String, Object>> findHds(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxHuodongMapper.findHds(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findHd(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		return wxHuodongMapper.findHd(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void savehd(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("create")) {
			params.put("id", StringUtils.getUUID());
			wxHuodongMapper.insertHd(params);
		} else if (method.equals("edit")){
			wxHuodongMapper.updateHd(params);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteHd(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		wxHuodongMapper.deleteHd(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void savehdRecord(HttpServletRequest request, int prize){
		String hdId = RequestUtils.getValue(request, "id");
		String openId = RequestUtils.getValue(request, "openId");
		String accountId = RequestUtils.getValue(request, "accountId");
		String nickName = SessionUtils.getAttribute(request.getSession(), "nickName");
		Map<String, Object> params = new HashMap<String, Object>();
		String seqId = DateUtils.getToday("yyyyMMddHHmmss") + sequenceMapper.getSequenceValue("seq_log_id");
		params.put("id", seqId);
		params.put("hdId", hdId);
		params.put("openId", openId);
		params.put("nickName", nickName);
		params.put("accountId", accountId);
		params.put("prize", prize);
		wxHuodongMapper.insertHdRecord(params);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveHdPrize(HttpServletRequest request){
		HttpSession session = request.getSession();
		String hdId = RequestUtils.getValue(request, "id");
		String mobile = RequestUtils.getValue(request, "mobile");
		String accountId = RequestUtils.getValue(request, "accountId");
		int prize = SessionUtils.getAttrInt(session, "prize");
		String openId = SessionUtils.getAttribute(session, "openId");
		String nickName = SessionUtils.getAttribute(session, "nickName");
		Map<String, Object> params = new HashMap<String, Object>();
		String seqId = DateUtils.getToday("yyyyMMddHHmmss") + sequenceMapper.getSequenceValue("seq_log_id");
		params.put("id", seqId);
		params.put("hdId", hdId);
		params.put("openId", openId);
		params.put("nickName", nickName);
		params.put("mobile", mobile);
		params.put("accountId", accountId);
		params.put("prize", prize);
		wxHuodongMapper.insertHdPrize(params);
	}

	public PageInfo<Map<String, Object>> findHdRecords(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxHuodongMapper.findHdRecords(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public PageInfo<Map<String, Object>> findHdPrizes1(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxHuodongMapper.findHdPrizes(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public List<Map<String, Object>> findHdRecords(HttpServletRequest request, String today){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		if (!today.equals("")) {
			params.put("today", today);
		}
		return wxHuodongMapper.findHdRecords(params);
	}

	public List<Map<String, Object>> findHdPrizes(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		return wxHuodongMapper.findHdPrizes(params);
	}

	public int getPrizeCount(HttpServletRequest request, int prize){
		String id = RequestUtils.getValue(request, "id");
		return wxHuodongMapper.getPrizeCount(id, String.valueOf(prize));
	}
}
