package com.xzcs.service.weixin.vote;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.mapper.weixin.vote.WxVoteMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.net.HttpClientUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Service
public class WxVoteService extends BaseService {

	@Autowired
	private WxVoteMapper wxVoteMapper;
	@Autowired
	private SequenceMapper sequenceMapper;

	public PageInfo<Map<String, Object>> findVotes(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxVoteMapper.findVotes(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findVote(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		return wxVoteMapper.findVote(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveVote(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("wappicurl", RequestUtils.getValue(request, "wappicurl").replaceAll("／", "/"));
		params.put("dbdhurl", RequestUtils.getValue(request, "dbdhurl").replaceAll("／", "/"));
		params.put("shuma", request.getParameter("shuma"));
		params.put("shumb", request.getParameter("shumb"));
		params.put("shumc", request.getParameter("shumc"));
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("create")) {
			params.put("id", StringUtils.getUUID());
			wxVoteMapper.insertVote(params);
		} else if (method.equals("edit")){
			wxVoteMapper.updateVote(params);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteVote(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		wxVoteMapper.deleteVote(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void updateVoteCheck(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		wxVoteMapper.updateVoteCheck(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public String saveVoteItem(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		String id = RequestUtils.getValue(request, "id");
		String accountId = RequestUtils.getValue(request, "accountId");
		String openId = SessionUtils.getAttribute(request.getSession(), "openId");
		String[] pics = RequestUtils.getValues(request, "pic");
		Integer itemNo = wxVoteMapper.getMaxVoteItemNo(id, accountId);
		itemNo = (null != itemNo) ? itemNo + 1 : 1;
		String itemId = StringUtils.getUUID();
		params.put("itemId", itemId);
		params.put("itemNo", itemNo);
		params.put("openId", openId);
		for (int i = 0; i < pics.length; i++) {
			params.put("picurl"+(i+1), pics[i]);
		}
		wxVoteMapper.updateVoteBms(id);
		wxVoteMapper.insertVoteItem(params);
		return itemId;
	}

	public PageInfo<Map<String, Object>> findVoteItems(HttpServletRequest request, String pageSize){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? "12" : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = wxVoteMapper.findVoteItems(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findVoteItem(HttpServletRequest request){
		String itemId = RequestUtils.getValue(request, "itemId");
		return wxVoteMapper.findVoteItem(itemId);
	}

	public String getAreaByIp(String ip){
		String api = "http://ip.taobao.com/service/getIpInfo.php?ip=";
		HttpClientUtils http = new HttpClientUtils("GBK");
		String result = http.doGetRequest(api+ip, null, null, null, false);
		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveVoteRecord(HttpServletRequest request, String ip, String area){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		String id = RequestUtils.getValue(request, "id");
		String itemId = RequestUtils.getValue(request, "itemId");
		String openId = SessionUtils.getAttribute(request.getSession(), "openId");
		String seqId = DateUtils.getToday("yyyyMMddHHmmss") + sequenceMapper.getSequenceValue("seq_log_id");
		params.put("recordId", seqId);
		params.put("openId", openId);
		params.put("ip", ip);
		params.put("area", area);
		wxVoteMapper.updateVoteTps(id);
		wxVoteMapper.updateVoteItemCount(itemId);
		wxVoteMapper.insertVoteRecord(params);
	}
}
