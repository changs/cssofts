package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WxAutoRespMapper {
	public void insertAutoMsg(Map<String, Object> params);
	public boolean updateAutoMsg(Map<String, Object> params);
	public boolean deleteAutoMsg(String id);
	public Map<String, Object> findAutoMsg(String id);
	public List<Map<String, Object>> findAutoMsgs(Map<String, Object> params);
}
