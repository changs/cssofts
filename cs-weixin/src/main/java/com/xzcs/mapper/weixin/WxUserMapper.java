package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WxUserMapper {
	public void insertWxUser(Map<String, Object> params);
	public boolean updateWxUser(Map<String, Object> params);
	public boolean deleteWxUser(String openId, String accountId);
	public Map<String, Object> findWxUser(String id);
	public List<Map<String, Object>> findWxUsers(Map<String, Object> params);
}
