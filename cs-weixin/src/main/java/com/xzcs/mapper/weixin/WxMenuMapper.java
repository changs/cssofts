package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WxMenuMapper {
	public void insertMenu(Map<String, Object> params);
	public boolean updateMenu(Map<String, Object> params);
	public boolean deleteMenu(String menuId);
	public Map<String, Object> findMenu(String menuId);
	public List<Map<String, Object>> findMenus(Map<String, Object> params);
	public int checkMenuKey(Map<String, Object> params);
	public int checkChildren(String accountId, String menuId);
}
