package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WxNewsMsgMapper {
	public void insertNewsMsg(Map<String, Object> params);
	public boolean updateNewsMsg(Map<String, Object> params);
	public boolean deleteNewsMsg(String id);
	public void insertNewsItem(Map<String, Object> params);
	public boolean deleteNewsItems(String templateId);
	public boolean deleteNewsItem(String id);
	public Map<String, Object> findNewsMsg(String id);
	public List<Map<String, Object>> findNewsMsgs(Map<String, Object> params);
	public List<Map<String, Object>> findNewsItem(String templateId);
}
