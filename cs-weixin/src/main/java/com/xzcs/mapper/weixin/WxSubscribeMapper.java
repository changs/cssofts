package com.xzcs.mapper.weixin;

import java.util.Map;

public interface WxSubscribeMapper {
	public void insertSubsMsg(Map<String, Object> params);
	public boolean deleteSubsMsg(String accountId);
	public Map<String, Object> findSubsMsg(String accountId);
}
