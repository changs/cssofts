package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WxResponseMapper {
	public List<Map<String, Object>> findAutoResponses(String accountId);
	public Map<String, Object> findTextResponse(String id);
	public List<Map<String, Object>> findNewsResponse(String id);
	public Map<String, Object> findSubsResponse(String accountId);
	public List<Map<String, Object>> findExpands(String accountId);
	public Map<String, Object> findExpand(String id);
}
