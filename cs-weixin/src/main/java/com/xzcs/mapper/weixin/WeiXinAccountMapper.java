package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WeiXinAccountMapper {
	public Map<String, Object> findWeixAccount(String accountId);
	public List<Map<String, Object>> findWeixAccounts(Map<String, Object> params);
	public boolean deleteWeixAccount(String accountId);
	public void insertWeixAccount(Map<String, Object> params);
	public boolean updateWeixAccount(Map<String, Object> params);
	public boolean updateAccessToken(Map<String, Object> params);
}
