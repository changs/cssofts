package com.xzcs.mapper.weixin.huodong;

import java.util.List;
import java.util.Map;

public interface WxHuodongMapper {
	public void insertHd(Map<String, Object> params);
	public boolean updateHd(Map<String, Object> params);
	public boolean deleteHd(String id);
	public Map<String, Object> findHd(String id);
	public List<Map<String, Object>> findHds(Map<String, Object> params);

	public void insertHdRecord(Map<String, Object> params);
	public void insertHdPrize(Map<String, Object> params);
	public List<Map<String, Object>> findHdRecords(Map<String, Object> params);
	public List<Map<String, Object>> findHdPrizes(Map<String, Object> params);
	public int getPrizeCount(String hdId, String prize);
}
