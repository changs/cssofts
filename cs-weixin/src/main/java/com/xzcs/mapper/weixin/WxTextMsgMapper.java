package com.xzcs.mapper.weixin;

import java.util.List;
import java.util.Map;

public interface WxTextMsgMapper {
	public void insertTextMsg(Map<String, Object> params);
	public boolean updateTextMsg(Map<String, Object> params);
	public boolean deleteTextMsg(String id);
	public Map<String, Object> findTextMsg(String id);
	public List<Map<String, Object>> findTextMsgs(Map<String, Object> params);
}
