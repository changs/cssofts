package com.xzcs.mapper.weixin.vote;

import java.util.List;
import java.util.Map;

public interface WxVoteMapper {
	public void insertVote(Map<String, Object> params);
	public boolean updateVote(Map<String, Object> params);
	public boolean updateVoteCheck(String id);
	public boolean updateVoteBms(String id);
	public boolean updateVoteTps(String id);
	public boolean deleteVote(String id);
	public Map<String, Object> findVote(String id);
	public List<Map<String, Object>> findVotes(Map<String, Object> params);

	public void insertVoteItem(Map<String, Object> params);
	public Integer getMaxVoteItemNo(String id, String accountId);
	public List<Map<String, Object>> findVoteItems(Map<String, Object> params);
	public Map<String, Object> findVoteItem(String itemId);
	public boolean updateVoteItemCount(String itemId);

	public void insertVoteRecord(Map<String, Object> params);
	public Integer getVoteRecordCount(Map<String, Object> params);
}
