package com.xzcs.controller.weixin.huodong;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.service.weixin.huodong.WxHuodongService;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 大转盘
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/weixin/huodong/")
public class WxHuodongController extends BaseController {

	@Autowired
	private WxHuodongService wxHuodongService;
	@Autowired
	private DictMgrService dictMgrService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		return "html/weixin/huodong/frame";
	}

	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = wxHuodongService.findHds(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> info = wxHuodongService.findHd(request);
			request.setAttribute("info", info);
		}
		request.setAttribute("hdType", dictMgrService.findDictItem("WEIX.HUODONG.TYPE"));
		return "html/weixin/huodong/edit";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxHuodongService.savehd(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxHuodongService.deleteHd(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "hdRecord.do")
	public String hdRecord(HttpServletRequest request) {
		return "html/weixin/huodong/hdRecord";
	}

	@RequestMapping(value = "hdRecordList.do")
	public void hdRecordList(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = wxHuodongService.findHdRecords(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "hdPrize.do")
	public String hdPrize(HttpServletRequest request) {
		return "html/weixin/huodong/hdPrize";
	}

	@RequestMapping(value = "hdPrizeList.do")
	public void hdPrizeList(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = wxHuodongService.findHdPrizes1(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "index.shtml")
	public String index(HttpServletRequest request) {
		Map<String, Object> info = wxHuodongService.findHd(request);
		request.setAttribute("info", info);
		if (null == info) {
			return "html/weixin/huodong/error";
		}
		String hdType = StringUtils.get(info, "hd_type");
		if (hdType.equals("1")) {//大转盘
			return "html/weixin/huodong/dzp";
		}
		return "html/weixin/huodong/index";
	}

	@RequestMapping(value = "getDzpPrize.shtml")
	public void getDzpPrize(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> hd = wxHuodongService.findHd(request);
		if (null != hd) {
			String hdStatus = StringUtils.get(hd, "hd_status");
			float oneGl = StringUtils.toFloat(StringUtils.get(hd, "one_gl"));
			float twoGl = StringUtils.toFloat(StringUtils.get(hd, "two_gl"));
			float threeGl = StringUtils.toFloat(StringUtils.get(hd, "three_gl"));
			int oneTotal = StringUtils.toInt(StringUtils.get(hd, "one_total"));
			int twoTotal = StringUtils.toInt(StringUtils.get(hd, "two_total"));
			int threeTotal = StringUtils.toInt(StringUtils.get(hd, "three_total"));
			int hdCount = StringUtils.toInt(StringUtils.get(hd, "count"));
			if (hdStatus.equals("0")) {
				result.put("retcode", "0");
				result.put("msg", "活动已结束！");
			} else {
				List<Map<String, Object>> prizes = wxHuodongService.findHdPrizes(request);
				if (prizes.size() > 0) {
					result.put("retcode", "-1");
					result.put("msg", "本次活动你已经中过奖，不能再参加活动！,本次显示上次中奖结果");
					result.put("prize", StringUtils.get(prizes.get(0), "prize"));
				} else {
					List<Map<String, Object>> hdRecords = wxHuodongService.findHdRecords(request, DateUtils.getToday());
					if (hdRecords.size() >= hdCount ){
						result.put("retcode", "-2");
						result.put("msg", "您当天已经抽了 "+hdRecords.size()+" 次奖。");
					} else {
						int prize = createPrize(new float[]{oneGl, twoGl, threeGl});
						int prizeCount = wxHuodongService.getPrizeCount(request, prize); //已中奖数量
						if (prize == 1 && prizeCount >= oneTotal) {//中奖数量大于等于奖品数量，置为不中奖
							prize = 0;
						}
						if (prize == 2 && prizeCount >= twoTotal) {
							prize = 0;
						}
						if (prize == 3 && prizeCount >= threeTotal) {
							prize = 0;
						}
						result.put("retcode", "1");
						result.put("prize", prize);
						HttpSession session  = request.getSession();
						session.setAttribute("prize", prize);
						wxHuodongService.savehdRecord(request, prize);
					}
				}
			}
		} else {
			result.put("retcode", "0");
			result.put("msg", "活动已结束！");
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "savePrize.shtml")
	public void savePrize(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxHuodongService.saveHdPrize(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			e.printStackTrace();
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	public int createPrize(float[] gl){
		//float[] gl = {0.01f, 0.09f, 0.5f};
		int[][] temp = new int[gl.length][3];
		int i = 0;
		int sum = 0;
		for(int k = 0; k < gl.length; k++){
			int odds = (int) (gl[k] * 1000);
			temp[i][0] = sum; //起始值
			sum = sum + odds;
			temp[i][1] = sum; //结束值
			temp[i][2] = k + 1;  //rows index
			i++;
		}
		Random random = new Random();
		sum = sum > 1000 ? sum : 1000;
		int r = random.nextInt(sum);
		int j = 0;
		for(int k = 0; k < i; k++){
			//System.out.println(temp[k][0]+ "   "+ temp[k][1]);
			if(r >= temp[k][0] && r <= temp[k][1]){
				j = temp[k][2];
				break;
			}
		}
		//System.out.println("j:"+j+"   r:"+r);
		return j;
	}
}
