package com.xzcs.controller.weixin;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.service.weixin.WxAutoRespService;
import com.xzcs.service.weixin.WxNewsMsgService;
import com.xzcs.service.weixin.WxTextMsgService;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自动回复管理
 * @author chang
 * @date 2016-08-14 15:42
 */
@Controller
@RequestMapping(value = "/weixin/autoresp/")
public class WxAutoRespController extends BaseController {

	@Autowired
	private WxTextMsgService textMsgService;
	@Autowired
	private WxNewsMsgService newsMsgService;
	@Autowired
	private WxAutoRespService autoRespService;
	@Autowired
	private DictMgrService dictMgrService;

	@RequestMapping(value = "getMsgs.do")
	public void getMsgs(HttpServletRequest request, HttpServletResponse response) {
		String msgType = RequestUtils.getValue(request, "msgType");
		List<Map<String, Object>> msgs = new ArrayList<Map<String, Object>>();
		if (msgType.equals("text")) {
			msgs = textMsgService.findTextMsgList(request);
		} else if (msgType.equals("news")) {
			msgs = newsMsgService.findNewsMsgList(request);
		} else if (msgType.equals("expand")) {
			msgs = autoRespService.findExpandList(request);
		}
		ResponseUtils.writeJson(msgs, response);
	}

	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		request.setAttribute("msgType", dictMgrService.findDictItem("WEIX.MESSAGE.TYPE"));
		return "html/weixin/autoresp/frame";
	}

	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = autoRespService.findAutoMsgs(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> autoMsg = autoRespService.findAutoMsg(request);
			request.setAttribute("info", autoMsg);
		}
		request.setAttribute("msgType", dictMgrService.findDictItem("WEIX.MESSAGE.TYPE"));
		return "html/weixin/autoresp/edit";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			autoRespService.saveAutoMsg(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			autoRespService.deleteAutoMsg(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}
}
