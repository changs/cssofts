package com.xzcs.controller.weixin;

import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.service.weixin.WxMenuService;
import com.xzcs.util.easyui.EasyuiUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自定义菜单管理
 * @author chang
 * @date 2016-08-14 15:42
 */
@Controller
@RequestMapping(value = "/weixin/menumgr/")
public class WxMenuController extends BaseController {

	@Autowired
	private WxMenuService wxMenuService;
	@Autowired
	private DictMgrService dictMgrService;

	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		return "html/weixin/menu/frame";
	}

	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> menus = wxMenuService.findMenuList(request);
		List<Map<String, Object>> menus2 = EasyuiUtils.getEasyuiTreeGridData(menus);
		ResponseUtils.writeJson(menus2, response);
	}
	
	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		List<Map<String, Object>> parents = wxMenuService.findParentMenu(request);
		request.setAttribute("parents", parents);
		request.setAttribute("msgType", dictMgrService.findDictItem("WEIX.MESSAGE.TYPE"));
		request.setAttribute("menuType", dictMgrService.findDictItem("WEIX.MENU.TYPE"));
		if (method.equals("edit")) {
			Map<String, Object> info = wxMenuService.findMenu(request);
			request.setAttribute("info", info);
		}
		return "html/weixin/menu/edit";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response) {
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> result = new HashMap<String, Object>();
		int chk = 0;
		try {
			if(method.equals("create")){
				chk = wxMenuService.checkMenuCount(request);
			}
			if (chk == 0) {
				wxMenuService.saveMenu(request);
				result.put("result", 1);
				result.put("msg", getMessage("operating.type.success"));
			} else if (chk == 1) {
				result.put("result", 0);
				result.put("msg", "一级菜单最多支持3个");
			} else {
				result.put("result", 0);
				result.put("msg", "二级菜单最多支持5个");
			}
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "checkMenuKey.do")
	public void checkMenuKey(HttpServletRequest request, HttpServletResponse response){
		int result = 0;
		try {
			result = wxMenuService.checkMenuKey(request);
		} catch (Exception e) {
			result = 0;
		}
		if (result > 0) {
			ResponseUtils.writeText("false", response);
		} else {
			ResponseUtils.writeText("true", response);
		}
	}
	
	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			int chk = wxMenuService.checkChildren(request);
			if (chk > 0) {
				result.put("result", 0);
				result.put("msg", "该菜单有子菜单，请先删除子菜单！");
			} else {
				wxMenuService.deleteMenu(request);
				result.put("result", 1);
				result.put("msg", getMessage("operating.type.success"));
			}
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	/**
	 * 推送菜单
	 * @param request
	 * @param response
     */
	@RequestMapping(value = "pushMenu.do")
	public void pushMenu(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxMenuService.pushMenu(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	/**
	 * 拉取菜单
	 * @param request
	 * @param response
     */
	@RequestMapping(value = "pullMenu.do")
	public void pullMenu(HttpServletRequest request, HttpServletResponse response){

	}
}
