package com.xzcs.controller.weixin;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.weixin.WxTextMsgService;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 微信文本消息管理
 * @author chang
 * @date 2016-08-14 15:42
 */
@Controller
@RequestMapping(value = "/weixin/textmgr/")
public class WxTextMsgController extends BaseController {

	@Autowired
	private WxTextMsgService wxTextMsgService;

	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		return "html/weixin/textmsg/frame";
	}

	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = wxTextMsgService.findTextMsgs(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}
	
	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> textMsg = wxTextMsgService.findTextMsg(request);
			request.setAttribute("info", textMsg);
		}
		return "html/weixin/textmsg/edit";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxTextMsgService.saveTextMsg(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}
	
	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxTextMsgService.deleteTextMsg(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}
	
}
