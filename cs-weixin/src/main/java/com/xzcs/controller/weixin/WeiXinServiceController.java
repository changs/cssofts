package com.xzcs.controller.weixin;

import com.xzcs.controller.BaseController;
import com.xzcs.service.weixin.WechatService;
import com.xzcs.service.weixin.WeiXinAccountService;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 微信服务
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "weixin/service/")
public class WeiXinServiceController extends BaseController {
	
	@Autowired
	private WeiXinAccountService weiXinAccountService;
	@Autowired
	private WechatService wechatService;
	
	@RequestMapping(value = "index.do", method = RequestMethod.GET)
	public void wechatGet(HttpServletRequest request, HttpServletResponse response) {
		String echostr = RequestUtils.getValue(request, "echostr");
		boolean checkResult = wechatService.checkSignature(request);
		if (checkResult) {
			ResponseUtils.writeWeixMsg(echostr, "UTF-8", response);
		} else {
			ResponseUtils.writeWeixMsg("非法请求", "UTF-8", response);
		}
	}

	@RequestMapping(value = "index.do", method = RequestMethod.POST)
	public void wechatPost(HttpServletRequest request, HttpServletResponse response) {
		boolean checkResult = wechatService.checkSignature(request);
		if (checkResult) {
			String responseMsg = wechatService.coreService(request);
			ResponseUtils.writeText(responseMsg, response);
		} else {
			ResponseUtils.writeWeixMsg("非法请求", "UTF-8", response);
		}
	}

	@RequestMapping(value = "oauth2.do")
	public String oauth2(HttpServletRequest request){
		String site = propertyUtils.getPropertyValue("xzcs.site.home");
		String app = propertyUtils.getPropertyValue("xzcs.site.app");
		String accountId = RequestUtils.getValue(request, "accountId");
		String reqParams = request.getQueryString();
		String url = site + app + "/weixin/service/oauth2CallBack.do?" + reqParams;
		String result = wechatService.getOauth2buildAuthorizationUrl(accountId, url);
		//logger.debug("oauth2 url:"+result);
		return "redirect:"+result;
	}

	@RequestMapping(value = "oauth2CallBack.do")
	public String oauth2CallBack(HttpServletRequest request) throws WxErrorException {
		String code = RequestUtils.getValue(request, "code");
		String accountId = RequestUtils.getValue(request, "accountId");
		String reqParams = request.getQueryString();
		String purl = RequestUtils.getValue(request, "purl").replaceAll("／", "/");
		if (reqParams != null) {
			reqParams = reqParams.substring(0, reqParams.indexOf("&purl"));
		}
		purl = purl + "?" + reqParams;
		//System.out.println("purl:"+purl);
		WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wechatService.oauth2getAccessToken(accountId, code);
		if (wxMpOAuth2AccessToken == null) {//未获取到OAuth2AccessToken
			return "";
		}
		WxMpUser wxMpUser = wechatService.oauth2getUserInfo(accountId, wxMpOAuth2AccessToken);
		if (wxMpUser == null) {//未获取到微信用户
			return "";
		}
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("openId", wxMpUser.getOpenId());
		httpSession.setAttribute("nickName", wxMpUser.getNickname());
		//System.out.println("wxMpuser:"+wxMpUser.toString());
		return "redirect:"+purl;
	}
}
