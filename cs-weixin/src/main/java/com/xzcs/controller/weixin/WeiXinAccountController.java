package com.xzcs.controller.weixin;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.service.weixin.WeiXinAccountService;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信账号管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/weixin/account/")
public class WeiXinAccountController extends BaseController {
	
	@Autowired
	private WeiXinAccountService weiXinAccountService;
	@Autowired
	private DictMgrService dictMgrService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		return "html/weixin/account/frame";
	}
	
	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = weiXinAccountService.findWeixAccounts(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		List<Map<String, Object>> wxType = dictMgrService.findDictItem("WEIX.ACCOUNT.TYPE");
		request.setAttribute("wxType", wxType);
		if (!method.equals("create")) {
			Map<String, Object> info = weiXinAccountService.findWeixAccount(request);
			request.setAttribute("info", info);
		}
		return "html/weixin/account/edit";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			weiXinAccountService.saveWeixAccount(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			weiXinAccountService.deleteWeixAccount(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "index.do")
	public String index(HttpServletRequest request) {
		request.setAttribute("accountId", RequestUtils.getValue(request, "accountId"));
		return "html/weixin/index";
	}

	@RequestMapping(value = "resetAccessToken.do")
	public void resetAccessToken(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			weiXinAccountService.resetAccessToken(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "help.do")
	public String help(HttpServletRequest request) {
		Map<String, Object> info = weiXinAccountService.findWeixAccount(request);
		request.setAttribute("info", info);
		return "html/weixin/account/help";
	}
}
