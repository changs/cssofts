package com.xzcs.controller.weixin;

import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.service.weixin.WxSubsMsgService;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 关注消息
 * @author chang
 * @date 2016-08-14 15:42
 */
@Controller
@RequestMapping(value = "/weixin/subsmgr/")
public class WxSubsMsgController extends BaseController {

	@Autowired
	private WxSubsMsgService subsMsgService;
	@Autowired
	private DictMgrService dictMgrService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		Map<String, Object> subsMsg = subsMsgService.findSubsMsg(request);
		request.setAttribute("info", subsMsg);
		request.setAttribute("msgType", dictMgrService.findDictItem("WEIX.MESSAGE.TYPE"));
		return "html/weixin/subsmsg/frame";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			subsMsgService.saveSubsMsg(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}
}
