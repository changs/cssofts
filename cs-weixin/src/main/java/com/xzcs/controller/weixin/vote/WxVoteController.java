package com.xzcs.controller.weixin.vote;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.service.weixin.vote.WxVoteService;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.common.GsonUtils;
import com.xzcs.util.common.IpUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信投票
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/weixin/vote/")
public class WxVoteController extends BaseController {

	@Autowired
	private WxVoteService wxVoteService;
	@Autowired
	private DictMgrService dictMgrService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {
		return "html/weixin/vote/frame";
	}

	@RequestMapping(value = "list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = wxVoteService.findVotes(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> info = wxVoteService.findVote(request);
			request.setAttribute("info", info);
		}
		request.setAttribute("moban", dictMgrService.findDictItem("WEIX.VOTE.MOBAN"));
		return "html/weixin/vote/edit";
	}

	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxVoteService.saveVote(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "delete.do")
	public void delete(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wxVoteService.deleteVote(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "index.shtml")
	public String index(HttpServletRequest request) {
		Map<String, Object> info = wxVoteService.findVote(request);
		request.setAttribute("info", info);
		if (null == info) {
			return "html/weixin/vote/error";
		}
		String myzps = StringUtils.get(info, "myzps");
		wxVoteService.updateVoteCheck(request);
		request.setAttribute("nav", "index");
		return "html/weixin/vote/index";
	}

	@RequestMapping(value = "addItem.shtml")
	public String addItem(HttpServletRequest request) {
		Map<String, Object> info = wxVoteService.findVote(request);
		request.setAttribute("info", info);
		request.setAttribute("nav", "bm");
		return "html/weixin/vote/addItem";
	}

	@RequestMapping(value = "saveItem.shtml")
	public void saveItem(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> info = wxVoteService.findVote(request);
			if (null == info) {
				result.put("result", -1);
				result.put("msg", "活动已结束");
			} else {
				String statDate = StringUtils.get(info, "statdate");
				String endDate = StringUtils.get(info, "enddate");
				String today = DateUtils.getToday("yyyy-MM-dd HH:mm:ss");
				if (DateUtils.compareDate(statDate, today, "yyyy-MM-dd HH:mm:ss")==1) {
					result.put("result", -2);
					result.put("msg", "活动尚未开始报名，请等待");
				} else if (DateUtils.compareDate(today, endDate, "yyyy-MM-dd HH:mm:ss")==1){
					result.put("result", -2);
					result.put("msg", "报名时间已结束，感谢您的参与");
				} else {
					String itemId = wxVoteService.saveVoteItem(request);
					result.put("result", 1);
					result.put("itemId", itemId);
					result.put("msg", "报名成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("result", 0);
			result.put("msg", "报名失败，请重试！");
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "itemInfo.shtml")
	public String itemInfo(HttpServletRequest request) {
		Map<String, Object> info = wxVoteService.findVote(request);
		request.setAttribute("info", info);

		Map<String, Object> item = wxVoteService.findVoteItem(request);
		request.setAttribute("item", item);
		return "html/weixin/vote/itemInfo";
	}

	@RequestMapping(value = "itemRank.shtml")
	public String itemRank(HttpServletRequest request) {
		Map<String, Object> info = wxVoteService.findVote(request);
		request.setAttribute("info", info);
		request.setAttribute("nav", "bd");
		return "html/weixin/vote/itemRank";
	}

	@RequestMapping(value = "getItems.shtml")
	public void getItems(HttpServletRequest request, HttpServletResponse response) {
		String myzps = RequestUtils.getValue(request, "myzps");
		PageInfo<Map<String, Object>> items = wxVoteService.findVoteItems(request, myzps);
		ResponseUtils.writeJson(items, response);
	}

	@RequestMapping(value = "voteGift.shtml")
	public String voteGift(HttpServletRequest request) {
		Map<String, Object> info = wxVoteService.findVote(request);
		request.setAttribute("info", info);
		Map<String, Object> item = wxVoteService.findVoteItem(request);
		request.setAttribute("item", item);
		return "html/weixin/vote/voteGift";
	}

	@RequestMapping(value = "itemTicket.shtml")
	public void itemTicket(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> info = wxVoteService.findVote(request);
			if (null == info) {
				result.put("result", -1);
				result.put("msg", "活动已结束");
			} else {
				String startTime = StringUtils.get(info, "start_time");
				String overTime = StringUtils.get(info, "over_time");
				String today = DateUtils.getToday("yyyy-MM-dd HH:mm:ss");
				if (DateUtils.compareDate(startTime, today, "yyyy-MM-dd HH:mm:ss")==1) {
					result.put("result", -2);
					result.put("msg", "活动尚未开始投票，请等待");
				} else if (DateUtils.compareDate(today, overTime, "yyyy-MM-dd HH:mm:ss")==1){
					result.put("result", -2);
					result.put("msg", "投票时间已结束，感谢您的参与");
				} else {
					String ip = IpUtils.getClientIP(request);
					String ipInfo = wxVoteService.getAreaByIp(ip);
					Map<String, Object> ipMap = (Map<String, Object>) GsonUtils.stringToMap(ipInfo);
					Map<String, Object> ipMap2 = (Map<String, Object>) ipMap.get("data");
					String address = "";
					if (null != ipMap2) {
						String country = StringUtils.get(ipMap2, "country");//国家
						String area = StringUtils.get(ipMap2, "area");//华东
						String region = StringUtils.get(ipMap2, "region");//省
						String city = StringUtils.get(ipMap2, "city");//市
						String county = StringUtils.get(ipMap2, "county");//县
						String isp = StringUtils.get(ipMap2, "isp");//运营商
						address = country + "-" + area + "-" + region + "-" + city + "-" + county + "-" + isp;
					}
					wxVoteService.saveVoteRecord(request, ip, address);
					result.put("result", 1);
					result.put("msg", "投票成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("result", 0);
			result.put("msg", "投票失败，请重试！");
		}
		ResponseUtils.writeJson(result, response);
	}
}
