package com.xzcs.controller;

import com.xzcs.util.spring.PropertyUtils;
import com.xzcs.util.web.SessionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 控制组件基类
 * @author chang
 * @createDate Mar 1, 2013
 * @description
 *  @Controller 声明Action组件
    @Service    声明Service组件    @Service("myMovieLister") 
    @Repository 声明Dao组件
    @Component   泛指组件, 当不好归类时. 
    @RequestMapping("/menu")  请求映射
    @Resource  用于注入，( j2ee提供的 ) 默认按名称装配，@Resource(name="beanName") 
    @Autowired 用于注入，(spring提供的) 默认按类型装配 
    @Transactional( rollbackFor={Exception.class}) 事务管理
    @ResponseBody
    @Scope("prototype") 设定bean的作用域
 */
public class BaseController {
	public Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	public PropertyUtils propertyUtils;
	@Autowired
	private MessageSource messageSource;

	public String getMessage(String key) {
		return this.messageSource.getMessage(key, null, null);
	}

	public Map<String, Object> getUser(HttpServletRequest request){
		Subject subject = SecurityUtils.getSubject();
		Session session	= subject.getSession();
		return SessionUtils.getAttribute(session, "user");
	}
}
