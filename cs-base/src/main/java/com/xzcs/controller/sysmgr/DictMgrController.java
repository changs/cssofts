package com.xzcs.controller.sysmgr;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.DictMgrService;
import com.xzcs.util.common.GsonUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字典管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/sysmgr/")
public class DictMgrController extends BaseController {
	
	@Autowired
	private DictMgrService dictMgrService;
	
	@RequestMapping(value = "dict/frame.do")
	public String frame(HttpServletRequest request) {
		return "html/sysmgr/dict/frame";
	}
	
	@RequestMapping(value = "dict/list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = dictMgrService.findDictGroups(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "dict/edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (!method.equals("create")) {
			Map<String, Object> group = dictMgrService.findDictGroup(request);
			request.setAttribute("group", group);
			List<Map<String, Object>> items = dictMgrService.findDictItem(request);
			request.setAttribute("items", GsonUtils.list2Json(items));
		}
		return "html/sysmgr/dict/edit";
	}

	@RequestMapping(value = "dict/save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			dictMgrService.saveDict(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "dict/delete.do")
	public void deleteUser(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			dictMgrService.deleteDict(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "dict/checkGroupId.do")
	public void checkGroupId(HttpServletRequest request, HttpServletResponse response){
		int result = 0;
		try {
			result = dictMgrService.checkGroupId(request);
		} catch (Exception e) {
			result = 0;
		}
		if (result > 0) {
			ResponseUtils.writeText("false", response);
		} else {
			ResponseUtils.writeText("true", response);
		}
	}
}
