package com.xzcs.service.sysmgr;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.dictionary.DictionaryMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.GsonUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DictMgrService extends BaseService {

	@Autowired
	private DictionaryMapper dictionaryMapper;

	public PageInfo<Map<String, Object>> findDictGroups(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = dictionaryMapper.findDictGroups(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findDictGroup(HttpServletRequest request){
		String groupId = RequestUtils.getValue(request, "groupId");
		return dictionaryMapper.findDictGroup(groupId);
	}

	public List<Map<String, Object>> findDictItem(HttpServletRequest request){
		String groupId = RequestUtils.getValue(request, "groupId");
		return dictionaryMapper.findDictItems(groupId);
	}

	public List<Map<String, Object>> findDictItem(String groupId){
		return dictionaryMapper.findDictItems(groupId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveDict(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		String groupId = RequestUtils.getValue(request, "groupId");
		String items = RequestUtils.getValue(request, "items").replaceAll("“", "\"");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("edit")) {
			dictionaryMapper.updateDictGroup(params);
		} else if(method.equals("create")){//新增
			dictionaryMapper.insertDictGroup(params);
		}
		List<Map<String, Object>> itemList = GsonUtils.parseMapList(items);
		dictionaryMapper.deleteDictItem(groupId);
		for (int i = 0; i < itemList.size(); i++) {
			Map<String, Object> params1 = new HashMap<String, Object>();
			params1.put("groupId", groupId);
			params1.put("itemId", StringUtils.get(itemList.get(i), "item_id"));
			params1.put("itemName", StringUtils.get(itemList.get(i), "item_name"));
			params1.put("showOrder", i);
			dictionaryMapper.insertDictItem(params1);
		}
	}

	public int checkGroupId(HttpServletRequest request){
		String groupId = RequestUtils.getValue(request, "groupId");
		Map<String, Object> dict = dictionaryMapper.findDictGroup(groupId);
		return dict.isEmpty() ? 0 : 1;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteDict(HttpServletRequest request){
		String groupId = RequestUtils.getValue(request, "groupId");
		dictionaryMapper.deleteDictGroup(groupId);
		dictionaryMapper.deleteDictItem(groupId);
	}
}
