package com.xzcs.service;

import com.xzcs.util.Constants;
import com.xzcs.util.db.DBUtils;
import com.xzcs.util.spring.PropertyUtils;
import com.xzcs.util.web.SessionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 服务组件基类
 * @author chang
 * @createDate Mar 8, 2013
 * @description
 */
public class BaseService {
	public Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	public PropertyUtils propertyUtils;
	@Autowired
	public DBUtils dbUtils;
	@Autowired
	private MessageSource messageSource;

	public String getMessage(String key) {
		return this.messageSource.getMessage(key, null, null);
	}
	
	public String getDefaultPageSize(){
		return propertyUtils.getPropertyValue(Constants.DEFAULT_PAGESIZE);
	}

	public Map<String, Object> getUser(HttpServletRequest request){
		Subject subject = SecurityUtils.getSubject();
		Session session	= subject.getSession();
		return SessionUtils.getAttribute(session, "user");
	}
}