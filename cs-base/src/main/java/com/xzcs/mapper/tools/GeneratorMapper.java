package com.xzcs.mapper.tools;

import java.util.List;
import java.util.Map;

public interface GeneratorMapper {
	public List<Map<String, Object>> findTables(Map<String, Object> params);
	public Map<String, Object> findTable(String tableName);
	public List<Map<String, Object>> findTableColumns(String tableName);
}
