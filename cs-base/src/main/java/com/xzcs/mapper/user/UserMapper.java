package com.xzcs.mapper.user;

import java.util.List;
import java.util.Map;

public interface UserMapper {

	public boolean updateUser(Map<String, Object> params);
	public boolean deleteUser(String userId);
	public void insertUser(Map<String, Object> params);
	public Map<String, Object> findByUserId(String userId);
	public Map<String, Object> findByUserName(String userName);
	public int checkUserName(String userName, String userId);
	public List<Map<String, Object>> findUsers(Map<String, Object> params);
	public List<Map<String, Object>> findUserRole(String userId);
	public boolean deleteUserRole(String userId);
	public void saveUserRole(Map<String, Object> params);
	public List<Map<String, Object>> findUserModule(Map<String, Object> params);
	public List<Map<String, Object>> findUserPermit(String userId);
	public boolean updateUserPwd(Map<String, Object> params);
}
