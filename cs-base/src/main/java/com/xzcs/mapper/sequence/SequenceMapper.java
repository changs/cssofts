package com.xzcs.mapper.sequence;


public interface SequenceMapper {
	public long getSequenceValue(String sequenceName);
}
