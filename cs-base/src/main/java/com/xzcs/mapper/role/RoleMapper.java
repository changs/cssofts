package com.xzcs.mapper.role;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleMapper {

	public List<Map<String, Object>> findRoles(Map<String, Object> params);
	public Map<String, Object> findRole(String roleId);
	public void insertRole(Map<String, Object> params);
	public boolean updateRole(Map<String, Object> params);
	public boolean deleteRole(String roleId);
	public boolean deleteRoleModule(String roleId);
	public boolean deleteRolePermit(String roleId);
	public boolean deleteRoleModuleWithModule(String moduleId);
	public boolean deleteRolePermitWithPermit(String permitId);
	public int checkRoleName(String roleName, String roleId);
	public List<Map<String, Object>> findPermitTree(String roleId);
	public void insertRoleModule(@Param("roleId") String roleId, @Param("moduleId") String moduleId);
	public void insertRolePermit(String roleId, String permitId);
}
