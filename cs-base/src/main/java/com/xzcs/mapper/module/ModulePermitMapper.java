package com.xzcs.mapper.module;

import java.util.List;
import java.util.Map;

public interface ModulePermitMapper {

	public List<Map<String, Object>> findAllPermit();
	public String findModulePath(String moduleId);
	public List<Map<String, Object>> findModules(Map<String, Object> params);
	public Map<String, Object> findModule(String moduleId);
	public void insertModule(Map<String, Object> params);
	public boolean updateModule(Map<String, Object> params);
	public boolean deleteModule(String moduleId);
	public List<Map<String, Object>> findPerimitWithModule(String moduleId);
	public Map<String, Object> findPermit(String permitId);
	public void insertPermit(Map<String, Object> params);
	public boolean updatePermit(Map<String, Object> params);
	public boolean deletePermit(String permitId);
	public boolean deletePermitWithModule(String moduleId);
	public int checkCode(String code, String id);
	public List<Map<String, Object>> findModuleRoles(String moduleId);
	public boolean deleteModuleRoleWithModule(String moduleId);
	public void insertModuleRole(Map<String, Object> params);
	public List<Map<String, Object>> findPermitRoles(String permitId);
	public boolean deletePermitRoleWithPermit(String permitId);
	public void insertPermitRole(Map<String, Object> params);
}
