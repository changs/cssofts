package com.xzcs.mapper.task;

import java.util.List;
import java.util.Map;

public interface QuartzTaskMapper {
	public Map<String, Object> findQuartzTask(String taskId);
	public List<Map<String, Object>> findQuartzTasks(Map<String, Object> params);
	public boolean deleteQuartzTask(String taskId);
	public void insertQuartzTask(Map<String, Object> params);
	public boolean updateQuartzTask(Map<String, Object> params);
	public boolean updateQuartzTaskStatus(String taskId, String status);
	public void insertQuartzTaskLog(Map<String, Object> params);
	public boolean updateQuartzTaskLog(Map<String, Object> params);
}
