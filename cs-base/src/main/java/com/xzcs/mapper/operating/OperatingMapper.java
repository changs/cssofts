package com.xzcs.mapper.operating;

import java.util.List;
import java.util.Map;

public interface OperatingMapper {
	public boolean deleteOperating(int seqId);
	public void insertOperating(Map<String, Object> params);
	public List<Map<String, Object>> findOperatings(Map<String, Object> params);
}
