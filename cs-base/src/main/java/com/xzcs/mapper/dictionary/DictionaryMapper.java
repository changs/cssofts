package com.xzcs.mapper.dictionary;

import java.util.List;
import java.util.Map;

public interface DictionaryMapper {
	public List<Map<String, Object>> findDictGroups(Map<String, Object> params);
	public Map<String, Object> findDictGroup(String groupId);
	public List<Map<String, Object>> findDictItems(String groupId);
	public void insertDictGroup(Map<String, Object> params);
	public boolean updateDictGroup(Map<String, Object> params);
	public boolean deleteDictGroup(String groupId);
	public boolean deleteDictItem(String groupId);
	public void insertDictItem(Map<String, Object> params);
}
