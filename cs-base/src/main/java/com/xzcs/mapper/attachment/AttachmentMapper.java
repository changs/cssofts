package com.xzcs.mapper.attachment;

import java.util.List;
import java.util.Map;

public interface AttachmentMapper {
	public List<Map<String, Object>> findAttachments(Map<String, Object> params);
	public Map<String, Object> findAttachment(String attachmentId);
	public boolean deleteAttachment(String attachmentId);
	public void insertAttachment(Map<String, Object> params);
}
