package com.xzcs.cache;

import com.xzcs.util.common.SerializeUtils;
import com.xzcs.util.redis.JedisUtils;
import org.apache.ibatis.cache.Cache;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @desc: 
 * @author: chang
 * @date: 2017/6/13
 */
public class RedisMybatisCache implements Cache {

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
    private final String id;

    private static JedisUtils jedisUtils;

    public static void setJedisUtils(JedisUtils jedisUtils) {
        RedisMybatisCache.jedisUtils = jedisUtils;
    }

    public RedisMybatisCache(String id){
        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void putObject(Object o, Object o1) {
        jedisUtils.set(SerializeUtils.serialize(o), SerializeUtils.serialize(o1));
    }

    @Override
    public Object getObject(Object o) {
        byte[] value = jedisUtils.get(SerializeUtils.serialize(o));
        Object obj = SerializeUtils.deserialize(value);
        return obj;
    }

    @Override
    public Object removeObject(Object o) {
        Object obj = getObject(o);
        jedisUtils.expire(SerializeUtils.serialize(o), 0);
        return obj;
    }

    @Override
    public void clear() {
        jedisUtils.flushDB();
    }

    @Override
    public int getSize() {
        return Integer.valueOf(jedisUtils.dbSize().toString());
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }
}
