package com.xzcs.util.common;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Json工具类
 */
public class JsonUtils {
	public static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);
	private ObjectMapper mapper;

	public JsonUtils() {

	}

	public JsonUtils(Include include) {
		mapper = new ObjectMapper();
		// 设置输出包含的属性
		mapper.getSerializationConfig().withSerializationInclusion(include);
	}

	/**
	 * 创建输出全部属性到Json字符串的Binder.
	 */
	public static JsonUtils buildNormalBinder() {
		return new JsonUtils(Include.ALWAYS);
	}

	/**
	 * 创建只输出非空属性到Json字符串的Binder.
	 */
	public static JsonUtils buildNonNullBinder() {
		return new JsonUtils(Include.NON_NULL);
	}

	/**
	 * 创建只输出初始值被改变的属性到Json字符串的Binder.
	 */
	public static JsonUtils buildNonDefaultBinder() {
		return new JsonUtils(Include.NON_DEFAULT);
	}

	/**
	 * 如果JSON字符串为Null或"null"字符串,返回Null. 如果JSON字符串为"[]",返回空集合.
	 * 如需读取集合如List/Map,且不是List<String>这种简单类型时使用如下语句: List<MyBean> beanList =
	 * binder.getMapper().readValue(listString, new
	 * TypeReference<List<MyBean>>() {});
	 * @param jsonString {@link String} JSON字符串
	 * @param clazz {@link Class} 待转换目标对象
	 * @return 转换后的对象
	 */
	public <T> T parse(String jsonString, Class<T> clazz) {
		if (null == jsonString || jsonString.equals("")) {
			return null;
		}
		try {
			return mapper.readValue(jsonString, clazz);
		} catch (IOException e) {
			logger.warn("parse json string error:" + jsonString, e);
			return null;
		}
	}

	/**
	 * 将对象格式化成JSON格式字符串
	 * @param arg {@link Object} 待格式化的参数
	 * @return {@link String} 格式后的JSON字符串
	 */
	public String format(Object arg) {
		try {
			return mapper.writeValueAsString(arg);
		} catch (IOException e) {
			logger.warn("write to json string error:" + arg, e);
			return null;
		}
	}

	/**
	 * 设置转换日期类型的format pattern,如果不设置默认打印Timestamp毫秒数.
	 * @param pattern {@link String} 日期格式
	 */
	public void setDateFormat(String pattern) {
		DateFormat df = new SimpleDateFormat(pattern);
		mapper.getSerializationConfig().with(df);
		mapper.getDeserializationConfig().with(df);
	}

	/**
	 * 取出Mapper做进一步的设置或使用其他序列化API.
	 */
	public ObjectMapper getMapper() {
		return mapper;
	}
}
