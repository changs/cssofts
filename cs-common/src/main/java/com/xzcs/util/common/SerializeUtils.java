package com.xzcs.util.common;

import java.io.*;

/**
 * @desc: 
 * @author: chang
 * @date: 2017/5/24
 */
public class SerializeUtils {
    public static byte[] serialize(Object o) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ObjectOutputStream outo = new ObjectOutputStream(out);
            outo.writeObject(o);
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
        return out.toByteArray();
    }

    public static Object deserialize(byte[] b) {
        ObjectInputStream oin;
        try {
            oin = new ObjectInputStream(new ByteArrayInputStream(b));
            return oin.readObject();
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }
}
