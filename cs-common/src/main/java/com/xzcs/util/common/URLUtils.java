package com.xzcs.util.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * URI帮助类
 */
public class URLUtils {

	/**
	 * 获取请求URL
	 * @param request
	 * @return
	 */
	public static String getURI(HttpServletRequest request) {
		UrlPathHelper helper = new UrlPathHelper();
		String uri = helper.getOriginatingRequestUri(request);
		String ctx = helper.getOriginatingContextPath(request);
		if (!StringUtils.isBlank(ctx)) {
			return uri.substring(ctx.length());
		} else {
			return uri;
		}
	}
	
	public static int getPageNo(String uri) {
		if (uri == null) {
			throw new IllegalArgumentException("URI can not be null");
		}
		if (!uri.startsWith("/")) {
			throw new IllegalArgumentException("URI must start width '/'");
		}
		int pageNo = 1;
		int bi = uri.indexOf("_");
		int mi = uri.indexOf("-");
		int pi = uri.indexOf(".");
		if (bi != -1) {
			String pageNoStr;
			if (mi != -1) {
				pageNoStr = uri.substring(bi + 1, mi);
			} else {
				if (pi != -1) {
					pageNoStr = uri.substring(bi + 1, pi);
				} else {
					pageNoStr = uri.substring(bi + 1);
				}
			}
			try {
				pageNo = Integer.valueOf(pageNoStr);
			} catch (Exception e) {
			}
		}
		return pageNo;
	}
	
	public static String[] getParams(String uri) {
		if (uri == null) {
			throw new IllegalArgumentException("URI can not be null");
		}
		if (!uri.startsWith("/")) {
			throw new IllegalArgumentException("URI must start width '/'");
		}
		int pi = uri.indexOf(".");
		int bi = uri.indexOf("_");
		String[] params;
		if (bi != -1) {
			String paramStr = uri.substring(1, bi);
			params = paramStr.split("/");
		} else {
			if (pi != -1) {
				String paramStr = uri.substring(1, pi);
				params = paramStr.split("/");
			} else {
				params = new String[0];
			}
		}
		return params;
	}
	
	public static String getReqInfo(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();  
        String queryString = "";  
        for (String key : params.keySet()) {  
            String[] values = params.get(key);  
            for (int i = 0; i < values.length; i++) {  
                queryString += key + "=" + values[i] + "&";  
            }  
        }
        if (!queryString.equals("")) {
        	queryString = queryString.substring(0, queryString.length() - 1);
		}
        if(queryString.length() > 4000) {
        	queryString = queryString.substring(0, 3996) + "...";
        }
        return queryString;
    }
	
	public static PageInfo getPageInfo(String uri, String queryString) {
		if (uri == null) {
			return null;
		}
		if (!uri.startsWith("/")) {
			throw new IllegalArgumentException("URI must start width '/'");
		}
		int bi = uri.indexOf("_");
		int mi = uri.indexOf("-");
		int pi = uri.indexOf(".");
		int lastSpt = uri.lastIndexOf("/") + 1;
		String url;
		if (!StringUtils.isBlank(queryString)) {
			url = uri + "?" + queryString;
		} else {
			url = uri;
		}
		// 翻页前半部
		String urlFormer;
		if (bi != -1) {
			urlFormer = uri.substring(lastSpt, bi);
		} else if (mi != -1) {
			urlFormer = uri.substring(lastSpt, mi);
		} else if (pi != -1) {
			urlFormer = uri.substring(lastSpt, pi);
		} else {
			urlFormer = uri.substring(lastSpt);
		}
		// 翻页后半部
		String urlLater;
		if (mi != -1) {
			urlLater = url.substring(mi);
		} else if (pi != -1) {
			urlLater = url.substring(pi);
		} else {
			urlLater = url.substring(uri.length());
		}
		String href = url.substring(lastSpt);
		String hrefPath = url.substring(0, url.lastIndexOf("/"));
		return new PageInfo(hrefPath, href, urlFormer, urlLater);
	}

	/**
	 * URI信息
	 */
	public static class PageInfo {
		private String hrefPath;
		/**
		 * 页面地址
		 */
		private String href;
		/**
		 * href前半部（相对于分页）
		 */
		private String hrefFormer;
		/**
		 * href后半部（相对于分页）
		 */
		private String hrefLatter;

		public PageInfo(String hrefPath,String href, String hrefFormer, String hrefLatter) {
			this.hrefPath = hrefPath;
			this.href = href;
			this.hrefFormer = hrefFormer;
			this.hrefLatter = hrefLatter;
		}

		public String getHref() {
			return href;
		}

		public void setHref(String href) {
			this.href = href;
		}

		public String getHrefFormer() {
			return hrefFormer;
		}

		public void setHrefFormer(String hrefFormer) {
			this.hrefFormer = hrefFormer;
		}

		public String getHrefLatter() {
			return hrefLatter;
		}

		public void setHrefLatter(String hrefLatter) {
			this.hrefLatter = hrefLatter;
		}

		public String getHrefPath() {
			return hrefPath;
		}

		public void setHrefPath(String hrefPath) {
			this.hrefPath = hrefPath;
		}
		
		public String getUrl(int pageNo){
			String url = "";
			if (pageNo < 2) {
				url = this.hrefPath + "/" + this.hrefFormer + this.hrefLatter;
			} else {
				url = this.hrefPath + "/" + this.hrefFormer + "_" + pageNo + this.hrefLatter;
			}
			return url; 
		}
	}
}
