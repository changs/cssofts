package com.xzcs.util.common;

import java.security.MessageDigest;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

public class StringUtils {

	/**
	 * 字符数组转字符串
	 * @param strArray
	 * @param splitFlag
     * @return
     */
	public static String arrayToString(String[] strArray, String splitFlag) {
		String tmpString = "";
		if (strArray.length == 0) {
			tmpString = "";
		} else {
			for (int i = 0; i < strArray.length; i++) {
				tmpString = tmpString + strArray[i];
				if (i < strArray.length - 1) {
					tmpString = tmpString + splitFlag;
				}
			}
		}
		tmpString = tmpString.trim();
		return tmpString;
	}

	/**
	 * 字符list转字符
	 * @param list
	 * @param splitFlag
     * @return
     */
	public static String listToString(List<String> list, String splitFlag) {
		String tmpString = "";
		if (list.size() == 0) {
			tmpString = "";
		} else {
			for (String str : list) {
				tmpString = tmpString + splitFlag + str;
			}
			tmpString = tmpString.substring(1);
		}
		tmpString = tmpString.trim();
		return tmpString;
	}

	public static String listToString(List<Map<String, Object>> list, String key, String split) {
		String str = "";
		for (Map<String, Object> map : list) {
			str += get(map, key) + split;
		}
		if (!"".equals(str))
			str = str.substring(0, str.lastIndexOf(split));
		return str;
	}

	/**
	 * 字符串反转
	 * @param strReverse
	 * @return
     */
	public static String reverse(String strReverse) {
		if (strReverse == null) {
			return strReverse;
		} else {
			StringBuffer tmpString = new StringBuffer(strReverse);

			tmpString = tmpString.reverse();

			return tmpString.toString();
		}
	}

	/**
	 * 对象转字符
	 * @param value
	 * @return
     */
	public static String notEmpty(Object value) {
		if (value == null) {
			value = "";
		}
		return String.valueOf(value);
	}

	/**
	 * 获取map的键值
	 * @param map
	 * @param keyName
     * @return
     */
	public static String get(Map map, String keyName) {
		return notEmpty(map.get(keyName));
	}

	/**
	 * 替换参数字符串
	 * @param sql
	 * @param params
	 */
	public static String getSql(String sql, Object[] params) {
		int i = 0;
		StringTokenizer st = new StringTokenizer(sql, "?", true);
		StringBuffer bf= new StringBuffer("");
		while (st.hasMoreTokens()) {
			String temp = st.nextToken();
			if(temp.equals("?")) {
				bf.append("'"+String.valueOf(params[i])+"'");
				i++;
			} else {
				bf.append(temp);
			}
		}
		
		return bf.toString();
	}
	
	/**
	 * 获取不定参数的sql语句
	 * @param sql
	 * @param list
	 * @return
	 */
	public static String getSql(String sql, List<String> list) {
		return getSql(sql, list.toArray());
	}

	/**
	 * 字符转整型
	 * @param s
	 * @return
     */
	public static int toInt(String s){
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 字符转long
	 * @param s
	 * @return
	 */
	public static long toLong(String s){
		try {
			return Long.parseLong(s);
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 字符转浮点
	 * @param s
	 * @return
	 */
	public static float toFloat(String s){
		try {
			return Float.parseFloat(s);
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 字符转double
	 * @param s
	 * @return
	 */
	public static double toDouble(String s){
		try {
			return Double.parseDouble(s);
		} catch (Exception e) {
			return 0;
		}
	}
	
	public String[] split(String strSplit, String splitFlag) {
		if (strSplit == null || splitFlag == null) {
			String[] tmpSplit = new String[1];
			tmpSplit[0] = strSplit;
			return tmpSplit;
		}

		StringTokenizer st = new StringTokenizer(strSplit,
				splitFlag);

		int size = st.countTokens();
		String[] tmpSplit = new String[size];

		for (int i = 0; i < size; i++) {
			tmpSplit[i] = st.nextToken();
		}
		return tmpSplit;
	}
	
	/**
	 * md5加密
	 * @param source
	 * @return
	 */
	public static String md5(String source) {  

		StringBuffer sb = new StringBuffer(32);  

		try {  
			MessageDigest md = MessageDigest.getInstance("MD5");  
			byte[] array = md.digest(source.getBytes("UTF-8"));  

			for (int i = 0; i < array.length; i++) {  
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));  
			}  
		} catch (Exception e) {  
			return "";  
		}  
		return sb.toString();  
	}  
	
	/**
	 * md5加密
	 * @param source
	 * @return
	 */
	public static String md5(byte[] source) {  

		StringBuffer sb = new StringBuffer(32);  

		try {  
			MessageDigest md = MessageDigest.getInstance("MD5");  
			byte[] array = md.digest(source);  

			for (int i = 0; i < array.length; i++) {  
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));  
			}  
		} catch (Exception e) {  
			return "";  
		}  
		return sb.toString();  
	}

	/**
	 * 获取UUID
	 * @return
     */
	public static String getUUID(){
		UUID uuid = UUID.randomUUID();
		String str = uuid.toString().replaceAll("-", "");
		return str;
	}

	/**
	 * 断目标字符串是否包含特定字符串
	 * @param container
	 * @param regx
	 * @return
	 */
	public static boolean isContains(String container, String regx[]) {
		boolean result = false;
		for (int i = 0; i < regx.length; i++) {
			if (container.indexOf(regx[i]) != -1) {
				return true;
			}
		}
		return result;
	}

	/**
	 * 替换字符串中的特殊字符
	 * @param value
	 * @return
	 */
	public static String cleanAllXSS(String value) {
		String[] filterChars = {"'", "\"", "%", "(", "<", ">", "/", ")", ";"};
		String[] replaceChars = {"‘", "“", "％", "（", "＜", "＞", "／", "）", "；"};

		value = value.replaceAll("(?i)<\\s*script.*<\\s*/\\s*script\\s*>", "");
		value = value.replaceAll("(?i)onclick", "");
		value = value.replaceAll("(?i)ondblclick", "");
		value = value.replaceAll("(?i)onerror", "");
		value = value.replaceAll("(?i)onblur", "");
		value = value.replaceAll("(?i)onfocus", "");
		value = value.replaceAll("(?i)onkeydown", "");
		value = value.replaceAll("(?i)onkeypress", "");
		value = value.replaceAll("(?i)onkeyup", "");
		value = value.replaceAll("(?i)onmousedown", "");
		value = value.replaceAll("(?i)onmousemove", "");
		value = value.replaceAll("(?i)onmouseout", "");
		value = value.replaceAll("(?i)onmouseover", "");
		value = value.replaceAll("(?i)onmouseup", "");
		value = value.replaceAll("(?i)alert", "");
		value = value.replaceAll("(?i)prompt", "");
		value = value.replaceAll("(?i)confirm", "");
		value = value.replaceAll("(?i)select", "");
		value = value.replaceAll("(?i)input", "");
		value = value.replaceAll("(?i)button", "");
		value = value.replaceAll("(?i)textarea", "");
		value = value.replaceAll("(?i)iframe", "");
		value = value.replaceAll("(?i)marquee", "");
		value = value.replaceAll("(?i)eval\\((.*)\\)", "");
		value = value.replaceAll("(?i)<\\s*iframe\\s+.*>", "");
		value = value.replaceAll("(?i)<\\s*frame\\s+.*>", "");
		value = value.replaceAll("(?i)<\\s*input.*>", "");
		value = value.replaceAll("(?i)<\\s*select.*<\\s*/\\s*select\\s*>", "");
		value = value.replaceAll("(?i)<\\s*img\\s+.*>", "");

		for (int i = 0; i < filterChars.length; i++) {
			value = value.replace(filterChars[i], replaceChars[i]);
		}

		return value;
	}

	public static String htmlEncode(String source) {
		if (source == null) {
			return "";
		}
		String html = "";
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < source.length(); i++) {
			char c = source.charAt(i);
			switch (c) {
				case '<':
					buffer.append("&lt;");
					break;
				case '>':
					buffer.append("&gt;");
					break;
				case '&':
					buffer.append("&amp;");
					break;
				case '"':
					buffer.append("&quot;");
					break;
				case '\'':
					buffer.append("&#x27;");
					break;
				case 10:
				case 13:
					break;
				default:
					buffer.append(c);
			}
		}
		html = buffer.toString();
		return html;
	}

	public static void main(String[] args) {
		String s = StringUtils.reverse("hello world");
		System.out.println("s:"+s);
	}
}