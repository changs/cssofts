package com.xzcs.util.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @desc: 
 * @author: chang
 * @date: 2017/5/23
 */
public class DateUtils {
	private static String defaultPattern = "yyyy-MM-dd";

	/**
	 * 得到今天的日期 默认格式：yyyy-MM-dd
	 * @return
	 */
	public static String getToday() {
		return getToday(defaultPattern);
	}

	/**
	 * 得到今天的日期
	 * @param pattern 格式：如yy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getToday(String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(new Date());
	}

	/**
	 * 获取日期
	 * @param days 偏移
	 * @return
	 */
	public static String getDay(int days) {
		return getDay(days, defaultPattern);
	}

	/**
	 * 获取日期
	 * @param days 偏移
	 * @param pattern yy-MM-dd HH:mm:ss
     * @return
     */
	public static String getDay(int days, String pattern) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, days);
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(cal.getTime());
	}

	/**
	 * 得到昨天的日期 默认格式：yyyy-MM-dd
	 * @return
	 */
	public static String getYesterday() {
		return getYesterday(defaultPattern);
	}

	/**
	 * 得到昨天的日期
	 * @param pattern 格式：如yy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getYesterday(String pattern) {
		return getDay(-1, pattern);
	}

	/**
	 * 得到指定月份
	 * @param offset 偏移量 offset = -1 取得上一个月 offset = 0 取得本月 offset = 1 取得下一个月
	 * @param pattern yy-MM
	 * @return
	 */
	public static String getMonth(int offset, String pattern) {
		String yearMonth = "197101";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Calendar nowCal = Calendar.getInstance();
		nowCal.add(Calendar.MONTH, offset);
		yearMonth = format.format(nowCal.getTime());
		return yearMonth;
	}

	/**
	 * 日期转字符
	 * @param date
	 * @param pattern
     * @return
     */
	public static String date2String(Date date, String pattern) {
		DateFormat format = new SimpleDateFormat(pattern);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return format.format(c.getTime());
	}

	/**
	 * 字符转日期
	 * @param str
	 * @param pattern
     * @return
     */
	public static Date string2Date(String str, String pattern) {
		DateFormat format = new SimpleDateFormat(pattern);
		try {
			return format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 时间比较
	 * @param date1
	 * @param date2
	 * @param pattern
     * @return 1:date1大于date2  2:date1小于date2
     */
	public static int compareDate(String date1, String date2, String pattern) {
		try {
			Date dt1 = string2Date(date1, pattern);
			Date dt2 = string2Date(date2, pattern);
			if (dt1.getTime() > dt2.getTime()) {
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				return 2;
			} else {
				return 0;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	}

	/**
	 * 时间戳转时间
	 * @param stamp
	 * @param pattern
	 * @return
	 */
	public static String stampToDate(String stamp, String pattern){
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		long lt = new Long(stamp);
		Date date = new Date(lt);
		res = simpleDateFormat.format(date);
		return res;
	}
}
