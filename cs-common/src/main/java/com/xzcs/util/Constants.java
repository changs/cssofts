package com.xzcs.util;


public class Constants {
    /**
	 * UTF-8编码
	 */
	public static String UTF8 = "UTF-8";
	
	public static String SYSTEM_PROPERTY_PATH = "/WEB-INF/config/system.properties";
	
	public static String DEFAULT_PAGESIZE = "xzcs.default.pagesize";
	
	public static String DEFAULT_ROLEID = "xzcs.default.user.roleid";
	
	public static String DEFAULT_FILE_UPLOAD_PATH = "xzcs.default.file.upload.path";
	
	public static String DEFAULT_PASSWORD = "xzcs.default.password";
	
	public static String DEFAULT_COMPANY_NAME = "xzcs.company.name";
}