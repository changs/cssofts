package com.xzcs.util.web;

import com.xzcs.util.common.StringUtils;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class RequestUtils {

	/**
	 * 判断是否ajax请求
	 * @return true是
	 */
	public static boolean isAjaxRequest(HttpServletRequest request){
		String xRequestedWith = request.getHeader("X-Requested-With");
		return (xRequestedWith != null &&
				"XMLHttpRequest".equals(xRequestedWith)) ;
	}

	/**
	 * 判断是否微信浏览器请求
	 * @param request
	 * @return
     */
	public static boolean isWeixRequest(HttpServletRequest request){
		String userAgent = request.getHeader("User-Agent");
		userAgent = (null != userAgent) ? userAgent.toLowerCase() : "";
		return userAgent.indexOf("micromessenger") != -1 ;
	}

	public static boolean isMultipartRequest(HttpServletRequest request) {
		String contentType = request.getContentType();
		return contentType != null &&
				contentType.toLowerCase().indexOf("multipart") != -1;
	}

	/**
	 * 获取应用路径
	 * @param request
	 * @return
     */
	public static String getAppContextPath(HttpServletRequest request){
		ServletContext servletContext = request.getServletContext();
		String path = servletContext.getContextPath();
		return path;
	}

	public static Map<String, Object> getRequestMap(HttpServletRequest request){
		Map<String, String[]> map = request.getParameterMap();
		Map<String, Object> newMap = new HashMap<String, Object>();
		for (Map.Entry<String, String[]> entry : map.entrySet()) {
			String key = entry.getKey();
			String[] value = entry.getValue();
			for (int i = 0; i < value.length; i++) {
				value[i] = StringUtils.htmlEncode(value[i]);
			}
			if (null != value && value.length == 1) {
				newMap.put(key, value[0]);
			} else if (null != value && value.length > 1) {
				newMap.put(key, value);
			} else {
				newMap.put(key, value);
			}
		}
		return newMap;
	}

	public static String getValue(HttpServletRequest request, String paramName) {
		String obj = request.getParameter(paramName);
		if (obj == null) {
			obj = "";
		}
		obj = StringUtils.htmlEncode(obj);
		return obj;
	}

	public static String[] getValues(HttpServletRequest request, String paramName) {
		String[] value = request.getParameterValues(paramName);
		if (value == null) {
			value = new String[]{};
		}
		for (int i = 0; i < value.length; i++) {
			value[i] = StringUtils.htmlEncode(value[i]);
		}
		return value;
	}

	public static String getMpValue(MultipartHttpServletRequest multipartRequest, String paramName) {
		String obj = multipartRequest.getParameter(paramName);
		if (obj == null) {
			obj = "";
		}
		obj = StringUtils.htmlEncode(obj);
		return obj;
	}
	
	public static String[] getMpValues(MultipartHttpServletRequest multipartRequest, String paramName) {
		String[] value = multipartRequest.getParameterValues(paramName);
		if (value == null) {
			value = new String[]{};
		}
		for (int i = 0; i < value.length; i++) {
			value[i] = StringUtils.htmlEncode(value[i]);
		}
		return value;
	}
	
	public static String getFilePath(HttpServletRequest request, String path){
		ServletContext context = request.getServletContext();
		return getFilePath(context, path);
	}
	
	public static String getFilePath(ServletContext context, String path){
		String realpath = context.getRealPath(path);
		//tomcat8.0获取不到真实路径，通过/获取路径
		if(null == realpath){
			realpath=context.getRealPath("/")+path;
		}
		return realpath;
	}
}
