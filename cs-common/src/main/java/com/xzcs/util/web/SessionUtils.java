package com.xzcs.util.web;

import org.apache.shiro.session.Session;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @desc:
 * @author: chang
 * @date: 2017/5/26
 */
public class SessionUtils {
    public static Map<String, Object> getAttribute(Session session, String key) {
        Map<String, Object> result  = (Map<String, Object>)session.getAttribute(key);
        return result;
    }

    public static String getAttribute(HttpSession httpSession, String key) {
        String result  = (String) httpSession.getAttribute(key);
        if (result == null) {
            result = "";
        }
        return result;
    }

    public static int getAttrInt(HttpSession httpSession, String key) {
        Integer result  = (Integer) httpSession.getAttribute(key);
        if (result == null) {
            result = -1;
        }
        return result;
    }
}
