package com.xzcs.util.web;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @desc: 
 * @author: chang
 * @date: 2017/5/22
 */
public class ResponseUtils {

    public static void writeText(Object data, HttpServletResponse response) {
        writeText(data, "UTF-8", response);
    }

    public static void writeText(Object data, String charset, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding(charset);
            out = response.getWriter();
            out.print(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != out) {
                out.flush();
                out.close();
            }
        }
    }

    public static void writeWeixMsg(Object data, String charset, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding(charset);
            out = response.getWriter();
            out.print(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeJson(Object data, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            JsonConfig jsonConfig = new JsonConfig();
            jsonConfig.registerJsonValueProcessor(java.sql.Timestamp.class, new JsonDateValueProcessor());
            response.setCharacterEncoding("UTF-8");
            out = response.getWriter();
            if (data instanceof Map) {
                out.print(JSONObject.fromObject(data, jsonConfig));
            } else if (data instanceof List) {
                JSONArray array = JSONArray.fromObject(((List) data).toArray(), jsonConfig);
                out.print(array);
            } else {
                out.print(JSONObject.fromObject(data, jsonConfig));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != out) {
                out.flush();
                out.close();
            }
        }
    }

    public static class JsonDateValueProcessor implements JsonValueProcessor {

        private String format = "yyyy-MM-dd HH:mm:ss";

        public Object processArrayValue(Object value, JsonConfig config) {
            return process(value);
        }

        public Object processObjectValue(String key, Object value, JsonConfig config) {
            return process(value);
        }

        private Object process(Object value){

            if(value instanceof Date){
                SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.UK);
                return sdf.format(value);
            }
            return value == null ? "" : value.toString();
        }
    }
    
}
