package com.xzcs.util.weixin;

import java.util.Random;

/**
 * @desc: 微信工具类
 * @author: chang
 * @date: 2017/10/28
 */
public class WeixinUtils {

	/**
	 * 获取token
	 * @param length 长度
	 * @param hasTime 是否含有时间戳
	 * @param hasNumber 是否含有数字
     * @return
     */
	public static String getToken(int length, int hasTime, int hasNumber){
		String base = "";
		if (hasNumber == 1){
			base="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
		} else {
			base="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}
		Random random = new Random();     
	    StringBuffer sb = new StringBuffer();     
	    for (int i = 0; i < length; i++) {     
	        int number = random.nextInt(base.length());     
	        sb.append(base.charAt(number));     
	    }  
		if (hasTime == 1){
			sb.append(System.currentTimeMillis()/1000);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(System.currentTimeMillis()/1000);
		System.out.println("getToken:"+getToken(6, 1, 0));
	}
}
