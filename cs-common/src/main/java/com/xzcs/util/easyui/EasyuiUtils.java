package com.xzcs.util.easyui;

import com.xzcs.util.common.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @desc: eayui工具类
 * @author: chang
 * @date: 2017/8/23
 */
public class EasyuiUtils {

    public static List<Map<String, Object>> getEasyuiTreeGridData(List<Map<String, Object>> list){
        List<Map<String, Object>> treeData = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> map : list) {
            String parentId = StringUtils.get(map, "parent_id");
            if (parentId.equals("-1")) {
                treeData.add(map);
                getChildOfTree(map, list);
            }
        }
        return treeData;
    }

    private static void getChildOfTree(Map<String, Object> map, List<Map<String, Object>> list){
        List<Map<String, Object>> children = new ArrayList<Map<String, Object>>();
        map.put("children", children);
        String parentMId = StringUtils.get(map, "module_id");
        for (Map<String, Object> menu : list) {
            String parentId = StringUtils.get(menu, "parent_id");
            if (parentId.equals(parentMId)){
                children.add(menu);
                getChildOfTree(menu, list);
            }
        }
    }
}
