package com.xzcs.util.mail;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Email implements Serializable {
	private static final long serialVersionUID = 1L;
	private String fromPersonal;
	private String fromAddress;
	private String[] to;
	private String[] cc;
	private String[] bcc;
	private String subject;
	private String sendDate;
	private String receiveDate;
	private int size;
	private String content;
	private List<Map<String, Object>> attachments;
	public String getFromPersonal() {
		return fromPersonal;
	}
	public void setFromPersonal(String fromPersonal) {
		this.fromPersonal = fromPersonal;
	}
	public String getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	public String[] getTo() {
		return to;
	}
	public void setTo(String[] to) {
		this.to = to;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	public String[] getBcc() {
		return bcc;
	}
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSendDate() {
		return sendDate;
	}
	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<Map<String, Object>> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Map<String, Object>> attachments) {
		this.attachments = attachments;
	}
	
	@Override
	public String toString() {
		System.out.println("attachments:"+attachments);
		return "subject:"+subject+"   from :"+fromAddress+"   sendDate:"+sendDate+"   receiveDate:"+receiveDate+
				"   size:"+size+"   content:"+content.replaceAll("<([^>]*)>", "");
	}
}
