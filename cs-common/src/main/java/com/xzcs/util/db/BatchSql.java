package com.xzcs.util.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BatchSql {
	private List<Map<String, Object>> sqlList = new ArrayList<Map<String, Object>>();

	public void addBatch(String sql, Object[] objects) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sql", sql);
		map.put("objects", objects);
		sqlList.add(map);
	}

	public void addBatch(String sql) {
		this.addBatch(sql, new Object[] {});
	}

	public List<Map<String, Object>> getSqlList() {
		return sqlList;
	}
	
	/**
	 * 清除批处理中记录
	 */
	public void clearBatch() {
	    this.sqlList.clear();
	}
}
