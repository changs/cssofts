package com.xzcs.util.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class MyViewResolver implements ViewResolver {

    private static Logger logger = LoggerFactory.getLogger(MyViewResolver.class);

    @Override
    public View resolveViewName(String viewName, Locale locale) throws Exception {
        String forwardUrl;
        if (viewName.startsWith("redirect:")) {
            forwardUrl = viewName.substring("redirect:".length());
            RedirectView view = new RedirectView(forwardUrl, true, true);
            return view;
        } else if (viewName.startsWith("forward:")) {
            forwardUrl = viewName.substring("forward:".length());
            return new InternalResourceView(forwardUrl);
        }
        return null;
    }
}
