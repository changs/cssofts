package com.xzcs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


/**
 * 首页
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/qymh")
public class QymhIndexController extends BaseController {
	
	@RequestMapping(value = "/")
	public String home(HttpServletRequest request) {
		request.setAttribute("page", "index");
		return "html/qymh/index";
	}

	@RequestMapping(value = "/index.shtml")
	public String index(HttpServletRequest request) {
		request.setAttribute("page", "index");
		return "html/qymh/index";
	}

}
