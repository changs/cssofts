//在页面未加载完毕之前显示的loading Html自定义内容
var _LoadingHtml = '<div id="loading" class="loading-wrap">' +
    '<div class="loading-content"><div class="loading-round"></div>' +
    '<div class="loading-dot"></div></div></div>';
//呈现loading效果
document.write(_LoadingHtml);
//监听加载状态改变
document.onreadystatechange = completeLoading;
//加载状态为complete时移除loading效果
function completeLoading() {
    if (document.readyState == "complete") {
        var loadingMask = document.getElementById('loading');
        loadingMask.parentNode.removeChild(loadingMask);
    }
}