var iframeName;//iframe操作对象

/*定义验证规则组*/
Validator = {
	Phone:/^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/, 
	Msisdn:/^1[3|4|5|7|8][0-9]\d{4,8}$/,
	Currency:/^\d+.?\d{0,2}$/,
	Number:/^\d+$/,
    Zip:/^[1-9]\d{5}$/,
	QQ:/^[1-9]\d{4,8}$/,
	Integer:/^[-\+]?\d+$/,
    IP:/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/,
    Email:/^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/,
	Double:/^[-\+]?\d+(\.\d+)?$/,
	English:/^[A-Za-z]+$/, 
	Chinese:/^[\u0391-\uFFE5]+$/, 
	Username:/^[a-z]\w{3,}$/i, 
	UnSafe:/^(([A-Z]*|[a-z]*|\d*|[-_\~!@#\$%\^&\*\.\(\)\[\]\{\}<>\?\\\/\'\"]*)|.{0,5})$|\s/,
    Password: /^[\S]{6,16}$/
}

/*重写验证规则*/
$.extend($.fn.validatebox.defaults.rules, {   
    Commbox: {//easyui下拉框必选
        validator: function(value, param){
            //alert($(param[1]+" option[text='"+value+"']").val());
            var x = "";
            $(param[1]+" option").each(function() {
                if($(this).text() == value) {
                    x = $(this).val();
                }
            });
            return x != '';
        },   
        message: '{0}' 
    },
    Phone: {//电话号码
        validator: function(value, param){  
            return Validator.Phone.test(value);   
        },   
        message: '{0}' 
    },
    Currency: {
        validator: function(value, param){  
            return Validator.Currency.test(value);   
        },   
        message: '{0}' 
    },
    Number: {//数字
        validator: function(value, param){  
            return Validator.Number.test(value);   
        },   
        message: '{0}' 
    },
    QQ: {
        validator: function(value, param){  
            return Validator.QQ.test(value);   
        },   
        message: '{0}' 
    },
    Msisdn: {//移动号码
        validator: function(value, param){  
            return Validator.Msisdn.test(value);   
        },   
        message: '{0}' 
    },
    Integer: {
        validator: function(value, param){  
            return Validator.Integer.test(value);   
        },   
        message: '{0}' 
    },
    Email: {
        validator: function(value, param){
            return Validator.Email.test(value);
        },
        message: '{0}'
    },
    Double: {
        validator: function(value, param){  
            return Validator.Double.test(value);   
        },   
        message: '{0}' 
    },
    English: {
        validator: function(value, param){  
            return Validator.English.test(value);   
        },   
        message: '{0}' 
    },
    Chinese: {
        validator: function(value, param){  
            return Validator.Chinese.test(value);   
        },   
        message: '{0}' 
    },
    Username: {
        validator: function(value, param){  
            return Validator.Username.test(value);   
        },   
        message: '{0}' 
    },
    UnSafe: {
        validator: function(value, param){  
            return Validator.UnSafe.test(value);   
        },   
        message: '{0}' 
    },
    Password: {
        validator: function(value, param){
            return Validator.Password.test(value);
        },
        message: '{0}'
    },
    Group: {
        validator: function(value, param){
            return mustChecked(value, param[1]);
        },
        message: '{0}'
    },
    /*必须和某个字段相等*/
    equalTo: {
        validator: function (value, param) {
            return $(param[1]).val() == value;
        },
        message: '{0}'
    }
});

/* 判定一组是否有一个选中 */
var mustChecked = function (value, name) {
    var groups = document.getElementsByName(name);
	var hasChecked = 0;
	min = 1;
	max = groups.length;
	for (var i = groups.length - 1; i >= 0; i--) {
		if (groups[i].checked) {
			hasChecked++;
		}
	}
	return min <= hasChecked && hasChecked <= max;
};

/*重写 combobox的过滤规则*/
$.extend($.fn.combobox.defaults, {
	filter: function(q, row){
		var opts = $(this).combobox('options');
		return row[opts.textField].indexOf(q) >= 0;
	}
});

easyui = {
	alert: function (msg, title) {
		$.messager.alert((title == null ? '系统提醒' : title), msg);
	},
    addTab: function(id, tabName, url, isClosed, iconCls){
        if(isClosed == null) isClosed = true;
        if(iconCls == null) iconCls = '';
        var tab = $('#maintabs').tabs('getTab', tabName);
        if(tab==null || (tab.panel('options').id != id)){//新增tab
            $('#maintabs').tabs('add',{
                id: id,
                title: tabName,
                content: '<iframe id="frm_'+id+'" name="frm_'+id+'" src="'+url+'" frameborder="0" width="100%" height="99.3%"></iframe>',
                iconCls: iconCls,
                closable: isClosed
            });
            bindTabEvent();
        } else {//更新tab
            $('#maintabs').tabs('update',{
                tab: tab,
                options: {
                    content:'<iframe id="frm_'+id+'" name="frm_'+id+'" src="'+url+'" frameborder="0" width="100%" height="99.3%"></iframe>'
                }
            });
            var tabIndex = $('#maintabs').tabs('getTabIndex',tab);//获取tab的索引
            $('#maintabs').tabs('select', tabIndex);//选中更新的tab
        }
    },
    createWindow: function(title, url, width, height, frameName){
        iframeName = frameName;
        parent.layer.open({
            type: 2,
            title: title,
            area: [width, height],
            skin: 'layui-layer-rim',//加上边框样式
            fixed: false, //不固定
            maxmin: true,
            content: url
        });
    },
    msgShow: function (title, msg, time, width, height){
        $.messager.show({
            title: title,
            msg: msg,
            width: width,
            height: height,
            timeout: time,
            showType: 'slide'
        });
    },
    confirm: function (title, msg, fn){
        $.messager.confirm(title, msg, fn);
    }
}