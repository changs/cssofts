wang = {
    customConfig: {
        downloadImgUrl: 'http://127.0.0.1/',
        uploadImgServer: '/sysmgr/file/upload.do',
        uploadImgMaxSize: 5 * 1024 * 1024,
        uploadImgMaxLength: 5,
        uploadFileName: 'files',
        uploadImgParams: {
            moduleName: 'wang_editor'
        },
        uploadImgHooks: {
            customInsert: function(insertImg, result, editor){
                insertImg(editor.customConfig.downloadImgUrl+result.filePath);
            }
        }
    },
    createEditor: function(id, app, imgSite, opt){
        var config = jQuery.extend(true, this.customConfig, opt);
        config.uploadImgServer = app + config.uploadImgServer;
        config.downloadImgUrl = imgSite;
        var E = window.wangEditor;
        var editor;
        editor = new E(id);
        editor.customConfig = config;
        editor.create();
        return editor;
    }
}