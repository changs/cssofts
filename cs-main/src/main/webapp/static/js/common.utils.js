common = {
	ajax: function(url, data, method, dataType, async, sucCallBack, errCallBack){
		async
		$.ajax({
			async: async == null ? false : async,
			url: url,
			data: data,
			cache: false, //设置为false将不会从浏览器缓存中加载请求信息
			type: method,
			dataType: dataType == null ? "text" : dataType,
			success: sucCallBack,
			error: errCallBack !== null ? errCallBack : function (XMLHttpRequest, textStatus, errorThrown) {
				alert("后台服务请求失败:"+errorThrown);
			}
		});
	},

	isWeixin: function(){
		var ua = navigator.userAgent.toLowerCase();
		var isWeixin = ua.indexOf('micromessenger') != -1;
		if (isWeixin) {
			return true;
		} else {
			return false;
		}
	}
};

+function ($) {
	'use strict';

	$.fn.extend({
		/**
		 * 将表单数据转成JSON对象 用法：$(form).serializeJson()
		 */
		serializeJson: function () {
			var o = {};
			var a = this.serializeArray();

			$.each(a, function () {
				if (o[this.name] !== undefined) {
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});

			return o;
		},
		/**
		 * 输出firebug日志
		 * @param {Object} msg
		 */
		log: function(msg) {
			return this.each(function() {
				if (console) console.log('%s: %o', msg, this);
			});
		}
	});

	/**
	 * 扩展String方法
	 */
	$.extend(String.prototype, {
		isInteger: function() {
			return (new RegExp(/^\d+$/).test(this));
		},
		isNumber: function() {
			return (new RegExp(/^([-]{0,1}(\d+)[\.]+(\d+))|([-]{0,1}(\d+))$/).test(this));
		},
		trim: function() {
			return $.trim(this);
		},
		startsWith: function (pattern) {
			return this.indexOf(pattern) === 0;
		},
		endsWith: function(pattern) {
			var d = this.length - pattern.length;
			return d >= 0 && this.lastIndexOf(pattern) === d;
		},
		replaceAll: function(os, ns) {
			return this.replace(new RegExp(os, 'gm'), ns);
		},
		isValidPwd: function() {
			return (new RegExp(/^([_]|[a-zA-Z0-9]){6,32}$/).test(this));
		},
		isValidMail: function() {
			return(new RegExp(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/).test(this.trim()));
		},
		isSpaces: function() {
			for (var i = 0; i < this.length; i += 1) {
				var ch = this.charAt(i);

				if (ch!=' '&& ch!='\n' && ch!='\t' && ch!='\r')
					return false;
			}
			return true;
		},
		isPhone:function() {
			return (new RegExp(/(^([0-9]{3,4}[-])?\d{3,8}(-\d{1,6})?$)|(^\([0-9]{3,4}\)\d{3,8}(\(\d{1,6}\))?$)|(^\d{3,8}$)/).test(this));
		},
		isUrl:function() {
			return (new RegExp(/^[a-zA-z]+:\/\/([a-zA-Z0-9\-\.]+)([-\w .\/?%&=:]*)$/).test(this));
		},
		toJson: function() {
			var json = this;

			try {
				if (typeof json === 'object')
					json = json.toString();
				if (!json.trim().match("^\{(.+:.+,*){1,}\}$"))
					return this;
				else
					return JSON.parse(this);
			} catch (e) {
				return this;
			}
		},
		/**
		 * String to Function
		 * 参数(方法字符串或方法名)： 'function(){...}' 或 'getName' 或 'USER.getName' 均可
		 * Author: K'naan
		 */
		toFunc: function() {
			if (!this || !this.length)
				return undefined;

			if (this.startsWith('function')) {
				return (new Function('return '+ this))();
			}

			try {
				var m_arr = this.split('.'), fn = window;

				for (var i = 0; i < m_arr.length; i++) {
					fn = fn[m_arr[i]];
				}

				if (typeof fn === 'function') {
					return fn;
				}
			} catch (e) {
				return undefined;
			}
		}
	});

	/* Function */
	$.extend(Function.prototype, {
		//to fixed String.prototype -> toFunc
		toFunc: function() {
			return this;
		}
	})

	/* Array */
	$.extend(Array.prototype, {
		remove: function(index) {
			if (index < 0)
				return this;
			else
				return this.slice(0, index).concat(this.slice(index + 1, this.length));
		},
		swap: function(indexA, indexB) {
			var arr = this, temp = arr[indexA];

			arr[indexA] = arr[indexB];
			arr[indexB] = temp;

			return this;
		},
		move: function(old_index, new_index) {
			if (old_index < 0 || new_index < 0)
				return this;

			if (new_index >= this.length) {
				var k = new_index - this.length;

				while ((k--) + 1) {
					this.push(undefined);
				}
			}

			this.splice(new_index, 0, this.splice(old_index, 1)[0]);

			return this;
		},
		// move continuous item
		moveItems: function(startIndex, newIndex, len) {
			if (!len || newIndex == startIndex)
				return this;

			var moveArr = this.slice(startIndex, startIndex + len);

			if (startIndex > newIndex)
				this.splice(startIndex, len);

			this.splice.apply(this, [newIndex, 0].concat(moveArr));

			if (startIndex < newIndex)
				this.splice(startIndex, len);

			return this;
		},
		/* serializeArray to json */
		toJson: function() {
			var o = {};
			var a = this;

			$.each(a, function () {
				if (o[this.name] !== undefined) {
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			})

			return o;
		}
	});

	/* Global */
	$.isJson = function(obj) {
		var flag = true;

		try {
			flag = $.parseJSON(obj);
		} catch (e) {
			return false;
		}
		return flag ? true : false;
	}

}(jQuery);