package com.xzcs.directive;

import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component("fl_checkbox")
public class CheckBoxDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	private String id;
	private String name;
	private String otherAttr;
	private int count = 0;
	private int index = 0;
	private int cols = 10000;
	private String[] values;

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		id = DirectiveUtils.getString("id", params);
		name = DirectiveUtils.getString("name", params);
		String value = DirectiveUtils.getString("value", params);
		cols = DirectiveUtils.getInt("columns", params);
		String delims = DirectiveUtils.getString("delims", params);
		otherAttr = DirectiveUtils.getString("otherAttr", params);
		String checkboxLabel = DirectiveUtils.getString("checkboxLabel", params);
		String checkboxValue = DirectiveUtils.getString("checkboxValue", params);
		TemplateModel model = (TemplateModel) params.get("list");

		StringBuffer sb = new StringBuffer();
		if (model instanceof TemplateSequenceModel) {
			values = value.split(delims);
			TemplateSequenceModel tsModel = (TemplateSequenceModel) model;
			count = tsModel.size();
			sb.append(printTableStart());
			for (int i = 0; i < tsModel.size(); i++) {
				TemplateHashModel model1 = (TemplateHashModel)tsModel.get(i);
				String label1 = checkboxLabel == null ? "" : DirectiveUtils.getString(model1.get(checkboxLabel));
				String value1 = checkboxValue == null ? "" : DirectiveUtils.getString(model1.get(checkboxValue));
				sb.append(printTrStart());
				sb.append(printCheckbox(label1, value1));
				sb.append(printTrEnd());
			}
			sb.append(printTableEnd());
		}

		env.setVariable(DirectiveUtils.OUT_HTML, DirectiveUtils.getDefaultObjectWrapper().wrap(sb.toString()));
		body.render(env.getOut());
	}

	public String printTableStart() {
		StringBuffer sb = new StringBuffer();
		sb.append("<table style=\"border:0;\" width=\"100%\">");
		return sb.toString();
	}

	public String printTableEnd() {
		StringBuffer sb = new StringBuffer();
		sb.append("</table>");
		return sb.toString();
	}

	public String printTrStart() {
		StringBuffer sb = new StringBuffer();
		if(index % cols == 0) {
			sb.append("<tr style=\"border:0\">");
		}
		return sb.toString();
	}

	public String printTrEnd() {
		StringBuffer sb = new StringBuffer();
		if(index == count || index % cols == 0) {
			sb.append("</tr>");
		}
		return sb.toString();
	}

	public String printCheckbox(String checkboxName, String checkboxValue) {
		StringBuffer sb = new StringBuffer();
		if (index == count -1 && count % cols != 0) {
			sb.append("<td width='" + 100.0/(cols==10000?count:cols) + "%' colspan='"+(cols-(count%cols)+1)+"' style=\"border:0;text-align:left;font-size: 12px;\">");
		} else {
			sb.append("<td width='" + 100.0/(cols==10000?count:cols) + "%' style=\"border:0;text-align:left;font-size: 12px;\">");
		}
		sb.append("<input type=\"checkbox\" value='" + checkboxValue+ "' style=\"vertical-align:-2px;\"");
		if (id != null)
			sb.append("id=\"" + id + "\" ");
		if (name != null)
			sb.append("name=\"" + name + "\" ");
		if (otherAttr != null)
			sb.append(otherAttr);
		for(int i=0;i<values.length;i++) {
			if(values[i].equals(checkboxValue)) {
				sb.append("checked=\"checked\" ");
				break;
			}
		}
		sb.append("><label style=\"font-size:13px;\">" + checkboxName + "</label></input>");
		sb.append("</td>");
		index ++;
		return sb.toString();
	}
}
