package com.xzcs.directive;

import com.xzcs.mapper.user.UserMapper;
import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc: 模块列表
 * @author: chang
 * @date: 2017/6/16
 */
@Component("fl_module_list")
public class ModuleListDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserMapper userMapper;

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		String userId = DirectiveUtils.getString("userId", params);
		String moduleLevel = DirectiveUtils.getString("moduleLevel", params);
		String parentId = DirectiveUtils.getString("parentId", params);//大于的level
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("moduleLevel", moduleLevel);
		map.put("parentId", parentId);
		List<Map<String, Object>> list = userMapper.findUserModule(map);
		//老的写法，已经过时了
		//ObjectWrapper.DEFAULT_WRAPPER.wrap(list);
		env.setVariable(DirectiveUtils.OUT_LIST, DirectiveUtils.getDefaultObjectWrapper().wrap(list));

		body.render(env.getOut());
	}
}
