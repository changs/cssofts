package com.xzcs.directive;

import com.xzcs.util.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component("fl_easy_menu")
public class EasyMenuDirective implements TemplateDirectiveModel {

	public Logger logger = LoggerFactory.getLogger(getClass());

	public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
		String app = DirectiveUtils.getString("app", params);
		TemplateModel model = (TemplateModel) params.get("list");

		StringBuffer sb = new StringBuffer();
		sb.append("<div class=\"easyui-accordion\">");
		sb.append(getEasyuiMultisTree((TemplateSequenceModel) model, app));
		sb.append("</div>");

		env.setVariable(DirectiveUtils.OUT_HTML, DirectiveUtils.getDefaultObjectWrapper().wrap(sb.toString()));
		body.render(env.getOut());
	}

	public String getEasyuiMultisTree(TemplateSequenceModel model, String app)
			throws TemplateException {
		StringBuffer menuString = new StringBuffer();
		if (null == model) {
			return menuString.toString();
		}
		for (int i = 0; i < model.size(); i++) {
			TemplateHashModel model1 = (TemplateHashModel)model.get(i);
			String moduleId = DirectiveUtils.getString(model1.get("module_id"));
			String moduleName = DirectiveUtils.getString(model1.get("module_name"));
			String moduleLevel = DirectiveUtils.getString(model1.get("module_level"));
			if (moduleLevel.equals("2")) {
				menuString.append("<div title=\"" + moduleName
						+ "\" iconCls=\"fa fa-folder fa-lg\">");
				menuString.append("<ul class=\"easyui-tree\">");
				menuString.append(getChildOfTree(moduleId, model, app));
				menuString.append("</ul></div>");
			}
		}
		return menuString.toString();
	}

	private String getChildOfTree(String parentMId, TemplateSequenceModel model, String app)
			throws TemplateException {
		StringBuffer menuString = new StringBuffer();
		for (int i = 0; i < model.size(); i++) {
			TemplateHashModel model1 = (TemplateHashModel)model.get(i);
			String moduleId = DirectiveUtils.getString(model1.get("module_id"));
			String moduleName = DirectiveUtils.getString(model1.get("module_name"));
			String moduleUrl = DirectiveUtils.getString(model1.get("module_url"));
			String parentId = DirectiveUtils.getString(model1.get("parent_id"));
			if (parentId.equals(parentMId)){
				if(!moduleUrl.equals("")){
					menuString.append(getLeafOfTree(model1, app));
				}else {
					menuString.append("<li><span>"+moduleName+"</span>");
					menuString.append("<ul>");
					menuString.append(getChildOfTree(moduleId, model, app));
					menuString.append("</ul></li>");
				}
			}
		}
		return menuString.toString();
	}

	private String getLeafOfTree(TemplateHashModel model, String app)
			throws TemplateException {
		StringBuffer menuString = new StringBuffer();
		String moduleId = DirectiveUtils.getString(model.get("module_id"));
		String moduleName = DirectiveUtils.getString(model.get("module_name"));
		String moduleUrl = DirectiveUtils.getString(model.get("module_url"));
		String icon = "fa fa-file-text-o";
		menuString.append("<li><a onclick=\"easyui.addTab(\'");
		menuString.append(moduleId);
		menuString.append("\',\'");
		menuString.append(moduleName);
		menuString.append("\',\'");
		menuString.append(app + moduleUrl);
		menuString.append("\',\'");
		menuString.append("true");
		menuString.append("\',\'");
		menuString.append(icon);
		menuString.append("\')\" title=\"");
		menuString.append(moduleName);
		menuString.append("\" url=\"");
		menuString.append(moduleUrl);
		menuString.append("\" href=\"javascript:;\" >");
		menuString.append(moduleName);
		menuString.append("</a></li>");
		return menuString.toString();
	}
}
