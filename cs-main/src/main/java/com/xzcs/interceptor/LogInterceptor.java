package com.xzcs.interceptor;

import com.xzcs.service.sysmgr.OperatingService;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.common.IpUtils;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.common.URLUtils;
import com.xzcs.util.web.SessionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志记录
 * @author chang
 */
public class LogInterceptor extends HandlerInterceptorAdapter {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private OperatingService operatingService;
	private String excludeUrls = "";
	public void setExcludeUrls(String excludeUrls) {
		this.excludeUrls = excludeUrls;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		request.setAttribute("reqTime", DateUtils.getToday("yyyy-MM-dd HH:mm:ss"));
		response.addHeader("P3P", "CP=CAO PSA OUR");
		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		String reqUrl = URLUtils.getURI(request);
		String reqReferer = StringUtils.notEmpty(request.getHeader("Referer"));
		String reqMethod = request.getMethod();
		String reqParams = URLUtils.getReqInfo(request);
		String userAgent = StringUtils.notEmpty(request.getHeader("User-Agent"));
		String clientIp = IpUtils.getClientIP(request);
		int clientPort = request.getRemotePort();
		String serverIp = request.getLocalAddr();
		//String serverMac = StringUtils.listToString(IpUtils.getAllLocalMac(), ",");
		int serverPort = request.getLocalPort();
		Subject subject = SecurityUtils.getSubject();
		Session session	= subject.getSession();
		String sessionId = session.getId().toString();
		String reqTime = (String) request.getAttribute("reqTime");
		reqTime = (null != reqTime) ? reqTime : DateUtils.getToday("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reqUrl", reqUrl);
		params.put("reqTime", reqTime);
		params.put("reqReferer", reqReferer);
		params.put("reqMethod", reqMethod);
		params.put("reqParams", reqParams);
		params.put("userAgent", userAgent);
		params.put("clientIp", clientIp);
		params.put("clientPort", clientPort);
		params.put("serverIp", serverIp);
		//params.put("serverMac", serverMac);
		params.put("serverPort", serverPort);
		params.put("sessionId", sessionId);
		params.put("endTime", DateUtils.getToday("yyyy-MM-dd HH:mm:ss"));
		if (subject.isAuthenticated()) {
			params.put("userId", StringUtils.get(SessionUtils.getAttribute(session, "user"), "user_id"));
		} else {
			params.put("userId", "");
		}
		operatingService.insertLog(request, params);
		super.afterCompletion(request, response, handler, ex);
	}
}