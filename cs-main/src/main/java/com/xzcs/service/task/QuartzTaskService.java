package com.xzcs.service.task;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.mapper.task.QuartzTaskMapper;
import com.xzcs.quartz.BaseJob;
import com.xzcs.quartz.QuartzUtils;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QuartzTaskService extends BaseService {

	@Autowired
	private QuartzTaskMapper quartzTaskMapper;
	@Autowired
	private SequenceMapper sequenceMapper;
	@Autowired
	private QuartzUtils quartzUtils;
	
	public PageInfo<Map<String, Object>> findQuartzTasks(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = quartzTaskMapper.findQuartzTasks(params);
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}
	
	public Map<String, Object> findQuartzTask(HttpServletRequest request){
		String taskId = RequestUtils.getValue(request, "taskId");
		return quartzTaskMapper.findQuartzTask(taskId);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveQuartzTask(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		String taskId = RequestUtils.getValue(request, "taskId");
		String cronExpression = RequestUtils.getValue(request, "cronExpression").replace("／", "/");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("cronExpression", cronExpression);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("create")) {
			taskId = sequenceMapper.getSequenceValue("seq_common_id")+"";
			params.put("taskId", taskId);
			quartzTaskMapper.insertQuartzTask(params);
		} else {
			params.put("taskId", taskId);
			quartzTaskMapper.updateQuartzTask(params);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteQuartzTask(HttpServletRequest request){
		String taskId = RequestUtils.getValue(request, "taskId");
		Map<String, Object> quartzTask = quartzTaskMapper.findQuartzTask(taskId);
		quartzTaskMapper.deleteQuartzTask(taskId);
		String jobName = StringUtils.get(quartzTask, "job_name");
		quartzUtils.removeJob(jobName, jobName);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void startQuartzTask(HttpServletRequest request){
		String taskId = RequestUtils.getValue(request, "taskId");
		quartzTaskMapper.updateQuartzTaskStatus(taskId, "2");
		Map<String, Object> quartzTask = findQuartzTask(request);
		String jobName = StringUtils.get(quartzTask, "job_name");
		String className = StringUtils.get(quartzTask, "class_name");
		String cronExpression = StringUtils.get(quartzTask, "cron_expression");
		Map<String, Object> jobParams = new HashMap<String, Object>();
		jobParams.put("taskId", taskId);
		jobParams.put("taskClass", className);
		quartzUtils.addJob(jobName, jobName, BaseJob.class, cronExpression, jobParams);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void stopQuartzTask(HttpServletRequest request){
		String taskId = RequestUtils.getValue(request, "taskId");
		quartzTaskMapper.updateQuartzTaskStatus(taskId, "1");
		Map<String, Object> quartzTask = findQuartzTask(request);
		String jobName = StringUtils.get(quartzTask, "job_name");
		quartzUtils.removeJob(jobName, jobName);
	}
}
