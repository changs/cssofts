package com.xzcs.service.sysmgr;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.mapper.user.UserMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.Constants;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserMgrService extends BaseService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private SequenceMapper sequenceMapper;

	public PageInfo<Map<String, Object>> findUsers(HttpServletRequest request){
		String pageNum = RequestUtils.getValue(request, "page");//页数
		String pageSize = RequestUtils.getValue(request, "rows");//每页条数
		pageNum = pageNum.equals("") ? "1" : pageNum;
		pageSize = pageSize.equals("") ? getDefaultPageSize() : pageSize;
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		PageHelper.startPage(StringUtils.toInt(pageNum), StringUtils.toInt(pageSize));
		List<Map<String, Object>> list = userMapper.findUsers((params));
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		return page;
	}

	public Map<String, Object> findUser(HttpServletRequest request){
		String userId = RequestUtils.getValue(request, "userId");
		return userMapper.findByUserId(userId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveUser(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("curUser", StringUtils.get(user, "user_id"));
		if (method.equals("edit")) {
			userMapper.updateUser(params);
		} else if(method.equals("create")){//新增
			String userId = sequenceMapper.getSequenceValue("seq_user_id") + "";
			params.put("userId", userId);
			params.put("userPwd", StringUtils.md5(propertyUtils.getPropertyValue(Constants.DEFAULT_PASSWORD)));
			userMapper.insertUser(params);
			Map<String, String> params2 = new HashMap<String, String>();
			params2.put("roleId", propertyUtils.getPropertyValue(Constants.DEFAULT_ROLEID));
			params2.put("userId", userId);
			userMapper.saveUserRole(params);
		}
	}

	public int checkUserName(HttpServletRequest request){
		String userId = RequestUtils.getValue(request, "userId");
		String userName = RequestUtils.getValue(request, "userName");
		return userMapper.checkUserName(userName, userId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteUser(HttpServletRequest request){
		String userId = RequestUtils.getValue(request, "userId");
		userMapper.deleteUser(userId);
		//userMapper.deleteUserRole(userId);
	}

	public String findUserRoles(HttpServletRequest request){
		String userId = RequestUtils.getValue(request, "userId");
		List<Map<String, Object>> roles = userMapper.findUserRole(userId);
		String roleIds = StringUtils.listToString(roles, "role_id", ",");
		return roleIds;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveUserRole(HttpServletRequest request){
		String userId = RequestUtils.getValue(request, "userId");
		String[] roleIds = RequestUtils.getValues(request, "roleId");
		userMapper.deleteUserRole(userId);
		for (int i = 0; i < roleIds.length; i++) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			params.put("roleId", roleIds[i]);
			userMapper.saveUserRole(params);
		}
	}

	public void modifyPassword(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		userMapper.updateUserPwd(params);
	}
}
