package com.xzcs.service.sysmgr;

import com.xzcs.mapper.role.RoleMapper;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Service
public class RoleService extends BaseService {

	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private SequenceMapper sequenceMapper;
	
	public List<Map<String, Object>> findRoles(HttpServletRequest request){
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		List<Map<String, Object>> list = roleMapper.findRoles(params);
		return list;
	}

	public Map<String, Object> findRole(HttpServletRequest request){
		String roleId = RequestUtils.getValue(request, "roleId");
		return roleMapper.findRole(roleId);
	}

	public void saveRole(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> params = RequestUtils.getRequestMap(request);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("edit")) {
			roleMapper.updateRole(params);
		} else {
			params.put("roleId", sequenceMapper.getSequenceValue("seq_role_id"));
			roleMapper.insertRole(params);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteRole(HttpServletRequest request){
		String roleId = RequestUtils.getValue(request, "roleId");
		roleMapper.deleteRole(roleId);
		roleMapper.deleteRoleModule(roleId);
		roleMapper.deleteRolePermit(roleId);
	}

	public int checkRoleName(HttpServletRequest request){
		String roleId = RequestUtils.getValue(request, "roleId");
		String roleName = RequestUtils.getValue(request, "roleName");
		return roleMapper.checkRoleName(roleName, roleId);
	}

	public List<Map<String, Object>> findPermitTree(HttpServletRequest request){
		String roleId = RequestUtils.getValue(request, "roleId");
		List<Map<String, Object>> list = roleMapper.findPermitTree(roleId);
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveRolePermit(HttpServletRequest request){
		String roleId = RequestUtils.getValue(request, "roleId");
		String permitIds = RequestUtils.getValue(request, "permitId");
		String flags = RequestUtils.getValue(request, "flag");
		String[] permitId = permitIds.split(",");
		String[] flag = flags.split(",");
		roleMapper.deleteRoleModule(roleId);
		roleMapper.deleteRolePermit(roleId);
		for (int i=0 ; i < permitId.length; i ++) {
			if(flag[i].equals("1")){
				roleMapper.insertRoleModule(roleId, permitId[i]);
			}
			if(flag[i].equals("2")){
				roleMapper.insertRolePermit(roleId, permitId[i]);
			}
		}
	}
}
