package com.xzcs.service.sysmgr;

import com.xzcs.mapper.module.ModulePermitMapper;
import com.xzcs.mapper.role.RoleMapper;
import com.xzcs.mapper.sequence.SequenceMapper;
import com.xzcs.service.BaseService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ModulePermitService extends BaseService {

	@Autowired
	private ModulePermitMapper modulePermitMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private SequenceMapper sequenceMapper;
	
	public List<Map<String, Object>> findModules(HttpServletRequest request){
		String moduleName = RequestUtils.getValue(request, "moduleName");
		String moduleType = RequestUtils.getValue(request, "moduleType");
		String parentId = RequestUtils.getValue(request, "parentId");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("moduleName", moduleName);
		params.put("moduleType", moduleType);
		params.put("parentId", parentId);

		List<Map<String, Object>> list = modulePermitMapper.findModules(params);
		return list;
	}

	public Map<String, Object> findModule(HttpServletRequest request){
		String moduleId = RequestUtils.getValue(request, "moduleId");
		return modulePermitMapper.findModule(moduleId);
	}

	public void saveModule(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		String moduleId = RequestUtils.getValue(request, "moduleId");
		String moduleName = RequestUtils.getValue(request, "moduleName");
		String parentIdLevel = RequestUtils.getValue(request, "parentId");
		String moduleType = RequestUtils.getValue(request, "moduleType");
		String moduleOrder = RequestUtils.getValue(request, "moduleOrder");
		String moduleIcon = RequestUtils.getValue(request, "moduleIcon");
		String moduleCode = RequestUtils.getValue(request, "moduleCode");
		String moduleUrl = RequestUtils.getValue(request, "moduleUrl").replaceAll("／", "/");
		String moduleRemark = RequestUtils.getValue(request, "moduleRemark");
		String parentId = parentIdLevel.split(",")[0];
		String parentLevel = parentIdLevel.split(",")[1];
		int moduleLevel = StringUtils.toInt(parentLevel) + 1;
		String modulePath = modulePermitMapper.findModulePath(parentId);
		if (null != modulePath) {
			modulePath = modulePath + " - " + moduleName;
		} else {
			modulePath = moduleName;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("moduleId", moduleId);
		params.put("moduleName", moduleName);
		params.put("parentId", parentId);
		params.put("moduleType", moduleType);
		params.put("moduleLevel", moduleLevel);
		params.put("moduleOrder", moduleOrder);
		params.put("moduleIcon", moduleIcon);
		params.put("moduleCode", moduleCode);
		params.put("moduleUrl", moduleUrl);
		params.put("moduleRemark", moduleRemark);
		params.put("modulePath", modulePath);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("edit")) {
			modulePermitMapper.updateModule(params);
		} else {
			params.put("moduleId", sequenceMapper.getSequenceValue("seq_module_id"));
			modulePermitMapper.insertModule(params);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteModule(HttpServletRequest request){
		String moduleId = RequestUtils.getValue(request, "moduleId");
		modulePermitMapper.deleteModule(moduleId);
		modulePermitMapper.deletePermitWithModule(moduleId);
		roleMapper.deleteRoleModuleWithModule(moduleId);
	}

	public List<Map<String, Object>> findPermitWithModule(HttpServletRequest request){
		String moduleId = RequestUtils.getValue(request, "moduleId");
		List<Map<String, Object>> list = modulePermitMapper.findPerimitWithModule(moduleId);
		return list;
	}

	public Map<String, Object> findPermit(HttpServletRequest request){
		String permitId = RequestUtils.getValue(request, "permitId");
		return modulePermitMapper.findPermit(permitId);
	}

	public void savePermit(HttpServletRequest request){
		Map<String, Object> user = this.getUser(request);
		String method = RequestUtils.getValue(request, "method");
		String permitId = RequestUtils.getValue(request, "permitId");
		String permitName = RequestUtils.getValue(request, "permitName");
		String moduleId = RequestUtils.getValue(request, "moduleId");
		String permitCode = RequestUtils.getValue(request, "permitCode");
		String permitUrl = RequestUtils.getValue(request, "permitUrl").replaceAll("／", "/");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("permitId", permitId);
		params.put("permitName", permitName);
		params.put("moduleId", moduleId);
		params.put("permitCode", permitCode);
		params.put("permitUrl", permitUrl);
		params.put("userId", StringUtils.get(user, "user_id"));
		if (method.equals("edit")) {
			modulePermitMapper.updatePermit(params);
		} else {
			params.put("permitId", sequenceMapper.getSequenceValue("seq_module_id"));
			modulePermitMapper.insertPermit(params);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deletePermit(HttpServletRequest request){
		String permitId = RequestUtils.getValue(request, "permitId");
		modulePermitMapper.deletePermit(permitId);
		roleMapper.deleteRolePermitWithPermit(permitId);
	}

	public int checkCode(HttpServletRequest request){
		String id = RequestUtils.getValue(request, "id");
		String code = RequestUtils.getValue(request, "code");
		return modulePermitMapper.checkCode(code, id);
	}

	public String findModuleRoles(HttpServletRequest request){
		String moduleId = RequestUtils.getValue(request, "moduleId");
		List<Map<String, Object>> roles = modulePermitMapper.findModuleRoles(moduleId);
		String roleIds = StringUtils.listToString(roles, "role_id", ",");
		return roleIds;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveModuleRole(HttpServletRequest request){
		String moduleId = RequestUtils.getValue(request, "moduleId");
		String[] roleIds = RequestUtils.getValues(request, "roleId");
		modulePermitMapper.deleteModuleRoleWithModule(moduleId);
		for (int i = 0; i < roleIds.length; i++) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("moduleId", moduleId);
			params.put("roleId", roleIds[i]);
			modulePermitMapper.insertModuleRole(params);
		}
	}

	public String findPermitRoles(HttpServletRequest request){
		String permitId = RequestUtils.getValue(request, "permitId");
		List<Map<String, Object>> roles = modulePermitMapper.findPermitRoles(permitId);
		String roleIds = StringUtils.listToString(roles, "role_id", ",");
		return roleIds;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void savePermitRole(HttpServletRequest request){
		String permitId = RequestUtils.getValue(request, "permitId");
		String[] roleIds = RequestUtils.getValues(request, "roleId");
		modulePermitMapper.deletePermitRoleWithPermit(permitId);
		for (int i = 0; i < roleIds.length; i++) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("permitId", permitId);
			params.put("roleId", roleIds[i]);
			modulePermitMapper.insertPermitRole(params);
		}
	}
}
