package com.xzcs.service.sysmgr;

import com.xzcs.service.BaseService;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Properties;

@Service
public class SiteMgrService extends BaseService {

    public void updateProperties(File file, String key, String value) {
        Properties props = new Properties();
        InputStream in;
        try {
            in = new FileInputStream(file);
            props.load(in);
            FileOutputStream out = new FileOutputStream(file);
            props.setProperty(key, value);
            props.store(out, null);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
