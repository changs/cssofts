package com.xzcs.listener;

import com.xzcs.util.Constants;
import com.xzcs.util.spring.PropertyUtils;
import com.xzcs.util.web.RequestUtils;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.io.File;
import java.util.Properties;

public class AppContextListener extends ContextLoaderListener {
	@Override
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		ServletContext servletContext = event.getServletContext();
		servletContext.setAttribute("app", servletContext.getContextPath());// 设置系统名称
		
		String filePath = new RequestUtils().getFilePath(servletContext, Constants.SYSTEM_PROPERTY_PATH);
		Properties properties = PropertyUtils.getProperties(new File(filePath));
		servletContext.setAttribute("home", properties.getProperty("xzcs.site.home"));
		servletContext.setAttribute("title", properties.getProperty("xzcs.site.title"));
		servletContext.setAttribute("keywords", properties.getProperty("xzcs.site.keywords"));
		servletContext.setAttribute("description", properties.getProperty("xzcs.site.description"));
		servletContext.setAttribute("imgSite", properties.getProperty("xzcs.images.site"));
		
		//ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
	}
	
}
