package com.xzcs.controller.sysmgr;

import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.SiteMgrService;
import com.xzcs.util.Constants;
import com.xzcs.util.spring.PropertyUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * 网站信息管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/sysmgr/site/")
public class SiteMgrController extends BaseController {
	
	@Autowired
	private SiteMgrService siteMgrService;
	
	@RequestMapping(value = "frame.do")
	public String frame(HttpServletRequest request) {

		return "html/sysmgr/site/frame";
	}
	
	@RequestMapping(value = "save.do")
	public void save(HttpServletRequest request, HttpServletResponse response){
		String home = RequestUtils.getValue(request, "home").replaceAll("／", "/");
		String title = RequestUtils.getValue(request, "title");
		String keywords = RequestUtils.getValue(request, "keywords");
		String siteDesc = RequestUtils.getValue(request, "siteDesc");
		String filePath = RequestUtils.getFilePath(request, Constants.SYSTEM_PROPERTY_PATH);
		siteMgrService.updateProperties(new File(filePath), "xzcs.site.home", home);
		siteMgrService.updateProperties(new File(filePath), "xzcs.site.title", title);
		siteMgrService.updateProperties(new File(filePath), "xzcs.site.keywords", keywords);
		siteMgrService.updateProperties(new File(filePath), "xzcs.site.description", siteDesc);

		Properties properties = PropertyUtils.getProperties(new File(filePath));
		ServletContext servletContext = request.getServletContext();
		servletContext.setAttribute("home", properties.getProperty("xzcs.site.home"));
		servletContext.setAttribute("title", properties.getProperty("xzcs.site.title"));
		servletContext.setAttribute("keywords", properties.getProperty("xzcs.site.keywords"));
		servletContext.setAttribute("description", properties.getProperty("xzcs.site.description"));

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", 1);
		result.put("msg", getMessage("operating.type.success"));
		ResponseUtils.writeJson(result, response);
	}
}
