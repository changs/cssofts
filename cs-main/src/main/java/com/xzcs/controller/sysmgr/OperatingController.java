package com.xzcs.controller.sysmgr;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.OperatingService;
import com.xzcs.util.common.DateUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 日志查询
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/sysmgr/")
public class OperatingController extends BaseController {
	
	@Autowired
	private OperatingService operatingService;
	
	@RequestMapping(value = "log/frame.do")
	public String frame(HttpServletRequest request) {
		request.setAttribute("startTime", DateUtils.getDay(-7, "yyyy-MM-dd"));
		request.setAttribute("endTime", DateUtils.getToday("yyyy-MM-dd"));
		return "html/sysmgr/log/frame";
	}
	
	@RequestMapping(value = "log/list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = operatingService.findOperatings(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}
}
