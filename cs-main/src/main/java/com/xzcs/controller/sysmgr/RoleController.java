package com.xzcs.controller.sysmgr;

import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.ModulePermitService;
import com.xzcs.service.sysmgr.RoleService;
import com.xzcs.util.common.GsonUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/sysmgr/")
public class RoleController extends BaseController {
	
	@Autowired
	private ModulePermitService modulePermitService;
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "role/frame.do")
	public String frame(HttpServletRequest request) {
		return "html/sysmgr/role/frame";
	}
	
	@RequestMapping(value = "role/list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> roles = roleService.findRoles(request);
		ResponseUtils.writeJson(roles, response);
	}

	@RequestMapping(value = "role/edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (method.equals("edit")) {
			Map<String, Object> role = roleService.findRole(request);
			request.setAttribute("role", role);
		}
		return "html/sysmgr/role/edit";
	}

	@RequestMapping(value = "role/save.do")
	public void saveRole(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			roleService.saveRole(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "role/delete.do")
	public void deleteRole(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			roleService.deleteRole(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "role/checkRoleName.do")
	public void checkRoleName(HttpServletRequest request, HttpServletResponse response){
		int result = 0;
		try {
			result = roleService.checkRoleName(request);
		} catch (Exception e) {
			result = 0;
		}
		if (result > 0) {
			ResponseUtils.writeText("false", response);
		} else {
			ResponseUtils.writeText("true", response);
		}
	}

	@RequestMapping(value = "role/permitTree.do")
	public String permitTree(HttpServletRequest request){
		List<Map<String, Object>> list = roleService.findPermitTree(request);
		request.setAttribute("node", GsonUtils.list2Json(list));
		return "html/sysmgr/role/permitTree";
	}

	@RequestMapping(value = "role/saveRolePermit.do")
	public void saveRolePermit(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			roleService.saveRolePermit(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

}
