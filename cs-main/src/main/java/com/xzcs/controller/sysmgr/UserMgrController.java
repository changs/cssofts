package com.xzcs.controller.sysmgr;

import com.github.pagehelper.PageInfo;
import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.RoleService;
import com.xzcs.service.sysmgr.UserMgrService;
import com.xzcs.util.common.StringUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/sysmgr/")
public class UserMgrController extends BaseController {
	
	@Autowired
	private UserMgrService userMgrService;
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "user/frame.do")
	public String frame(HttpServletRequest request) {
		return "html/sysmgr/user/frame";
	}
	
	@RequestMapping(value = "user/list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		PageInfo<Map<String, Object>> page = userMgrService.findUsers(request);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", page.getTotal());
		result.put("rows", page.getList());
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "user/edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		if (!method.equals("create")) {
			Map<String, Object> user = userMgrService.findUser(request);
			request.setAttribute("info", user);
		}
		return "html/sysmgr/user/edit";
	}

	@RequestMapping(value = "user/save.do")
	public void saveUser(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			userMgrService.saveUser(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "user/delete.do")
	public void deleteUser(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			userMgrService.deleteUser(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "user/checkUserName.do")
	public void checkUserName(HttpServletRequest request, HttpServletResponse response){
		int result = 0;
		try {
			result = userMgrService.checkUserName(request);
		} catch (Exception e) {
			result = 0;
		}
		if (result > 0) {
			ResponseUtils.writeText("false", response);
		} else {
			ResponseUtils.writeText("true", response);
		}
	}

	@RequestMapping(value = "user/userRole.do")
	public String userRole(HttpServletRequest request){
		List<Map<String, Object>> roles = roleService.findRoles(request);
		request.setAttribute("roles", roles);
		String roleIds = userMgrService.findUserRoles(request);
		request.setAttribute("roleIds", roleIds);
		return "html/sysmgr/user/userRole";
	}

	@RequestMapping(value = "user/saveUserRole.do")
	public void saveUserRole(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			userMgrService.saveUserRole(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "user/modifyPwd.do")
	public String modifyPwd(HttpServletRequest request){
		return "html/sysmgr/user/modifyPwd";
	}

	@RequestMapping(value = "user/saveModifyPwd.do")
	public void saveModifyPwd(HttpServletRequest request, HttpServletResponse response){
		String userPwdOld = RequestUtils.getValue(request, "userPwdOld");
		String userPwdNew = RequestUtils.getValue(request, "userPwdNew");
		String userPwdNew2 = RequestUtils.getValue(request, "userPwdNew2");
		Map<String, Object> user = userMgrService.findUser(request);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (null == user) {
				result.put("result", 0);
				result.put("msg", "用户不存在");
			} else if (!StringUtils.md5(userPwdOld).equals(StringUtils.get(user, "user_pwd"))) {
				result.put("result", 0);
				result.put("msg", "原密码不正确");
			} else if (!userPwdNew.equals(userPwdNew2)) {
				result.put("result", 0);
				result.put("msg", "新密码和确认密码不一致");
			} else {
				userMgrService.modifyPassword(request);
				result.put("result", 1);
				result.put("msg", getMessage("operating.type.success"));
			}
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}
}
