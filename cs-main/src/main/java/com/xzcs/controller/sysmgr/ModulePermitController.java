package com.xzcs.controller.sysmgr;

import com.xzcs.controller.BaseController;
import com.xzcs.service.sysmgr.ModulePermitService;
import com.xzcs.service.sysmgr.RoleService;
import com.xzcs.util.easyui.EasyuiUtils;
import com.xzcs.util.web.RequestUtils;
import com.xzcs.util.web.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模块资源管理
 * @author chang
 * @createDate 2015-6-23
 */
@Controller
@RequestMapping(value = "/sysmgr/")
public class ModulePermitController extends BaseController {
	
	@Autowired
	private ModulePermitService modulePermitService;
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "module/frame.do")
	public String frame(HttpServletRequest request) {
		return "html/sysmgr/module/frame";
	}
	
	@RequestMapping(value = "module/list.do")
	public void list(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> modules = modulePermitService.findModules(request);
		List<Map<String, Object>> modules2 = EasyuiUtils.getEasyuiTreeGridData(modules);
		ResponseUtils.writeJson(modules2, response);
	}

	@RequestMapping(value = "module/edit.do")
	public String edit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		List<Map<String, Object>> modules = modulePermitService.findModules(request);
		request.setAttribute("parents", modules);
		if (method.equals("edit")) {
			Map<String, Object> module = modulePermitService.findModule(request);
			request.setAttribute("module", module);
		}
		return "html/sysmgr/module/edit";
	}

	@RequestMapping(value = "module/save.do")
	public void saveModule(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			modulePermitService.saveModule(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "module/delete.do")
	public void deleteModule(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			modulePermitService.deleteModule(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "module/checkCode.do")
	public void checkCode(HttpServletRequest request, HttpServletResponse response){
		int result = 0;
		try {
			result = modulePermitService.checkCode(request);
		} catch (Exception e) {
			result = 0;
		}
		if (result > 0) {
			ResponseUtils.writeText("false", response);
		} else {
			ResponseUtils.writeText("true", response);
		}
	}

	@RequestMapping(value = "module/role.do")
	public String moduleRole(HttpServletRequest request) {
		List<Map<String, Object>> roles = roleService.findRoles(request);
		request.setAttribute("roles", roles);
		String roleIds = modulePermitService.findModuleRoles(request);
		request.setAttribute("roleIds", roleIds);
		return "html/sysmgr/module/role";
	}

	@RequestMapping(value = "module/saveModuleRole.do")
	public void saveModuleRole(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			modulePermitService.saveModuleRole(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "permit/frame.do")
	public String permitFrame(HttpServletRequest request) {
		String moduleId = RequestUtils.getValue(request, "moduleId");
		request.setAttribute("moduleId", moduleId);
		return "html/sysmgr/permit/frame";
	}

	@RequestMapping(value = "permit/list.do")
	public void permitList(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> permits = modulePermitService.findPermitWithModule(request);
		ResponseUtils.writeJson(permits, response);
	}

	@RequestMapping(value = "permit/edit.do")
	public String permitEdit(HttpServletRequest request) {
		String method = RequestUtils.getValue(request, "method");
		Map<String, Object> module = modulePermitService.findModule(request);
		request.setAttribute("module", module);
		if (method.equals("edit")) {
			Map<String, Object> permit = modulePermitService.findPermit(request);
			request.setAttribute("permit", permit);
		}
		return "html/sysmgr/permit/edit";
	}

	@RequestMapping(value = "permit/save.do")
	public void savePermit(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			modulePermitService.savePermit(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "permit/delete.do")
	public void deletePermit(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			modulePermitService.deletePermit(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

	@RequestMapping(value = "permit/role.do")
	public String permitRole(HttpServletRequest request) {
		List<Map<String, Object>> roles = roleService.findRoles(request);
		request.setAttribute("roles", roles);
		String roleIds = modulePermitService.findPermitRoles(request);
		request.setAttribute("roleIds", roleIds);
		return "html/sysmgr/permit/role";
	}

	@RequestMapping(value = "permit/savePermitRole.do")
	public void savePermitRole(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			modulePermitService.savePermitRole(request);
			result.put("result", 1);
			result.put("msg", getMessage("operating.type.success"));
		} catch (Exception e) {
			result.put("result", 0);
			result.put("msg", getMessage("operating.type.error"));
		}
		ResponseUtils.writeJson(result, response);
	}

}
