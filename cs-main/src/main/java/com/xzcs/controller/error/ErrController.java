package com.xzcs.controller.error;

import com.xzcs.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrController extends BaseController {

    @RequestMapping(value="403.shtml")
    public String http403(HttpServletRequest request){
        return "html/error/403";
    }

    @RequestMapping(value="404.shtml")
    public String http404(HttpServletRequest request){
        return "html/error/404";
    }

    @RequestMapping(value="500.shtml")
    public String http500(HttpServletRequest request){
        return "html/error/500";
    }

}
