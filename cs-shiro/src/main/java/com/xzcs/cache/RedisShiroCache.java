package com.xzcs.cache;

import com.xzcs.util.common.SerializeUtils;
import com.xzcs.util.redis.JedisUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;

import java.util.*;

/**
 * @desc:
 * @author: chang
 * @date: 2017/5/22
 */
public class RedisShiroCache<K,V> implements Cache<K,V> {

    private JedisUtils jedisUtils;
    private String keyPrefix = "shiro_cache:";

    public RedisShiroCache(JedisUtils jedisUtils){
        if (jedisUtils == null) {
            throw new IllegalArgumentException("Cache argument cannot be null.");
        }
        this.jedisUtils = jedisUtils;
    }

    private byte[] getByteKey(K key){
        if(key instanceof String){
            String preKey = this.keyPrefix + (String)key;
            return preKey.getBytes();
        }else{
            return SerializeUtils.serialize(key);
        }
    }

    @Override
    public V get(K k) throws CacheException {
        if (null == k) {
            return null;
        }else{
            byte[] rawValue = jedisUtils.get(getByteKey(k));
            V value = (V)SerializeUtils.deserialize(rawValue);
            return value;
        }
    }

    @Override
    public V put(K k, V v) throws CacheException {
        jedisUtils.set(getByteKey(k), SerializeUtils.serialize(v));
        return v;
    }

    @Override
    public V remove(K k) throws CacheException {
        V v = get(k);
        jedisUtils.del(getByteKey(k));
        return v;
    }

    @Override
    public void clear() throws CacheException {
        //jedisUtils.flushDB();
        Set<byte[]> keys = jedisUtils.keys(keyPrefix+"*");
        if (!CollectionUtils.isEmpty(keys)) {
            for (byte[] key : keys) {
                jedisUtils.del(key);
            }
        }
    }

    @Override
    public int size() {
        //Long size = jedisUtils.dbSize();
        //return size.intValue();
        Set<K> keys = keys();
        return keys.size();
    }

    @Override
    public Set<K> keys() {
        Set<byte[]> keys = jedisUtils.keys(keyPrefix+"*");
        if (CollectionUtils.isEmpty(keys)) {
            return Collections.emptySet();
        }else{
            Set<K> newKeys = new HashSet<K>();
            for(byte[] key:keys){
                newKeys.add((K)key);
            }
            return newKeys;
        }
    }

    @Override
    public Collection<V> values() {
        Set<byte[]> keys = jedisUtils.keys(keyPrefix+"*");
        if (!CollectionUtils.isEmpty(keys)) {
            List<V> values = new ArrayList<V>(keys.size());
            for (byte[] key : keys) {
                V value = get((K)key);
                if (value != null) {
                    values.add(value);
                }
            }
            return Collections.unmodifiableList(values);
        } else {
            return Collections.emptyList();
        }
    }
}
