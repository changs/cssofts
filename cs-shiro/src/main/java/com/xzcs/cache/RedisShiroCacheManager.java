package com.xzcs.cache;

import com.xzcs.util.redis.JedisUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @desc: 
 * @author: chang
 * @date: 2017/5/22
 */
public class RedisShiroCacheManager implements CacheManager {

    private ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<String, Cache>();

    private JedisUtils jedisUtils;

    public JedisUtils getJedisUtils() {
        return jedisUtils;
    }

    public void setJedisUtils(JedisUtils jedisUtils) {
        this.jedisUtils = jedisUtils;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        Cache cache = caches.get(name);
        if (null == cache) {
            cache = new RedisShiroCache<K, V>(jedisUtils);
            caches.put(name, cache);
        }
        return cache;
    }
}
