package com.xzcs.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @desc: 
 * @author: chang
 * @date: 2017/6/7
 */
public class MyHashedCredentialsMatcher extends HashedCredentialsMatcher {
	private Cache<String, AtomicInteger> cache;

	public MyHashedCredentialsMatcher(String pwdCacheName, CacheManager cacheManager) {
		cache = cacheManager.getCache(pwdCacheName);
	}

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token,
			AuthenticationInfo info) {
		String userName = (String) token.getPrincipal();

		// retry count + 1
		AtomicInteger retryCount = cache.get(userName);

		if (null == retryCount) {
			retryCount = new AtomicInteger(0);
			cache.put(userName, retryCount);
		}
		
		if (retryCount.incrementAndGet() > 5) {
			throw new ExcessiveAttemptsException();
		}
		boolean matches = super.doCredentialsMatch(token, info);

		if (matches) {
			cache.remove(userName);
		}

		return matches;
	}
}
